DROP TABLE IF EXISTS js_verification_token;
DROP TABLE IF EXISTS js_price;
DROP TABLE IF EXISTS js_route;
DROP TABLE IF EXISTS js_ticket;
DROP TABLE IF EXISTS js_carriage;
DROP TABLE IF EXISTS js_train;
DROP TABLE IF EXISTS js_station;
DROP TABLE IF EXISTS js_user;

CREATE TABLE js_user
(
    user_id bigserial not null,
    user_first_name varchar(100) not null,
    user_last_name varchar(100) not null,
    user_date_of_birth timestamp not null,
    user_login varchar(100) unique not null,
    user_password varchar(100) not null,
    user_email varchar(100) unique not null,
    user_phone varchar (100) unique not null,
    user_role varchar(100) not null,
    user_enabled boolean not null,
    PRIMARY KEY (user_id)
);

CREATE TABLE js_verification_token
(
    token_id bigserial not null,
    user_id bigint not null,
    token_token varchar (100),
    token_expire_date timestamp not null,
    PRIMARY KEY (token_id),
    FOREIGN KEY (user_id) REFERENCES js_user(user_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE js_train
(
    train_id bigserial not null,
    train_name varchar(100) not null,
    PRIMARY KEY (train_id)
);

CREATE TABLE js_carriage
(
    carriage_id bigserial not null,
    train_id bigint not null,
    carriage_number varchar(100) not null,
    carriage_type varchar(100) not null,
    carriage_capacity integer not null,
    PRIMARY KEY (carriage_id),
    FOREIGN KEY (train_id) REFERENCES js_train(train_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE js_station
(
    station_id bigserial not null,
    station_name varchar(300) unique not null,
    PRIMARY KEY (station_id)
);


CREATE TABLE js_ticket
(
    ticket_id bigserial not null,
    carriage_id bigint not null,
    place_number integer not null,
    user_id bigint not null,
    start_station_id bigint,
    finish_station_id bigint,
    ticket_price decimal,
    creation_time timestamp not null,
    PRIMARY KEY (ticket_id),
    FOREIGN KEY (carriage_id) REFERENCES js_carriage(carriage_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (start_station_id) REFERENCES js_station(station_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (finish_station_id) REFERENCES js_station(station_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (user_id) REFERENCES js_user(user_id) ON DELETE RESTRICT ON UPDATE CASCADE
);
CREATE UNIQUE INDEX ticket_constraint ON js_ticket (carriage_id, place_number) WHERE (ticket_price < 0);

CREATE TABLE js_price
(
    price_part_id bigserial not null,
    carriage_type varchar(100) not null,
    train_id bigint not null,
    start_station_id bigint not null,
    finish_station_id bigint not null,
    price decimal,
    PRIMARY KEY (price_part_id),
    FOREIGN KEY (train_id) REFERENCES js_train(train_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (start_station_id) REFERENCES js_station(station_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (finish_station_id) REFERENCES js_station(station_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    UNIQUE (carriage_type, train_id, start_station_id, finish_station_id)
);


CREATE TABLE js_route
(
    route_node_id bigserial not null,
    train_id bigserial not null,
    station_id bigint not null,
    arrival_time timestamp not null,
    departure_time timestamp not null,
    PRIMARY KEY (route_node_id),
    FOREIGN KEY (train_id) REFERENCES js_train(train_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (station_id) REFERENCES js_station(station_id) ON DELETE CASCADE ON UPDATE CASCADE,
    UNIQUE (train_id, station_id)
);


INSERT INTO js_train ("train_name")
VALUES ('SPB-MSK'),
       ('MSK-ANAPA'),
       ('SPB-VORONEZH'),
       ('SPB-ANAPA'),
       ('SPB_ROSTOV'),
       ('ROSTOV-SPB'),
       ('ANAPA-MSK'),
       ('ANAPA-MSK'),
       ('ANAPA-MSK');

INSERT INTO js_carriage ("train_id", "carriage_type", "carriage_number", "carriage_capacity")
VALUES (4, 'BUSINESS', '1 carriage', 15),
       (4, 'COMFORT', '2 carriage', 30),
       (4, 'ECONOMY', '3 carriage', 35),
       (4, 'COMFORT', '4 carriage', 30),
       (1, 'BUSINESS', '1 carriage', 15),
       (1, 'COMFORT', '2 carriage', 30),
       (1, 'ECONOMY', '3 carriage', 35),
       (1, 'COMFORT', '4 carriage', 30),
       (2, 'BUSINESS', '1 carriage', 15),
       (2, 'COMFORT', '2 carriage', 30),
       (2, 'ECONOMY', '3 carriage', 35),
       (2, 'COMFORT', '4 carriage', 30),
       (3, 'BUSINESS', '1 carriage', 15),
       (3, 'COMFORT', '2 carriage', 30),
       (3, 'ECONOMY', '3 carriage', 35),
       (3, 'COMFORT', '4 carriage', 30),
       (5, 'BUSINESS', '1 carriage', 15),
       (5, 'COMFORT', '2 carriage', 30),
       (5, 'ECONOMY', '3 carriage', 35),
       (6, 'COMFORT', '4 carriage', 30),
       (6, 'BUSINESS', '1 carriage', 15),
       (6, 'COMFORT', '2 carriage', 30),
       (7, 'ECONOMY', '3 carriage', 35),
       (7, 'COMFORT', '4 carriage', 30),
       (7, 'BUSINESS', '1 carriage', 15),
       (8, 'COMFORT', '2 carriage', 30),
       (8, 'ECONOMY', '3 carriage', 35),
       (8, 'COMFORT', '4 carriage', 30),
       (9, 'BUSINESS', '1 carriage', 15),
       (9, 'COMFORT', '2 carriage', 30),
       (9, 'ECONOMY', '3 carriage', 35),
       (9, 'COMFORT', '4 carriage', 30);


INSERT INTO js_user ("user_first_name", "user_last_name", "user_date_of_birth", "user_login",
                     "user_password", "user_email", "user_phone", "user_role", "user_enabled")
VALUES ('admin', 'admin', '1990-06-14', 'admin', '$2a$10$iAEq9SVkG8WnYtLz.fonU.wIXnfa.G1H411XFt/xycp/Bi7uBjWce',
        'krylov.dmitry90@gmail.com', '+79214357668', 'ROLE_ADMIN', 'true'),
       ('user', 'user', '1990-06-14', 'user', '$2a$10$iAEq9SVkG8WnYtLz.fonU.wIXnfa.G1H411XFt/xycp/Bi7uBjWce',
        'test@gmail.com', '+1234567890','ROLE_USER', 'true'),
       ('LOL', 'KEK', '1990-06-14', 'user2', '$2a$10$iAEq9SVkG8WnYtLz.fonU.wIXnfa.G1H411XFt/xycp/Bi7uBjWce',
        'ttt12321ttt@mail.ru', '+71111111111','ROLE_USER', 'true'),
        ('EXISTING', 'EXISTING', '1990-06-14', 'EXISTING', '$2a$10$iAEq9SVkG8WnYtLz.fonU.wIXnfa.G1H411XFt/xycp/Bi7uBjWce',
        'EXISTING@EXISTING.ru', '+12345678','ROLE_USER', 'true');
-- pass = 123

INSERT INTO js_station ("station_name")
VALUES ('SPB'),
       ('TVER'),
       ('MSK'),
       ('VORONEZH'),
       ('TULA'),
       ('ROSTOV'),
       ('ANAPA'),
       ('EXISTINGSTATION');

INSERT INTO js_route ("train_id", "station_id", "arrival_time", "departure_time")
VALUES (1, 1, '2030-07-19 23:40', '2030-07-19 23:55'),
       (1, 2, '2030-07-20 03:45', '2030-07-20 04:00'),
       (1, 3, '2030-07-20 07:55', '2030-07-20 08:30'),
       (2, 3, '2030-07-20 04:00', '2030-07-20 04:30'),
       (2, 4, '2030-07-20 16:55', '2030-07-20 17:30'),
       (2, 5, '2030-07-20 21:45', '2030-07-20 22:00'),
       (2, 6, '2030-07-21 02:55', '2030-07-21 03:15'),
       (3, 1, '2030-07-20 02:00', '2030-07-20 02:30'),
       (3, 2, '2030-07-20 04:55', '2030-07-20 04:30'),
       (3, 3, '2030-07-20 08:45', '2030-07-20 09:00'),
       (3, 4, '2030-07-20 18:55', '2030-07-20 19:15'),
       (4, 1, '2030-07-19 18:00', '2030-07-19 18:20'),
       (4, 3, '2030-07-20 06:55', '2030-07-20 07:20'),
       (4, 5, '2030-07-20 19:45', '2030-07-20 20:00'),
       (4, 7, '2030-07-21 02:55', '2030-07-21 03:15'),
       (5, 1, '2030-07-19 21:30', '2030-07-19 22:00'),
       (5, 3, '2030-07-20 05:00', '2030-07-20 05:20'),
       (5, 4, '2030-07-20 14:55', '2030-07-20 15:20'),
       (5, 5, '2030-07-20 23:45', '2030-07-21 00:00'),
       (5, 6, '2030-07-21 03:55', '2030-07-21 04:15'),
       (6, 6, '2030-07-20 05:00', '2030-07-20 05:20'),
       (6, 5, '2030-07-20 14:55', '2030-07-20 15:20'),
       (6, 3, '2030-07-20 23:45', '2030-07-21 00:00'),
       (6, 1, '2030-07-21 03:55', '2030-07-21 04:15'),
       (7, 7, '2030-07-20 04:00', '2030-07-20 04:30'),
       (7, 5, '2030-07-20 16:55', '2030-07-20 17:30'),
       (7, 4, '2030-07-20 21:45', '2030-07-20 22:00'),
       (7, 3, '2030-07-21 02:55', '2030-07-21 03:15'),
       (8, 7, '2030-07-20 04:00', '2030-07-20 04:30'),
       (8, 5, '2030-07-20 16:55', '2030-07-20 17:30'),
       (8, 4, '2030-07-20 21:45', '2030-07-20 22:00'),
       (8, 3, '2030-07-21 02:55', '2030-07-21 03:15'),
       (9, 7, '2030-07-20 04:00', '2030-07-20 04:30'),
       (9, 5, '2030-07-20 16:55', '2030-07-20 17:30'),
       (9, 4, '2030-07-20 21:45', '2030-07-20 22:00'),
       (9, 3, '2030-07-21 02:55', '2030-07-21 03:15');

INSERT INTO js_ticket ("carriage_id", "place_number", "user_id", "start_station_id", "finish_station_id",
                       "ticket_price", "creation_time")
VALUES (2, 1, 1, 3, 5, 2500.02, '2021-07-21 03:15'),
       (2, 2, 2, 3, 5, 1200, '2021-07-21 03:15'),
       (2, 10, 1, 3, 5, -1, '2029-07-21 03:15'),
       (2, 11, 3, 3, 5, -1, '2001-07-21 03:15'),
       (2, 3, 3, 1, 3, 2500, '2021-07-21 03:15'),
       (3, 4, 1, 3, 5, 1200, '2021-07-21 03:15'),
       (3, 5, 2, 5, 7, 2500, '2021-07-21 03:15'),
       (3, 6, 3, 5, 7, 1200, '2021-07-21 03:15');

INSERT INTO js_price (carriage_type, train_id, start_station_id, finish_station_id, price)
values ('BUSINESS', 1, 1, 2, 3000.07),
       ('COMFORT', 1, 1, 2, 2000.07),
       ('ECONOMY', 1, 1, 2, 1000),
       ('BUSINESS', 1, 2, 3, 3000),
       ('COMFORT', 1, 2, 3, 2000),
       ('ECONOMY', 1, 2, 3, 1000),
       ('BUSINESS', 2, 3, 4, 3000),
       ('COMFORT', 2, 3, 4, 2000),
       ('ECONOMY', 2, 3, 4, 1000),
       ('BUSINESS', 2, 4, 5, 3000),
       ('COMFORT', 2, 4, 5, 2000),
       ('ECONOMY', 2, 4, 5, 1000),
       ('BUSINESS', 2, 5, 6, 3000),
       ('COMFORT', 2, 5, 6, 2000),
       ('ECONOMY', 2, 5, 6, 1000),
       ('BUSINESS', 3, 1, 2, 3000),
       ('COMFORT', 3, 1, 2, 2000),
       ('ECONOMY', 3, 1, 2, 1000),
       ('BUSINESS', 3, 2, 3, 3000),
       ('COMFORT', 3, 2, 3, 2000),
       ('ECONOMY', 3, 2, 3, 1000),
       ('BUSINESS', 3, 3, 4, 3000),
       ('COMFORT', 3, 3, 4, 2000),
       ('ECONOMY', 3, 3, 4, 1000),
       ('BUSINESS', 4, 1, 3, 3000),
       ('COMFORT', 4, 1, 3, 2000),
       ('ECONOMY', 4, 1, 3, 1000),
       ('BUSINESS', 4, 3, 5, 3000),
       ('COMFORT', 4, 3, 5, 2000),
       ('ECONOMY', 4, 3, 5, 1000),
       ('BUSINESS', 4, 5, 7, 3000),
       ('COMFORT', 4, 5, 7, 2000),
       ('ECONOMY', 4, 5, 7, 1000),
       ('BUSINESS', 5, 1, 3, 3000),
       ('COMFORT', 5, 1, 3, 2000),
       ('ECONOMY', 5, 1, 3, 1000),
       ('BUSINESS', 5, 3, 4, 3000),
       ('COMFORT', 5, 3, 4, 2000),
       ('ECONOMY', 5, 3, 4, 1000),
       ('BUSINESS', 5, 4, 5, 3000),
       ('COMFORT', 5, 4, 5, 2000),
       ('ECONOMY', 5, 4, 5, 1000),
       ('BUSINESS', 5, 5, 6, 3000),
       ('COMFORT', 5, 5, 6, 2000),
       ('ECONOMY', 5, 5, 6, 1000),
       ('BUSINESS', 6, 6, 5, 3000),
       ('COMFORT', 6, 6, 5, 2000),
       ('ECONOMY', 6, 6, 5, 1000),
       ('BUSINESS', 6, 5, 3, 3000),
       ('COMFORT', 6, 5, 3, 2000),
       ('ECONOMY', 6, 5, 3, 1000),
       ('BUSINESS', 6, 3, 1, 3000),
       ('COMFORT', 6, 3, 1, 2000),
       ('ECONOMY', 6, 3, 1, 1000),
       ('BUSINESS', 7, 7, 5, 3000),
       ('COMFORT', 7, 7, 5, 2000),
       ('ECONOMY', 7, 7, 5, 1000),
       ('BUSINESS', 7, 5, 4, 3000),
       ('COMFORT', 7, 5, 4, 2000),
       ('ECONOMY', 7, 5, 4, 1000),
       ('BUSINESS', 7, 4, 3, 3000),
       ('COMFORT', 7, 4, 3, 2000),
       ('ECONOMY', 7, 4, 3, 1000),
       ('BUSINESS', 8, 7, 5, 3000),
       ('COMFORT', 8, 7, 5, 2000),
       ('ECONOMY', 8, 7, 5, 1000),
       ('BUSINESS', 8, 5, 4, 3000),
       ('COMFORT', 8, 5, 4, 2000),
       ('ECONOMY', 8, 5, 4, 1000),
       ('BUSINESS', 8, 4, 3, 3000),
       ('COMFORT', 8, 4, 3, 2000),
       ('ECONOMY', 8, 4, 3, 1000),
       ('BUSINESS', 9, 7, 5, 3000),
       ('COMFORT', 9, 7, 5, 2000),
       ('ECONOMY', 9, 7, 5, 1000),
       ('BUSINESS', 9, 5, 4, 3000),
       ('COMFORT', 9, 5, 4, 2000),
       ('ECONOMY', 9, 5, 4, 1000),
       ('BUSINESS', 9, 4, 3, 3000),
       ('COMFORT', 9, 4, 3, 2000),
       ('ECONOMY', 9, 4, 3, 1000);



