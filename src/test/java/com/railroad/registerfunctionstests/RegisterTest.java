package com.railroad.registerfunctionstests;

import com.railroad.config.HibernateTestConfig;
import com.railroad.config.MailTestConfig;
import com.railroad.config.MqTestConfig;
import com.railroad.config.TestContextConfiguration;
import com.railroad.controllers.RegistrationController;
import com.railroad.dto.user.RegistrationUserDto;
import com.railroad.dto.user.UserDto;
import com.railroad.exceptions.CaptchaVerificationException;
import com.railroad.exceptions.SuchUserAlreadyExistsException;
import com.railroad.service.captcha.CaptchaVerificationService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MqTestConfig.class, HibernateTestConfig.class, TestContextConfiguration.class,
        MailTestConfig.class})
@Sql(scripts = {"/reloadTestData.sql"})
public class RegisterTest {

    @Autowired
    private RegistrationController registrationController;

    @Autowired
    private CaptchaVerificationService captchaVerificationService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private RegistrationUserDto registrationUserDto = new RegistrationUserDto();

    @Before
    public void init() {
        registrationUserDto.setCaptchaResponse("captchaResponse");
        UserDto userDto = new UserDto();
        userDto.setFirstName("TestUser firstName");
        userDto.setLastName("TestUserLastname");
        userDto.setDateOfBirth("14.06.1990");
        userDto.setLogin("TestUserLogin");
        userDto.setPhone("+88888888");
        userDto.setEmail("testUser@testUser.com");
        userDto.setPassword("testUserPassword");
        registrationUserDto.setUserDto(userDto);
    }

    @Test
    public void captchaVerificationErrorShouldThrowError(){
        when(captchaVerificationService.verifyCaptchaResponse("captchaResponse")).thenReturn(false);
        expectedException.expect(CaptchaVerificationException.class);
        expectedException.expectMessage("CAPTCHA VERIFICATION FAILED");
        registrationController.tryToRegister(registrationUserDto, any());
    }

    @Test
    public void sameLoginShouldThrowError(){
        when(captchaVerificationService.verifyCaptchaResponse("captchaResponse")).thenReturn(true);
        expectedException.expect(SuchUserAlreadyExistsException.class);
        expectedException.expectMessage("LOGIN");
        registrationUserDto.getUserDto().setLogin("EXISTING");
        registrationController.tryToRegister(registrationUserDto, any());
    }
}
