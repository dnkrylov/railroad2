package com.railroad.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;


@Configuration
@Profile("test")
@PropertySource("classpath:application.properties")
public class MqTestConfig {
    @Value("${broker.uri}")
    private String wireLevelEndpoint;

    @Value("${broker.username}")
    private String jmsUser;

    @Value("${broker.password}")
    private String jmsPassword;

    @Bean
    public PooledConnectionFactory pooledConnectionFactory(){
        ActiveMQConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory(wireLevelEndpoint);
        connectionFactory.setUserName(jmsUser);
        connectionFactory.setPassword(jmsPassword);
        PooledConnectionFactory pooledConnectionFactory =
                new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(connectionFactory);
        pooledConnectionFactory.setMaxConnections(10);
        return pooledConnectionFactory;
    }
}
