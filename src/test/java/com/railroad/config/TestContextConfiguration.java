package com.railroad.config;

import com.railroad.service.SecurityService;
import com.railroad.service.captcha.CaptchaVerificationService;
import com.railroad.service.jms.JmsMessageSender;
import org.mockito.Mockito;
import org.springframework.context.annotation.*;

@Profile("test")
@Configuration
@ComponentScan(basePackages = {"com.railroad.service", "com.railroad.controllers",
"com.railroad.repo"})
public class TestContextConfiguration {
    @Bean
    @Primary
    public JmsMessageSender jmsMessageSender() {
        return Mockito.mock(JmsMessageSender.class);
    }

    @Bean
    @Primary
    public CaptchaVerificationService captchaVerificationService() {
        return Mockito.mock(CaptchaVerificationService.class);
    }

    @Bean
    @Primary
    public SecurityService securityService() {
        return Mockito.mock(SecurityService.class);
    }
}
