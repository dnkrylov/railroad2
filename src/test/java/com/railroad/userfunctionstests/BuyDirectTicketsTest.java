package com.railroad.userfunctionstests;


import com.railroad.config.HibernateTestConfig;
import com.railroad.config.MailTestConfig;
import com.railroad.config.MqTestConfig;
import com.railroad.config.TestContextConfiguration;
import com.railroad.controllers.UserController;
import com.railroad.domain.User;
import com.railroad.dto.UserDetailsDto;
import com.railroad.dto.user.TicketInfo;
import com.railroad.exceptions.DuplicatingTicketException;
import com.railroad.exceptions.ToLateException;
import com.railroad.response.BusinessResponse;
import com.railroad.service.SecurityService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;

import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MqTestConfig.class, HibernateTestConfig.class, TestContextConfiguration.class,
        MailTestConfig.class})
@Sql(scripts = {"/reloadTestData.sql"})
public class BuyDirectTicketsTest {

    @Autowired
    private UserController userController;

    @Mock
    private HttpSession httpSession;

    @Autowired
    private SecurityService securityService;

    private TicketInfo ticketInfo;

    private UserDetailsDto userDetailsDto;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ticketInfo = new TicketInfo();
        ticketInfo.setStartStationName("SPB");
        ticketInfo.setEndStationName("ANAPA");
        userDetailsDto = new UserDetailsDto(new User());
        userDetailsDto.getUser().setId(4L);
    }

    @Test
    @Transactional
    public void tempTicketShouldBuUpgraded() {
        when(securityService.getLoggedInUser()).thenReturn(userDetailsDto);
        when(httpSession.getAttribute("ticketInfo")).thenReturn(ticketInfo);
        userController.preparePaymentPage(httpSession, 1L, 1);
        BusinessResponse response = userController.tryToBuyDirectTicket(httpSession, "1111222233334444",
                "11/11", "222");
        Assert.assertEquals("ticket was successfully bought", response.getMessage());
    }

    @Test
    @Transactional
    public void ifTicketWasBoughtShouldThrowError() {
        when(securityService.getLoggedInUser()).thenReturn(userDetailsDto);
        when(httpSession.getAttribute("ticketInfo")).thenReturn(ticketInfo);
        userController.preparePaymentPage(httpSession, 1L, 1);

        ticketInfo.setTicketId(44L);
        expectedException.expect(ToLateException.class);
        expectedException.expectMessage("THIS TICKET WAS ALREADY BOUGHT");
        userController.tryToBuyDirectTicket(httpSession, "1111222233334444",
                "11/11", "222");
    }

    @Test
    @Transactional
    public void cantBuyMoreThanOneTicketToOneTrain() {
        userDetailsDto.getUser().setId(1L);
        when(securityService.getLoggedInUser()).thenReturn(userDetailsDto);
        when(httpSession.getAttribute("ticketInfo")).thenReturn(ticketInfo);
        userController.preparePaymentPage(httpSession, 1L, 1);
        expectedException.expect(DuplicatingTicketException.class);
        expectedException.expectMessage("YOU ALREADY HAVE A TICKET TO THIS TRAIN");
        userController.tryToBuyDirectTicket(httpSession, "1111222233334444",
                "11/11", "222");
    }

}
