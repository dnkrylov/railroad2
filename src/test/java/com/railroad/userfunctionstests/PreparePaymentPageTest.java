package com.railroad.userfunctionstests;

import com.railroad.config.HibernateTestConfig;
import com.railroad.config.MailTestConfig;
import com.railroad.config.MqTestConfig;
import com.railroad.config.TestContextConfiguration;
import com.railroad.controllers.UserController;
import com.railroad.domain.Ticket;
import com.railroad.domain.User;
import com.railroad.dto.UserDetailsDto;
import com.railroad.dto.user.TicketInfo;
import com.railroad.exceptions.ThisPlaceIsAlreadyBusyException;
import com.railroad.repo.TicketRepo;
import com.railroad.response.BusinessResponse;
import com.railroad.service.SecurityService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MqTestConfig.class, HibernateTestConfig.class, TestContextConfiguration.class,
        MailTestConfig.class})
@Sql(scripts = {"/reloadTestData.sql"})
public class PreparePaymentPageTest {

    @Autowired
    private UserController userController;

    @Autowired
    private TicketRepo ticketRepo;

    @Mock
    private HttpSession httpSession;

    @Autowired
    private SecurityService securityService;

    private TicketInfo ticketInfo;

    private UserDetailsDto userDetailsDto;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ticketInfo = new TicketInfo();
        ticketInfo.setStartStationName("SPB");
        ticketInfo.setEndStationName("ANAPA");
        userDetailsDto = new UserDetailsDto(new User());
        userDetailsDto.getUser().setId(1L);
    }

    @Test
    @Transactional
    public void temporaryTicketShouldBeCreated() {
        when(securityService.getLoggedInUser()).thenReturn(userDetailsDto);
        when(httpSession.getAttribute("ticketInfo")).thenReturn(ticketInfo);
        BusinessResponse response = userController.preparePaymentPage(httpSession, 1L, 1);
        assertEquals("TICKET PRICE: 9000.0", response.getMessage());
        Ticket tempTicket = ticketRepo.findTicketById(ticketInfo.getTicketId());
        assertEquals(-1.00, tempTicket.getPrice(), 0);
        assertEquals((long)tempTicket.getUser().getId(), ticketInfo.getUserId());
        assertEquals((long)tempTicket.getCarriage().getId(), ticketInfo.getCarriageId());
        assertNull(tempTicket.getStartStation());
    }

    @Test
    @Transactional
    public void ifPlaceIsBusyShouldThrowError() {
        when(securityService.getLoggedInUser()).thenReturn(userDetailsDto);
        when(httpSession.getAttribute("ticketInfo")).thenReturn(ticketInfo);
        expectedException.expect(ThisPlaceIsAlreadyBusyException.class);
        expectedException.expectMessage("THIS TICKET WAS ALREADY BOUGHT");
        userController.preparePaymentPage(httpSession, 2L, 3);
    }


}
