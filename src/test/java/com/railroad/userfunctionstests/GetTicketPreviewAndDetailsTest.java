package com.railroad.userfunctionstests;

import com.railroad.config.HibernateTestConfig;
import com.railroad.config.MailTestConfig;
import com.railroad.config.MqTestConfig;
import com.railroad.config.TestContextConfiguration;
import com.railroad.controllers.UserController;
import com.railroad.domain.Ticket;
import com.railroad.domain.User;
import com.railroad.dto.UserDetailsDto;
import com.railroad.dto.user.TicketDetailsDto;
import com.railroad.dto.user.TicketInfo;
import com.railroad.response.BusinessResponse;
import com.railroad.service.SecurityService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MqTestConfig.class, HibernateTestConfig.class, TestContextConfiguration.class,
        MailTestConfig.class})
@Sql(scripts = {"/reloadTestData.sql"})
public class GetTicketPreviewAndDetailsTest {

    @Autowired
    private UserController userController;

    @Autowired
    private SecurityService securityService;

    private UserDetailsDto userDetailsDto;

    @Before
    public void setup() {
        userDetailsDto = new UserDetailsDto(new User());
        userDetailsDto.getUser().setId(1L);
    }

    @Test
    @Transactional
    public void shouldReturnExpectedTickets() {
        when(securityService.getLoggedInUser()).thenReturn(userDetailsDto);
        assertEquals(2, userController.getTicketPreview().size());
    }

    @Test
    @Transactional
    public void shouldReturnExpectedTicket() {
        when(securityService.getLoggedInUser()).thenReturn(userDetailsDto);
        TicketDetailsDto ticketDetailsDto = userController.getTicketDetails(1L);
        assertEquals("admin", ticketDetailsDto.getUserFirstName());
        assertEquals("MSK", ticketDetailsDto.getStartStation());
        assertEquals(2500.02, ticketDetailsDto.getPrice(),0);
    }
}
