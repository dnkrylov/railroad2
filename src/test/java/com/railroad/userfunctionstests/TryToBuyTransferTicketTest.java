package com.railroad.userfunctionstests;

import com.railroad.config.HibernateTestConfig;
import com.railroad.config.MailTestConfig;
import com.railroad.config.MqTestConfig;
import com.railroad.config.TestContextConfiguration;
import com.railroad.controllers.UserController;
import com.railroad.domain.Ticket;
import com.railroad.domain.User;
import com.railroad.dto.UserDetailsDto;
import com.railroad.dto.user.TransferTicketInfo;
import com.railroad.repo.TicketRepo;
import com.railroad.response.BusinessResponse;
import com.railroad.service.SecurityService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MqTestConfig.class, HibernateTestConfig.class, TestContextConfiguration.class,
        MailTestConfig.class})
@Sql(scripts = {"/reloadTestData.sql"})
public class TryToBuyTransferTicketTest {

    @Autowired
    private UserController userController;

    @Mock
    private HttpSession httpSession;

    @Autowired
    private SecurityService securityService;

    private TransferTicketInfo transferTicketInfo;

    private UserDetailsDto userDetailsDto;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        transferTicketInfo = new TransferTicketInfo();
        transferTicketInfo.setStartStationName("SPB");
        transferTicketInfo.setMiddleStationName("MSK");
        transferTicketInfo.setEndStationName("ANAPA");
        transferTicketInfo.setTrainId1(5L);
        transferTicketInfo.setTrainId2(4L);
        userDetailsDto = new UserDetailsDto(new User());
        userDetailsDto.getUser().setId(4L);
    }

    @Test
    @Transactional
    public void temporaryTicketsShouldBeCreated() {
        when(securityService.getLoggedInUser()).thenReturn(userDetailsDto);
        when(httpSession.getAttribute("transferTicketInfo")).thenReturn(transferTicketInfo);
        userController.prepareTransferPaymentPage(
                httpSession, 19L, 1, 1,1);
        BusinessResponse response = userController.tryToBuyTransferTicket(httpSession, "1111222233334444",
                "11/11", "222");
        Assert.assertEquals("ticket was successfully bought", response.getMessage());
    }
}
