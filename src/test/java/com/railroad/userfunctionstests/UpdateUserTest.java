package com.railroad.userfunctionstests;

import com.railroad.config.HibernateTestConfig;
import com.railroad.config.MailTestConfig;
import com.railroad.config.MqTestConfig;
import com.railroad.config.TestContextConfiguration;
import com.railroad.controllers.UserController;
import com.railroad.domain.User;
import com.railroad.domain.UserRole;
import com.railroad.dto.UserDetailsDto;
import com.railroad.dto.user.UserDto;
import com.railroad.exceptions.SuchUserAlreadyExistsException;
import com.railroad.repo.UserRepo;
import com.railroad.service.SecurityService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MqTestConfig.class, HibernateTestConfig.class, TestContextConfiguration.class,
        MailTestConfig.class})
@Sql(scripts = {"/reloadTestData.sql"})
public class UpdateUserTest {

    @Autowired
    private UserController userController;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private SecurityService securityService;

    private UserDto userDto;

    private UserDetailsDto oldUser;

    @Before
    public void initDto() {
        userDto = new UserDto(null, "newName", "newSurname", "01.01.2001",
                "newLogin", "newEmail@mail.mail", "+999888222",
                "1111" , UserRole.ROLE_USER);
        User user = new User();
        user.setId(3L);
        user.setUserRole(UserRole.ROLE_USER);
        user.setEnabled(true);
        oldUser = new UserDetailsDto(user);
    }
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    @Transactional
    public void ifLoginIsBusyShouldThrowError() {
        when(securityService.getLoggedInUser()).thenReturn(oldUser);
        userDto.setLogin("EXISTING");
        expectedException.expect(SuchUserAlreadyExistsException.class);
        expectedException.expectMessage("LOGIN");
        userController.updateUser(userDto);
    }

    @Test
    @Transactional
    public void ifEmailIsBusyShouldThrowError() {
        when(securityService.getLoggedInUser()).thenReturn(oldUser);
        userDto.setEmail("EXISTING@EXISTING.ru");
        expectedException.expect(SuchUserAlreadyExistsException.class);
        expectedException.expectMessage("EMAIL");
        userController.updateUser(userDto);
    }

    @Test
    @Transactional
    public void ifPhoneIsBusyShouldThrowError() {
        when(securityService.getLoggedInUser()).thenReturn(oldUser);
        userDto.setPhone("+12345678");
        expectedException.expect(SuchUserAlreadyExistsException.class);
        expectedException.expectMessage("PHONE");
        userController.updateUser(userDto);
    }

    @Test
    @Transactional
    public void ifEverythingIsOkWeGotMessage () {
        when(securityService.getLoggedInUser()).thenReturn(oldUser);
        assertEquals("information updated", userController.updateUser(userDto).getMessage());
        assertEquals("newLogin", userRepo.findUserById(3L).getLogin());
    }

    @Test
    @Transactional
    public void ifUserChangesHisRoleByHackingUiHeWillNotGetProfit () {
        when(securityService.getLoggedInUser()).thenReturn(oldUser);
        userDto.setUserRole(UserRole.ROLE_ADMIN);
        assertEquals("information updated", userController.updateUser(userDto).getMessage());
        assertEquals(UserRole.ROLE_USER, userRepo.findUserById(3L).getUserRole());
    }


}
