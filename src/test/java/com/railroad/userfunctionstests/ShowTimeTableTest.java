package com.railroad.userfunctionstests;

import com.railroad.config.HibernateTestConfig;
import com.railroad.config.MailTestConfig;
import com.railroad.config.MqTestConfig;
import com.railroad.config.TestContextConfiguration;
import com.railroad.controllers.UserController;
import com.railroad.dto.user.TimeTableItemDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MqTestConfig.class, HibernateTestConfig.class, TestContextConfiguration.class,
        MailTestConfig.class})
@Sql(scripts = {"/reloadTestData.sql"})
public class ShowTimeTableTest {

    @Autowired
    private UserController userController;

    @Test
    public void ifNoTrainsShouldReturnEmptyList() {
        List<TimeTableItemDto> timeTable = userController.showTimeTable("01.01.2000", "SPB");
        assertTrue(timeTable.isEmpty());
    }

    @Test
    public void shouldReturnExpectedList() {
        List<TimeTableItemDto> timeTable = userController.showTimeTable("19.07.2030", "SPB");
        assertEquals(3, timeTable.size());
        assertEquals("SPB-ANAPA", timeTable.get(0).getTrainNumber());
    }

    @Test
    public void ifStationNotExistsShouldReturnEmptyList() {
        List<TimeTableItemDto> timeTable = userController.showTimeTable("19.07.2030", "sfsafsa");
        assertTrue(timeTable.isEmpty());
    }
}
