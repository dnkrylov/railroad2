package com.railroad.userfunctionstests;

import com.railroad.config.HibernateTestConfig;
import com.railroad.config.MailTestConfig;
import com.railroad.config.MqTestConfig;
import com.railroad.config.TestContextConfiguration;
import com.railroad.controllers.UserController;
import com.railroad.domain.User;
import com.railroad.dto.UserDetailsDto;
import com.railroad.dto.user.AvailableTransferRouteDto;
import com.railroad.dto.user.TransferTicketInfo;
import com.railroad.service.SecurityService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.servlet.http.HttpSession;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MqTestConfig.class, HibernateTestConfig.class, TestContextConfiguration.class,
        MailTestConfig.class})
@Sql(scripts = {"/reloadTestData.sql"})
public class GetTransferCarriagesTest {
    @Autowired
    private UserController userController;

    @Mock
    private HttpSession httpSession;

    @Autowired
    private SecurityService securityService;

    private TransferTicketInfo transferTicketInfo;

    private UserDetailsDto userDetailsDto;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        transferTicketInfo = new TransferTicketInfo();
        transferTicketInfo.setStartStationName("SPB");
        transferTicketInfo.setEndStationName("ANAPA");
        userDetailsDto = new UserDetailsDto(new User());
        userDetailsDto.getUser().setId(1L);
    }

    @Test
    public void shouldReturnExpectedTransferCarriages(){
        when(securityService.getLoggedInUser()).thenReturn(userDetailsDto);
        when(httpSession.getAttribute("transferTicketInfo")).thenReturn(transferTicketInfo);
        AvailableTransferRouteDto availableTransferRouteDto = new AvailableTransferRouteDto();
        availableTransferRouteDto.setTrainId1(5L);
        availableTransferRouteDto.setTrainId2(4L);
        availableTransferRouteDto.setMiddleStationName("MSK");

        assertEquals(3, userController.getTransferCarriages(
                httpSession, availableTransferRouteDto).getTrain1Carriages().size());
        assertEquals(4, userController.getTransferCarriages(
                httpSession, availableTransferRouteDto).getTrain2Carriages().size());
        assertEquals(3000.00, userController.getTransferCarriages(
                httpSession, availableTransferRouteDto).getTrain1Carriages().get(0).getTicketPrice(), 0.0);
    }
}
