package com.railroad.userfunctionstests;

import com.railroad.config.HibernateTestConfig;
import com.railroad.config.MailTestConfig;
import com.railroad.config.MqTestConfig;
import com.railroad.config.TestContextConfiguration;
import com.railroad.controllers.UserController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MqTestConfig.class, HibernateTestConfig.class, TestContextConfiguration.class,
        MailTestConfig.class})
@Sql(scripts = {"/reloadTestData.sql"})
public class GetStationsTest {

    @Autowired
    private UserController userController;

    @Test
    public void shouldReturn8Stations() {
        List<String> stationNames = userController.getStations();
        assertEquals(8, stationNames.size());
    }

    @Test
    public void shouldContainExistingStation() {
        List<String> stationNames = userController.getStations();
        assertTrue(stationNames.contains("EXISTINGSTATION"));
    }

    @Test
    public void shouldBeSorted() {
        List<String> stationNames = userController.getStations();
        assertTrue(stationNames.get(0).equals("ANAPA") && stationNames.get(7).equals("VORONEZH"));
    }
}
