package com.railroad.userfunctionstests;

import com.railroad.config.HibernateTestConfig;
import com.railroad.config.MailTestConfig;
import com.railroad.config.MqTestConfig;
import com.railroad.config.TestContextConfiguration;
import com.railroad.controllers.UserController;
import com.railroad.domain.User;
import com.railroad.dto.UserDetailsDto;
import com.railroad.dto.user.TicketInfo;
import com.railroad.repo.TicketRepo;
import com.railroad.service.SecurityService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MqTestConfig.class, HibernateTestConfig.class, TestContextConfiguration.class,
        MailTestConfig.class})
@Sql(scripts = {"/reloadTestData.sql"})
public class GetDirectCarriageDtosTest {
    @Autowired
    private UserController userController;

    @Mock
    private HttpSession httpSession;

    @Autowired
    private TicketRepo ticketRepo;

    @Autowired
    private SecurityService securityService;

    private TicketInfo ticketInfo;

    private UserDetailsDto userDetailsDto;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ticketInfo = new TicketInfo();
        ticketInfo.setStartStationName("SPB");
        ticketInfo.setEndStationName("MSK");
        userDetailsDto = new UserDetailsDto(new User());
        userDetailsDto.getUser().setId(1L);
    }

    @Test
    public void shouldReturnExpectedCarriages() {
        when(securityService.getLoggedInUser()).thenReturn(userDetailsDto);
        when(httpSession.getAttribute("ticketInfo")).thenReturn(ticketInfo);
        assertEquals(4, userController.getDirectCarriages(httpSession, 4L).size());
        assertEquals("1 carriage", userController.getDirectCarriages(httpSession, 4L).get(0).getNumber());
        assertEquals(3000.00, userController.getDirectCarriages(httpSession, 4L).get(0).getTicketPrice(), 0.0);
    }

    @Test
    public void shouldNotReturnBusyPlacesCarriages() {
        ticketInfo.setEndStationName("ANAPA");
        when(securityService.getLoggedInUser()).thenReturn(userDetailsDto);
        when(httpSession.getAttribute("ticketInfo")).thenReturn(ticketInfo);
        assertFalse(userController.getDirectCarriages(httpSession, 4L).get(1).getFreePlaceNumbers().contains(1));
    }

    @Test
    @Transactional
    public void shouldDeleteTemporaryTicketsForCurrentUser() {
        when(securityService.getLoggedInUser()).thenReturn(userDetailsDto);
        when(httpSession.getAttribute("ticketInfo")).thenReturn(ticketInfo);
        userController.getDirectCarriages(httpSession, 4L);
        Assert.assertNull(ticketRepo.findTicketById(3L));
    }

    @Test
    @Transactional
    public void shouldDeleteTrashRicketsForCurrentUser() {
        when(securityService.getLoggedInUser()).thenReturn(userDetailsDto);
        when(httpSession.getAttribute("ticketInfo")).thenReturn(ticketInfo);
        userController.getDirectCarriages(httpSession, 4L);
        Assert.assertNull(ticketRepo.findTicketById(4L));
    }
}
