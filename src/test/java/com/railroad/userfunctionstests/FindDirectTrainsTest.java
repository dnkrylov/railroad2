package com.railroad.userfunctionstests;

import com.railroad.config.HibernateTestConfig;
import com.railroad.config.MailTestConfig;
import com.railroad.config.MqTestConfig;
import com.railroad.config.TestContextConfiguration;
import com.railroad.controllers.UserController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.servlet.http.HttpSession;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MqTestConfig.class, HibernateTestConfig.class, TestContextConfiguration.class,
        MailTestConfig.class})
@Sql(scripts = {"/reloadTestData.sql"})
public class FindDirectTrainsTest {

    @Autowired
    private UserController userController;

    @Mock
    private HttpSession httpSession;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void ifNoDirectRoutesShouldReturnEmptyList() {
        assertTrue(userController.findDirectTrains(httpSession, "SPB", "MSK",
                "21.01.2001", false).isEmpty());
    }

    @Test
    public void ifDirectRouteAvailableShouldReturnThem() {
        assertTrue(userController.findDirectTrains(httpSession, "SPB", "MSK",
                "19.07.2030", false).size() == 3);
        assertTrue(userController.findDirectTrains(httpSession, "SPB", "MSK",
                "19.07.2030", false).get(0).getTrainNumber().equals("SPB-ANAPA"));
    }

    @Test
    public void ifStationDoesNotExistsShouldReturnEmptyList() {
        assertTrue(userController.findDirectTrains(httpSession, "FOO", "MSK",
                "21.01.2001", false).isEmpty());
    }

    @Test
    public void ifIsItArrivingTimeCheckboxTrueShouldReturnExpectedList() {
        assertTrue(userController.findDirectTrains(httpSession, "SPB", "MSK",
                "20.07.2030", true).size() == 4);
        assertTrue(userController.findDirectTrains(httpSession, "SPB", "MSK",
                "20.07.2030", true).get(0).getTrainNumber().equals("SPB-ANAPA"));
    }
}
