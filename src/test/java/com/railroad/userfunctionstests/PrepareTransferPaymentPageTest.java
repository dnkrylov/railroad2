package com.railroad.userfunctionstests;


import com.railroad.config.HibernateTestConfig;
import com.railroad.config.MailTestConfig;
import com.railroad.config.MqTestConfig;
import com.railroad.config.TestContextConfiguration;
import com.railroad.controllers.UserController;
import com.railroad.domain.Ticket;
import com.railroad.domain.User;
import com.railroad.dto.UserDetailsDto;
import com.railroad.dto.user.TicketInfo;
import com.railroad.dto.user.TransferTicketInfo;
import com.railroad.exceptions.ThisPlaceIsAlreadyBusyException;
import com.railroad.repo.TicketRepo;
import com.railroad.response.BusinessResponse;
import com.railroad.service.SecurityService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MqTestConfig.class, HibernateTestConfig.class, TestContextConfiguration.class,
        MailTestConfig.class})
@Sql(scripts = {"/reloadTestData.sql"})
public class PrepareTransferPaymentPageTest {

    @Autowired
    private UserController userController;

    @Autowired
    private TicketRepo ticketRepo;

    @Mock
    private HttpSession httpSession;

    @Autowired
    private SecurityService securityService;

    private TransferTicketInfo transferTicketInfo;

    private UserDetailsDto userDetailsDto;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        transferTicketInfo = new TransferTicketInfo();
        transferTicketInfo.setStartStationName("SPB");
        transferTicketInfo.setMiddleStationName("MSK");
        transferTicketInfo.setEndStationName("ANAPA");
        userDetailsDto = new UserDetailsDto(new User());
        userDetailsDto.getUser().setId(1L);
    }

    @Test
    @Transactional
    public void tempTicketShouldBuUpgraded() {
        when(securityService.getLoggedInUser()).thenReturn(userDetailsDto);
        when(httpSession.getAttribute("transferTicketInfo")).thenReturn(transferTicketInfo);
        BusinessResponse response = userController.prepareTransferPaymentPage(
                httpSession, 19L, 1, 1,1);
        assertEquals("TICKET PRICE: 7000.0", response.getMessage());
        Ticket tempTicket1 = ticketRepo.findTicketById(transferTicketInfo.getTicketId1());
        Ticket tempTicket2 = ticketRepo.findTicketById(transferTicketInfo.getTicketId2());
        assertEquals(-1.00, tempTicket1.getPrice(), 0);
        assertEquals(-1.00, tempTicket2.getPrice(), 0);
        assertEquals((long) tempTicket1.getUser().getId(), transferTicketInfo.getUserId());
        assertEquals((long) tempTicket2.getUser().getId(), transferTicketInfo.getUserId());

        assertEquals((long) tempTicket1.getCarriage().getId(), transferTicketInfo.getCarriageId1());
        assertEquals((long) tempTicket2.getCarriage().getId(), transferTicketInfo.getCarriageId2());
        assertNull(tempTicket1.getStartStation());
        assertNull(tempTicket2.getFinishStation());
    }

    @Test
    @Transactional
    public void ifPlaceIsBusyShouldThrowError() {
        when(securityService.getLoggedInUser()).thenReturn(userDetailsDto);
        when(httpSession.getAttribute("transferTicketInfo")).thenReturn(transferTicketInfo);
        expectedException.expect(ThisPlaceIsAlreadyBusyException.class);
        expectedException.expectMessage("FROM MSK TO ANAPA");
        BusinessResponse response = userController.prepareTransferPaymentPage(
                httpSession, 19L, 2, 1,2);
    }
}
