package com.railroad.userfunctionstests;

import com.railroad.config.HibernateTestConfig;
import com.railroad.config.MailTestConfig;
import com.railroad.config.MqTestConfig;
import com.railroad.config.TestContextConfiguration;
import com.railroad.controllers.UserController;
import com.railroad.domain.User;
import com.railroad.domain.UserRole;
import com.railroad.dto.UserDetailsDto;
import com.railroad.dto.user.UserDto;
import com.railroad.service.SecurityService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MqTestConfig.class, HibernateTestConfig.class, TestContextConfiguration.class,
        MailTestConfig.class})
@Sql(scripts = {"/reloadTestData.sql"})
public class GetUserInfoTest {
    @Autowired
    private UserController userController;

    @Autowired
    private SecurityService securityService;

@Test
    public void shouldReturnCurrentUserDto() {
    User user = new User();
    user.setLogin("login");
    user.setDateOfBirth(LocalDate.now());
    UserDetailsDto userDto = new UserDetailsDto(user);
    when(securityService.getLoggedInUser()).thenReturn(userDto);
    assertEquals("login", userController.getUserInfo().getLogin());
}
}
