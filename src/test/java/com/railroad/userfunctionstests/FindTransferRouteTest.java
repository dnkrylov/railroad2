package com.railroad.userfunctionstests;

import com.railroad.config.HibernateTestConfig;
import com.railroad.config.MailTestConfig;
import com.railroad.config.MqTestConfig;
import com.railroad.config.TestContextConfiguration;
import com.railroad.controllers.UserController;
import com.railroad.dto.user.AvailableTransferRouteDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.servlet.http.HttpSession;

import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MqTestConfig.class, HibernateTestConfig.class, TestContextConfiguration.class,
        MailTestConfig.class})
@Sql(scripts = {"/reloadTestData.sql"})
public class FindTransferRouteTest {

    @Autowired
    private UserController userController;

    @Mock
    private HttpSession httpSession;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void ifNoTransferRoutesShouldReturnEmptyList() {
        assertTrue(userController.findTransferTrains(httpSession, "SPB", "MSK",
                "21.01.2001", false).isEmpty());
    }

    @Test
    public void ifTransferRouteAvailableShouldReturnThem() {
        List<AvailableTransferRouteDto> transferTrains = userController.findTransferTrains(httpSession, "SPB", "ANAPA",
                "19.07.2030", false);
        assertTrue(transferTrains.size() == 1);
        assertTrue(transferTrains.get(0).getMiddleStationName().equals("MSK"));
        assertTrue(transferTrains.get(0).getTrainNumber1().equals("SPB_ROSTOV"));
        assertTrue(transferTrains.get(0).getTrainNumber2().equals("SPB-ANAPA"));
    }

    @Test
    public void ifTransferRouteAvailableShouldReturnThemArrivingDate() {
        List<AvailableTransferRouteDto> transferTrains = userController.findTransferTrains(httpSession, "SPB", "ANAPA",
                "21.07.2030", true);
        assertTrue(transferTrains.size() == 1);
        assertTrue(transferTrains.get(0).getMiddleStationName().equals("MSK"));
        assertTrue(transferTrains.get(0).getTrainNumber1().equals("SPB_ROSTOV"));
        assertTrue(transferTrains.get(0).getTrainNumber2().equals("SPB-ANAPA"));
    }
}
