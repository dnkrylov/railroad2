package com.railroad.adminfunctiontests;

import com.railroad.config.MqTestConfig;
import com.railroad.config.HibernateTestConfig;
import com.railroad.config.MailTestConfig;
import com.railroad.config.TestContextConfiguration;
import com.railroad.controllers.AdminController;
import com.railroad.exceptions.InvalidRequestParamException;
import com.railroad.exceptions.SuchStationAlreadyExistsException;
import com.railroad.response.BusinessResponse;
import com.railroad.service.jms.JmsMessageSender;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MqTestConfig.class, HibernateTestConfig.class, TestContextConfiguration.class,
        MailTestConfig.class})
@Sql(scripts = {"/reloadTestData.sql"})
public class AddingStationTest {

    @Autowired
    private AdminController adminController;

    @Autowired
    private JmsMessageSender jmsMessageSender;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    @Transactional
    public void ifStationWithSameNameExistsShouldThrowError(){
        expectedException.expect(SuchStationAlreadyExistsException.class);
        expectedException.expectMessage("ALREADY EXISTS");
        adminController.saveStation("existingstation            ");
    }

    @Test
    @Transactional
    public void ifNameContainsOfSpacesShouldThrowError(){
        expectedException.expect(InvalidRequestParamException.class);
        expectedException.expectMessage("INVALID STATION NAME");
        adminController.saveStation("               ");
    }

    @Test
    @Transactional
    public void ifStationCreatedAndMessageSentWeGotMessage(){
        when(jmsMessageSender.sendMessage("TIMETABLE_CHANGED", "NOTEXISTINGSTATION"))
                .thenReturn(true);
        BusinessResponse response = adminController.saveStation("NotExistingStation");
        assertThat(response.getMessage(), containsString("SUCCESSFULLY ADDED"));
    }

    @Test
    @Transactional
    public void ifStationCreatedAndMessageNotSentWeGotMessage(){
        when(jmsMessageSender.sendMessage("TIMETABLE_CHANGED", "STATION_ADDED"))
                .thenReturn(false);
        BusinessResponse response = adminController.saveStation("NotExistingStation");
        assertThat(response.getMessage(), containsString("WAS NOT SENT"));
    }
}