package com.railroad.adminfunctiontests;

import com.railroad.config.HibernateTestConfig;
import com.railroad.config.MailTestConfig;
import com.railroad.config.MqTestConfig;
import com.railroad.config.TestContextConfiguration;
import com.railroad.controllers.AdminController;
import com.railroad.controllers.UserController;
import com.railroad.dto.admin.CarriageForTrainCreationDto;
import com.railroad.dto.admin.RouteCreationDto;
import com.railroad.dto.admin.TrainCreationDto;
import com.railroad.exceptions.InvalidRequestParamException;
import com.railroad.response.BusinessResponse;
import com.railroad.service.jms.JmsMessageSender;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {MqTestConfig.class, HibernateTestConfig.class, TestContextConfiguration.class,
        MailTestConfig.class})
@Sql(scripts = {"/reloadTestData.sql"})
public class AddingTrainTest {

    @Autowired
    private JmsMessageSender jmsMessageSender;

    @Autowired
    private AdminController adminController;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private TrainCreationDto trainCreationDto = new TrainCreationDto();

    @Before
    public void initDto() {
        trainCreationDto.setNumber("testTrainDto");
        RouteCreationDto station1 = new RouteCreationDto("SPB", "01.01.2030:23.30",
                "01.01.2030:23.50", "0", "0", "0");
        RouteCreationDto station2 = new RouteCreationDto("MSK", "02.01.2030:07.30",
                "02.01.2030:08.00", "100", "200", "300");
        trainCreationDto.setStationList(Arrays.asList(station1, station2));
        CarriageForTrainCreationDto carriage1 = new CarriageForTrainCreationDto("car1", "BUSINESS", "22");
        CarriageForTrainCreationDto carriage2 = new CarriageForTrainCreationDto("car2", "ECONOMY", "23");
        trainCreationDto.setCarriages(Arrays.asList(carriage1, carriage2));
        reset(jmsMessageSender);
    }

    @Test
    public void duplicateCarriageNumbersShouldThrowError() {
        expectedException.expect(InvalidRequestParamException.class);
        expectedException.expectMessage("duplicate carriage numbers");

        trainCreationDto.getCarriages().get(1).setCarName("car1");
        adminController.saveTrain(trainCreationDto);
    }

    @Test
    public void invalidCarTypeShouldThrowError() {
        expectedException.expect(InvalidRequestParamException.class);
        expectedException.expectMessage("invalid type value");

        trainCreationDto.getCarriages().get(1).setCarType("INVALID");
        adminController.saveTrain(trainCreationDto);
    }

    @Test
    public void duplicateStationShouldThrowError() {
        expectedException.expect(InvalidRequestParamException.class);
        expectedException.expectMessage("duplicate stations: SPB");

        trainCreationDto.getStationList().get(1).setStationName("SPB");
        adminController.saveTrain(trainCreationDto);
    }

    @Test
    public void departingFromPreviousStationAfterArrivingToNextShouldThrowError() {
        expectedException.expect(InvalidRequestParamException.class);
        expectedException.expectMessage("is before departing from station");

        trainCreationDto.getStationList().get(1).setArrivingTime("01.01.2030:23.49");
        adminController.saveTrain(trainCreationDto);
    }

    @Test
    public void arrivingToStationInPastShouldThrowError() {
        expectedException.expect(InvalidRequestParamException.class);
        expectedException.expectMessage("(PAST)");

        trainCreationDto.getStationList().get(0).setArrivingTime("01.01.2000:23.49");
        adminController.saveTrain(trainCreationDto);
    }

    @Test
    public void DepartingBeforeArrivingShouldThrowError() {
        expectedException.expect(InvalidRequestParamException.class);
        expectedException.expectMessage("departing before arriving");

        trainCreationDto.getStationList().get(0).setArrivingTime("01.01.2030:23.51");
        adminController.saveTrain(trainCreationDto);
    }

    @Test
    public void notExistingStationShouldThrowError() {
        expectedException.expect(InvalidRequestParamException.class);
        expectedException.expectMessage("station does not exist");

        trainCreationDto.getStationList().get(0).setStationName("NOTEXISTING");
        adminController.saveTrain(trainCreationDto);
    }

    @Test
    public void ifMessagesDontSentWeGotMessage() {
        trainCreationDto.getStationList().get(0).setArrivingTime(LocalDateTime.now().plusMinutes(11)
                .format(DateTimeFormatter.ofPattern("dd.MM.yyyy:HH.mm")));
        when(jmsMessageSender.sendMessage("TIMETABLE_CHANGED", "SPB")).thenReturn(false);

        BusinessResponse response = adminController.saveTrain(trainCreationDto);
        assertThat(response.getMessage(), containsString("WAS NOT SENT"));
    }

    @Test
    public void ifChangesAffectTodayTimeTableMessagesDontWillBeSent() {
        trainCreationDto.getStationList().get(0).setArrivingTime(LocalDateTime.now().plusMinutes(11)
                .format(DateTimeFormatter.ofPattern("dd.MM.yyyy:HH.mm")));
        adminController.saveTrain(trainCreationDto);
        verify(jmsMessageSender, times(1)).sendMessage(any(), any());
    }

    @Test
    public void ifChangesDontAffectTodayTimeTableMessagesDontWillNotSent() {
        adminController.saveTrain(trainCreationDto);
        verify(jmsMessageSender, times(0)).sendMessage(any(), any());
    }

    @Test
    public void ifEverythingIsOkWeGotMessage() {
        BusinessResponse response = adminController.saveTrain(trainCreationDto);
        assertThat(response.getMessage(), containsString("Train successfully created"));
    }

}
