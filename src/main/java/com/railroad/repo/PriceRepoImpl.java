package com.railroad.repo;

import com.railroad.domain.CarriageType;
import com.railroad.domain.PricePart;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

/**
 * Repository to save or get route price information
 */
@Repository
@RequiredArgsConstructor
@Slf4j
public class PriceRepoImpl implements PriceRepo {
    private final SessionFactory sessionFactory;

    /**
     * Search price for ticket to travel in given type carriage by given train between neighboring stations
     * @throws NullPointerException if price of such ticket is not exists in database
     */
    @Override
    @SuppressWarnings("unchecked")
    public double findPriceByTrainIdCarriageTypeStartStationIdEndStationId(
            long trainId, CarriageType carriageType, long startStationId, long endStationId) {
        log.info("SEARCHING PRICE BY TRAIN ID, CARRIAGE TYPE, START STATION ID, END STATION ID "
                + trainId + " " + carriageType + " " + startStationId + " " + endStationId);
        Session session = sessionFactory.getCurrentSession();
        Query<Double> query = session.createQuery("SELECT p.price FROM PricePart p " +
                "WHERE p.train.id = :trainId AND p.carriageType = :carriageType " +
                "AND p.startStation.id = :startStationId AND p.endStation.id = :endStationId");
        query.setParameter("trainId", trainId);
        query.setParameter("carriageType", carriageType);
        query.setParameter("startStationId", startStationId);
        query.setParameter("endStationId", endStationId);
        return query.list().get(0);
    }

    /**
     * Save given pricePart to database
     * @param pricePart
     */
    @Override
    public void addPricePart(PricePart pricePart) {
        log.info("ADDING PRICE PART TRAINID: " + pricePart.getTrain().getId() + " "
                + pricePart.getStartStation() + "-" + pricePart.getEndStation());
        Session session = sessionFactory.getCurrentSession();
        session.save(pricePart);
    }
}
