package com.railroad.repo;

import com.railroad.domain.Ticket;
import com.railroad.dto.admin.UserForAdminRequestDto;
import com.railroad.dto.user.TicketDetailsDto;
import com.railroad.dto.user.TicketPreviewDto;
import com.railroad.response.BusinessResponse;

import java.util.List;

public interface TicketRepo {
    List<Ticket> findTicketByCarriageIdAndPlaceNumber(long carriageId, int placeNumber);
    List<Ticket> findTicketsByCarriageIdAndUserId(long carriageId, long userId);
    long addTemporaryTicket(Ticket ticket);
    void deleteTrashTickets(long carriageId);
    BusinessResponse upgradeTemporaryTicket(Ticket ticket);
    void deleteTemporaryTicketsForCurrentUser(long userId);
    List<Ticket> findTicketByIdForUpgrade(long id);
    List<UserForAdminRequestDto> findTicketsByCarriageIds(List<Long> carriageIds);
    List<TicketPreviewDto> getTicketsPreview(long userId);
    TicketDetailsDto getTicketDetails(long ticketId);
    Ticket findTicketById(long ticketId);
}
