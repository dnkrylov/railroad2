package com.railroad.repo;

import com.railroad.domain.Carriage;
import com.railroad.domain.Train;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Repository to add and search information about trains and carriages
 */
@Repository
@RequiredArgsConstructor
@Slf4j
public class TrainRepoImpl implements TrainRepo {

    private final SessionFactory sessionFactory;

    /**
     * Save new train
     */
    @Override
    public long addTrain(Train train) {
        log.info("ADDING TRAIN");
        Session session = sessionFactory.getCurrentSession();
        return (long) session.save(train);
    }

    /**
     * Save ne carriage
     */
    @Override
    public long addCarriage(Carriage carriage) {
        log.info("ADDING CARRIAGE");
        Session session = sessionFactory.getCurrentSession();
        return (long) session.save(carriage);
    }

    /**
     * Find all carriages of train with given ID
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Carriage> findCarriagesByTrainId(long trainId) {
        log.info("SEARCHING CARRIAGES BY TRAIN ID: " + trainId);
        Session session = sessionFactory.getCurrentSession();
        Query<Carriage> query = session.createQuery("FROM Carriage c WHERE c.train.id = :trainId");
        query.setParameter("trainId", trainId);
        return query.list();
    }

    /**
     * Find id of train, which contains carriage with given ID
     */
    @Override
    @SuppressWarnings("unchecked")
    public long findTrainIdByCarriageId(long carriageId) {
        log.info("SEARCHING TRAIN ID BY CARRIAGE ID: " + carriageId);
        Session session = sessionFactory.getCurrentSession();
        Query<Long> query = session.createQuery("select train.id FROM Carriage c WHERE c.id = :carriageId");
        query.setParameter("carriageId", carriageId);
        return query.list().get(0);
    }

    /**
     * Find carriage with given ID
     */
    @Override
    public Carriage findCarriageById(long carriageId) {
        log.info("SEARCHING CARRIAGE BY ID: " + carriageId);
        Session session = sessionFactory.getCurrentSession();
        return session.get(Carriage.class, carriageId);
    }
}
