package com.railroad.repo;

import com.railroad.domain.Station;
import java.util.List;

public interface StationRepo {
    Station findStationById(long id);
    List<Station> findAllStations();
    Station findStationByName(String name);
    long addStation(Station station);
}
