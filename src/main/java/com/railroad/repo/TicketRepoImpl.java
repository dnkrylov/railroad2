package com.railroad.repo;

import com.railroad.domain.Ticket;
import com.railroad.dto.admin.UserForAdminRequestDto;
import com.railroad.dto.user.TicketDetailsDto;
import com.railroad.dto.user.TicketPreviewDto;
import com.railroad.response.BusinessResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import javax.persistence.LockModeType;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Repository to add and search information about tickets
 */
@Repository
@RequiredArgsConstructor
@Slf4j
public class TicketRepoImpl implements TicketRepo {

    private final SessionFactory sessionFactory;

    /**
     * Search for tickets to given place in given carriages (for all route). If no tickets found-return empty list
     * @param carriageId carriage id
     * @param placeNumber place number in given carriage
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Ticket> findTicketByCarriageIdAndPlaceNumber(long carriageId, int placeNumber) {
        log.info("SEARCHING TICKET BY CARRIAGE ID, PLACE NUMBER: " + carriageId + " " + placeNumber);
        Session session = sessionFactory.getCurrentSession();
        Query<Ticket> query = session.createQuery("FROM Ticket t WHERE t.carriage.id = :carriageId " +
                "AND t.placeNumber = :placeNumber");
        query.setParameter("carriageId", carriageId);
        query.setParameter("placeNumber", placeNumber);
        return query.list();
    }

    /**
     * Search for tickets to given carriage bought/booked by given user
     * @param carriageId carriage id for ticket searching
     * @param userId given user id
     * @return
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Ticket> findTicketsByCarriageIdAndUserId(long carriageId, long userId) {
        log.info("SEARCHING TICKET BY CARRIAGE ID, USER ID: " + carriageId + " " + userId);
        Session session = sessionFactory.getCurrentSession();
        Query<Ticket> query = session.createQuery("FROM Ticket t WHERE t.carriage.id = :carriageId " +
                "AND t.user.id = :userId");
        query.setParameter("carriageId", carriageId);
        query.setParameter("userId", userId);
        return query.list();
    }

    /**
     * Save a ticket
     */
    @Override
    @SuppressWarnings("unchecked")
    public long addTemporaryTicket(Ticket ticket) {
        log.info("ADDING TEMPORARY TICKET: USER/CARRIAGE/PLACE" +
                ticket.getUser().getId() + "/" + ticket.getCarriage().getId() + "/" + ticket.getPlaceNumber());
        Session session = sessionFactory.getCurrentSession();
        return (long) session.save(ticket);
    }

    /**
     * Delete all ticket in given carriage with creation time before 10 minutes ago and price < 0
     * @param carriageId Carriage id
     */
    @Override
    @SuppressWarnings("unchecked")
    public void deleteTrashTickets(long carriageId) {
        log.info("DELETING TRASH TICKETS FOR CARRIAGE: " + carriageId);
        Session session = sessionFactory.getCurrentSession();
        LocalDateTime validTime = LocalDateTime.now().minusMinutes(10);
        Query<Ticket> query = session.createQuery("DELETE Ticket t WHERE t.carriage.id = :carriageId " +
                "AND t.creationTime < :validTime AND t.price < 0");
        query.setParameter("carriageId", carriageId);
        query.setParameter("validTime", validTime);
        query.executeUpdate();
    }

    /**
     * Updates ticket with given id, sets given start station, end station, price and current creationTime
     */
    @Override
    @SuppressWarnings("unchecked")
    public BusinessResponse upgradeTemporaryTicket(Ticket ticket) {
        log.info("UPGRADING TEMPORARY TICKET: " + ticket.getId());
        Session session = sessionFactory.getCurrentSession();
        Query<Ticket> query = session.createQuery("UPDATE Ticket t SET t.startStation = :startStation, " +
                "t.finishStation = :finishStation, t.price = :price, t.creationTime = :creationTime " +
                "WHERE t.id = :id");
        query.setParameter("startStation", ticket.getStartStation());
        query.setParameter("finishStation", ticket.getFinishStation());
        query.setParameter("price", ticket.getPrice());
        query.setParameter("creationTime", LocalDateTime.now());
        query.setParameter("id", ticket.getId());
        query.executeUpdate();
        return new BusinessResponse("ticket was successfully bought");
    }

    /**
     * Delete all ticket for user with given id
     */
    @Override
    @SuppressWarnings("unchecked")
    public void deleteTemporaryTicketsForCurrentUser(long userId) {
        log.info("DELETING TEMPORARY TICKETS FOR USER: " + userId);
        Session session = sessionFactory.getCurrentSession();
        Query<Ticket> query = session.createQuery("DELETE Ticket t WHERE t.user.id = :userId " +
                "AND t.price < 0");
        query.setParameter("userId", userId);
        query.executeUpdate();
    }

    /**
     * Search ticket with given ID and adds a lock for it in database to prevent deleting or changing by
     * other transactions
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Ticket> findTicketByIdForUpgrade(long id) {
        log.info("SEARCHING TICKET BY ID FOR UPGRADE: " + id);
        Session session = sessionFactory.getCurrentSession();
        Query<Ticket> query = session.createQuery("FROM Ticket t WHERE t.id = :id");
        query.setParameter("id", id);
        query.setLockMode(LockModeType.PESSIMISTIC_WRITE);
        return query.list();
    }

    /**
     * Search information about tickets and their owners in carriages with given IDs.
     * @param carriageIds - list of carriages id
     * @return list of UserForAdminRequestDto object, containing information about tickets and users,
     * which have bought them
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<UserForAdminRequestDto> findTicketsByCarriageIds(List<Long> carriageIds) {
        log.info("SEARCHING TICKETS FOR LIST OF CARRIAGE IDS: " + carriageIds);
        List<UserForAdminRequestDto> result = new ArrayList<>();
        Session session = sessionFactory.getCurrentSession();
        for (long carriageId : carriageIds) {
            Query<UserForAdminRequestDto> query = session.createQuery(
                    "SELECT new com.railroad.dto.admin.UserForAdminRequestDto" +
                            " (u.login, u.firstName, u.lastName, " +
                            " t.placeNumber, t.startStation.name, t.finishStation.name, t.price, c.number)" +
                            " FROM Ticket t INNER JOIN User u ON u.id = t.user.id " +
                            " INNER JOIN Carriage c ON c.id = t.carriage.id" +
                            " WHERE t.carriage.id = :carriageId AND t.price > 0");
            query.setParameter("carriageId", carriageId);
            result.addAll(query.list());
        }
        return result;
    }

    /**
     * Searches for preview information about all tickets, bought by user with given ID
     * @return List of TicketPreviewDto objects
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<TicketPreviewDto> getTicketsPreview(long userId) {
        log.info("GETTING TICKETS PREVIEW FOR USER: " + userId);
        Session session = sessionFactory.getCurrentSession();
        Query<TicketPreviewDto> query = session.createQuery("SELECT new com.railroad.dto.user.TicketPreviewDto(" +
                "t.id, t.startStation.name, t.finishStation.name, r.departureTime)" +
                " FROM Ticket t INNER JOIN RouteNode r ON t.carriage.train = r.train " +
                "WHERE t.startStation = r.station AND t.user.id = :userId AND t.price > 0"
        );
        query.setParameter("userId", userId);
        return query.list();
    }

    /**
     * Searches for detailed information about ticket with given id and user, which have bought this ticket
     * @return TicketDetailsDto objects
     */
    @Override
    @SuppressWarnings("unchecked")
    public TicketDetailsDto getTicketDetails(long ticketId) {
        log.info("GETTING TICKETS DETAILS FOR TICKET: " + ticketId);
        Session session = sessionFactory.getCurrentSession();
        Query<TicketDetailsDto> query = session.createQuery("SELECT new com.railroad.dto.user.TicketDetailsDto(" +
                "t.id, t.user.firstName, t.user.lastName, t.startStation.name, t.finishStation.name, " +
                "t.carriage.train.name, t.carriage.number, t.placeNumber, r1.departureTime, r2.arrivalTime, t.price) " +
                "FROM Ticket t " +
                "INNER JOIN RouteNode r1 ON t.carriage.train = r1.train " +
                "INNER JOIN RouteNode r2 ON t.carriage.train = r2.train " +
                "WHERE t.startStation = r1.station AND t.finishStation = r2.station AND t.id = :ticketId");
        query.setParameter("ticketId", ticketId);
        List<TicketDetailsDto> list = query.list();
        return list.isEmpty() ? null : list.get(0);
    }

    /**
     * Search ticket with given ID
     */
    @Override
    public Ticket findTicketById(long ticketId) {
        log.info("SEARCHING TICKET BY ID: " + ticketId);
        Session session = sessionFactory.getCurrentSession();
        return session.get(Ticket.class, ticketId);
    }
}
