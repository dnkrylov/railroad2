package com.railroad.repo;

import com.railroad.domain.Carriage;
import com.railroad.domain.Train;
import java.util.List;

public interface TrainRepo {
    long addTrain(Train train);
    long addCarriage(Carriage carriage);
    List<Carriage> findCarriagesByTrainId(long trainId);
    long findTrainIdByCarriageId(long carriageId);
    Carriage findCarriageById(long carriageId);
}
