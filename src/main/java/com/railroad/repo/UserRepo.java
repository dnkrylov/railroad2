package com.railroad.repo;

import com.railroad.domain.User;
import com.railroad.domain.VerificationToken;

public interface UserRepo {
    long addUser(User user);
    User findUserById(long id);
    User findUserByLogin(String phone);
    User findUserByPhone(String phone);
    User findUserByEmail(String email);
    void updateUser(User user);
    void addVerificationToken(VerificationToken verificationToken);
    VerificationToken getVerificationToken(String token);
}
