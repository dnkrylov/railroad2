package com.railroad.repo;

import com.railroad.domain.User;
import com.railroad.domain.VerificationToken;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository to add and search information about users
 */
@Repository
@RequiredArgsConstructor
@Slf4j
public class UserRepoImpl implements UserRepo {

    private final SessionFactory sessionFactory;

    /**
     * Saving new user
     * @throws ConstraintViolationException if user with same login, email or phone exists in database
     */
    @Override
    public long addUser(User user) {
        log.info("ADDING USER: " + user);
        Session session = sessionFactory.getCurrentSession();
        return (long) session.save(user);
    }

    /**
     * Save new verification token
     */
    @Override
    public void addVerificationToken(VerificationToken verificationToken) {
        log.info("ADDING VERIFICATION TOKEN: " + verificationToken);
        Session session = sessionFactory.getCurrentSession();
        session.save(verificationToken);
    }

    /**
     * Find user with given ID
     */
    @Override
    public User findUserById(long id) {
        log.info("SEARCHING USER BY ID: " + id);
        Session session = sessionFactory.getCurrentSession();
        return session.get(User.class, id);
    }

    /**
     * Find user with given login
     */
    @Override
    @SuppressWarnings("unchecked")
    public User findUserByLogin(String login) {
        log.info("SEARCHING USER BY LOGIN: " + login);
        Session session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery("FROM User u WHERE u.login = :login");
        query.setParameter("login", login);
        List<User> users = query.list();
        return users.isEmpty() ? null : users.get(0);
    }

    /**
     * Find user with given phone
     */
    @Override
    @SuppressWarnings("unchecked")
    public User findUserByPhone(String phone) {
        log.info("SEARCHING USER BY PHONE: " + phone);
        Session session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery("FROM User u WHERE u.phone = :phone");
        query.setParameter("phone", phone);
        List<User> users = query.getResultList();
        return users.isEmpty() ? null : users.get(0);
    }

    /**
     * Find user with given email
     */
    @Override
    @SuppressWarnings("unchecked")
    public User findUserByEmail(String email) {
        log.info("SEARCHING USER BY EMAIL: " + email);
        Session session = sessionFactory.getCurrentSession();
        Query<User> query = session.createQuery("FROM User u WHERE u.email = :email");
        query.setParameter("email", email);
        List<User> users = query.getResultList();
        return users.isEmpty() ? null : users.get(0);
    }

    /**
     * Update user
     * @throws ConstraintViolationException if user with same login, email or phone exists in database
     */
    @Override
    public void updateUser(User user) {
        log.info("UPDATING USER: " + user);
        Session session = sessionFactory.getCurrentSession();
        session.update(user);
    }

    /**
     * Find verification token object by given token values
     */
    @Override
    @SuppressWarnings("unchecked")
    public VerificationToken getVerificationToken(String token) {
        log.info("SEARCHING VERIFICATION TOKEN");
        Session session = sessionFactory.getCurrentSession();
        Query<VerificationToken> query = session.createQuery("FROM VerificationToken v WHERE v.token = :token");
        query.setParameter("token", token);
        List<VerificationToken> tokens = query.getResultList();
        return tokens.isEmpty() ? null : tokens.get(0);
    }
}
