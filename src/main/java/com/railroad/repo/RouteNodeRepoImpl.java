package com.railroad.repo;

import com.railroad.domain.RouteNode;
import com.railroad.domain.Station;
import com.railroad.dto.user.AvailableDirectRouteDto;
import com.railroad.dto.user.AvailableTransferRouteDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Repository to add and search information about train routes
 */
@Repository
@RequiredArgsConstructor
@Slf4j
public class RouteNodeRepoImpl implements RouteNodeRepo {

    private final SessionFactory sessionFactory;

    /**
     * Search for routeNodes by given station and date. Used for timetable rows creation
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<RouteNode> findRouteNodesByStationAndDate(Station station, LocalDate date) {
        log.info("SEARCHING ROUTE NODE BY STATION AND DATE " + station + " " + date);
        Session session = sessionFactory.getCurrentSession();
        Query<RouteNode> query = session.createQuery("FROM RouteNode r WHERE r.station = :station " +
                "AND (r.arrivalTime BETWEEN :startDate AND :endDate OR r.departureTime BETWEEN :startDate AND :endDate)" +
                " ORDER BY r.departureTime");
        query.setParameter("station", station);
        query.setParameter("startDate", date.atStartOfDay());
        query.setParameter("endDate", date.plusDays(1).atStartOfDay());
        return query.list();
    }

    /**
     * Search for direct trains between two given stations, which departed from start station at given date
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<AvailableDirectRouteDto> findDirectTrainsByStartStationEndStationAndStartDate(
            String startStationName, String endStationName, LocalDate date) {
        log.info("SEARCHING DIRECT TRAINS BY START STATION, END STATION, START DATE "
                + startStationName + " " + endStationName + " " + date);
        Session session = sessionFactory.getCurrentSession();
        Query<AvailableDirectRouteDto> query = session.createQuery(
                "SELECT new com.railroad.dto.user.AvailableDirectRouteDto(" +
                        "startNode.departureTime, startNode.train.name, startNode.train.id, endNode.arrivalTime)" +
                        " FROM RouteNode startNode INNER JOIN RouteNode endNode ON startNode.train = endNode.train" +
                        " WHERE startNode.station.name = :startStationName AND endNode.station.name = :endStationName" +
                        " AND startNode.departureTime BETWEEN :startTime AND :endTime" +
                        " AND startNode.departureTime < endNode.arrivalTime ORDER BY startNode.departureTime");
        query.setParameter("startStationName", startStationName);
        query.setParameter("endStationName", endStationName);
        query.setParameter("startTime", date.atStartOfDay());
        query.setParameter("endTime", date.plusDays(1).atStartOfDay());
        return query.list();
    }

    /**
     * Search for direct trains between two given stations, which arriving to finish station at given date
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<AvailableDirectRouteDto> findDirectTrainsByStartStationEndStationAndFinishDate(
            String startStationName, String endStationName, LocalDate date) {
        log.info("SEARCHING DIRECT TRAINS BY START STATION, END STATION, END DATE "
                + startStationName + " " + endStationName + " " + date);
        Session session = sessionFactory.getCurrentSession();
        Query<AvailableDirectRouteDto> query = session.createQuery(
                "SELECT new com.railroad.dto.user.AvailableDirectRouteDto( " +
                        "startNode.departureTime, startNode.train.name, startNode.train.id, endNode.arrivalTime) " +
                        "FROM RouteNode startNode INNER JOIN RouteNode endNode ON startNode.train = endNode.train " +
                        "WHERE startNode.station.name = :startStationName AND endNode.station.name = :endStationName " +
                        "AND endNode.arrivalTime BETWEEN :startTime AND :endTime " +
                        "AND startNode.departureTime < endNode.arrivalTime ORDER BY startNode.departureTime");
        query.setParameter("startStationName", startStationName);
        query.setParameter("endStationName", endStationName);
        query.setParameter("startTime", date.atStartOfDay());
        query.setParameter("endTime", date.plusDays(1).atStartOfDay());
        return query.list();
    }

    /**
     * Search for transfer route between two given stations, with trip starts at given date
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<AvailableTransferRouteDto> findTransferTrainsByStartStationEndStationAndStartDate(
            String startStationName, String endStationName, LocalDate date) {
        log.info("SEARCHING TRANSFER TRAINS BY START STATION, END STATION, START DATE "
                + startStationName + " " + endStationName + " " + date);
        Session session = sessionFactory.getCurrentSession();
        Query<AvailableTransferRouteDto> query = session.createQuery(
                "SELECT new com.railroad.dto.user.AvailableTransferRouteDto(" +
                        "startNode1.departureTime, startNode1.train.name, startNode1.train.id, endNode1.arrivalTime, " +
                        "startNode2.departureTime, startNode2.train.name, startNode2.train.id, endNode2.arrivalTime, " +
                        "endNode1.station.name) " +
                        "FROM RouteNode startNode1 INNER JOIN RouteNode endNode1 ON startNode1.train = endNode1.train " +
                        "INNER JOIN RouteNode startNode2 ON endNode1.station = startNode2.station " +
                        "INNER JOIN RouteNode endNode2 ON startNode2.train = endNode2.train " +
                        "WHERE startNode1.station.name = :startStationName " +
                        "AND endNode2.station.name = :endStationName " +
                        "AND startNode1.departureTime BETWEEN :startTime AND :endTime " +
                        "AND startNode1.departureTime < endNode1.arrivalTime " +
                        "AND startNode2.departureTime < endNode2.arrivalTime " +
                        "AND endNode1.arrivalTime < startNode2.departureTime " +
                        "AND startNode1.train != startNode2.train ORDER BY startNode1.departureTime");
        query.setParameter("startStationName", startStationName);
        query.setParameter("endStationName", endStationName);
        query.setParameter("startTime", date.atStartOfDay());
        query.setParameter("endTime", date.plusDays(1).atStartOfDay());
        return query.list();
    }

    /**
     * Search for transfer route between two given stations, with trip ends at given date
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<AvailableTransferRouteDto> findTransferTrainsByStartStationEndStationAndFinishDate(
            String startStationName, String endStationName, LocalDate date) {
        log.info("SEARCHING TRANSFER TRAINS BY START STATION, END STATION, END DATE "
                + startStationName + " " + endStationName + " " + date);
        Session session = sessionFactory.getCurrentSession();
        Query<AvailableTransferRouteDto> query = session.createQuery(
                "SELECT new com.railroad.dto.user.AvailableTransferRouteDto(" +
                        "startNode1.departureTime, startNode1.train.name, startNode1.train.id, endNode1.arrivalTime, " +
                        "startNode2.departureTime, startNode2.train.name, startNode2.train.id, endNode2.arrivalTime, " +
                        "endNode1.station.name) " +
                        "FROM RouteNode startNode1 INNER JOIN RouteNode endNode1 ON startNode1.train = endNode1.train " +
                        "INNER JOIN RouteNode startNode2 ON endNode1.station = startNode2.station " +
                        "INNER JOIN RouteNode endNode2 ON startNode2.train = endNode2.train " +
                        "WHERE startNode1.station.name = :startStationName " +
                        "AND endNode2.station.name = :endStationName " +
                        "AND endNode2.arrivalTime BETWEEN :startTime AND :endTime " +
                        "AND startNode1.departureTime < endNode1.arrivalTime " +
                        "AND startNode2.departureTime < endNode2.arrivalTime " +
                        "AND endNode1.arrivalTime < startNode2.departureTime " +
                        "AND startNode1.train != startNode2.train ORDER BY startNode1.departureTime");
        query.setParameter("startStationName", startStationName);
        query.setParameter("endStationName", endStationName);
        query.setParameter("startTime", date.atStartOfDay());
        query.setParameter("endTime", date.plusDays(1).atStartOfDay());
        return query.list();
    }

    /**
     * Save new RouteNode to database
     */
    @Override
    public long addRouteNode(RouteNode routeNode) {
        log.info("ADDING ROUTE NODE TRAIN/STATION:" + routeNode.getTrain().getId() + "/" +
                routeNode.getStation().getId());
        Session session = sessionFactory.getCurrentSession();
        return (long) session.save(routeNode);
    }

    /**
     * Search sorted by time station ids for train with given id
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Long> findSortedStationIdsByTrainId(long trainId) {
        log.info("SEARCHING SORTED STATION LIST FOR TRAIN " + trainId);
        Session session = sessionFactory.getCurrentSession();
        Query<Long> query = session.createQuery("SELECT r.station.id FROM RouteNode r " +
                "WHERE r.train.id = :trainId ORDER BY r.departureTime");
        query.setParameter("trainId", trainId);
        return query.list();
    }

    /**
     * Search departure time from given station for given train
     */
    @Override
    @SuppressWarnings("unchecked")
    public LocalDateTime findDepartureTimeByStationIdAndTrainId(long stationId, long trainId) {
        log.info("FIND DEPARTURE TIME BY STATION ID, TRAIN ID: " + stationId + " " + trainId);
        Session session = sessionFactory.getCurrentSession();
        Query<RouteNode> query = session.createQuery("FROM RouteNode r " +
                "WHERE r.train.id = :trainId AND r.station.id = :stationId");
        query.setParameter("trainId", trainId);
        query.setParameter("stationId", stationId);
        return query.list().get(0).getDepartureTime();
    }
}
