package com.railroad.repo;

import com.railroad.domain.Station;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository to add and search information about stations
 */
@Repository
@RequiredArgsConstructor
@Slf4j
public class StationRepoImpl implements StationRepo {

    private final SessionFactory sessionFactory;

    /**
     * Add a new station with given name to database
     * @param station new Station
     * @throws ConstraintViolationException if station with same name already exists in database
     */
    @Override
    public long addStation(Station station) {
        log.info("ADDING STATION: " + station.getName());
        Session session = sessionFactory.getCurrentSession();
        return (long) session.save(station);
    }

    /**
     * Search station with given id
     * @param id station id
     */
    @Override
    public Station findStationById(long id) {
        log.info("SEARCHING STATION BY ID: " + id);
        Session session = sessionFactory.getCurrentSession();
        return session.get(Station.class, id);
    }

    /**
     * Search for all existing stations
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Station> findAllStations() {
        log.info("GETTING ALL STATIONS");
        Session session = sessionFactory.getCurrentSession();
        Query<Station> query = session.createQuery("FROM Station");
        return query.getResultList();
    }

    /**
     * Search station with given name
     * @param name station name
     * @return station if station with same name exists
     * @return null if station with same name does not exists
     */
    @Override
    @SuppressWarnings("unchecked")
    public Station findStationByName(String name) {
        log.info("SEARCHING STATION BY NAME: " + name);
        Session session = sessionFactory.getCurrentSession();
        Query<Station> query = session.createQuery("FROM Station s WHERE s.name = :name");
        query.setParameter("name", name);
        List<Station> stations = query.getResultList();
        return stations.isEmpty() ? null : stations.get(0);
    }
}
