package com.railroad.repo;

import com.railroad.domain.RouteNode;
import com.railroad.domain.Station;
import com.railroad.dto.user.AvailableDirectRouteDto;
import com.railroad.dto.user.AvailableTransferRouteDto;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface RouteNodeRepo {
    List<RouteNode> findRouteNodesByStationAndDate(Station station, LocalDate date);
    List<AvailableDirectRouteDto> findDirectTrainsByStartStationEndStationAndStartDate(
            String startStationName, String endStationName, LocalDate date);
    List<AvailableDirectRouteDto> findDirectTrainsByStartStationEndStationAndFinishDate(
            String startStationName, String endStationName, LocalDate date);
    long addRouteNode(RouteNode routeNode);
    List<Long> findSortedStationIdsByTrainId(long trainId);
    LocalDateTime findDepartureTimeByStationIdAndTrainId(long startStationId, long trainId);
    List<AvailableTransferRouteDto> findTransferTrainsByStartStationEndStationAndStartDate(
            String startStationName, String endStationName, LocalDate date);
    List<AvailableTransferRouteDto> findTransferTrainsByStartStationEndStationAndFinishDate(
            String startStationName, String endStationName, LocalDate date);
}
