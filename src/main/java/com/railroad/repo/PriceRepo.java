package com.railroad.repo;


import com.railroad.domain.CarriageType;
import com.railroad.domain.PricePart;

public interface PriceRepo {
    double findPriceByTrainIdCarriageTypeStartStationIdEndStationId(
            long trainId, CarriageType carriageType, long startStationId, long endStationId);
    void addPricePart(PricePart pricePart);
}
