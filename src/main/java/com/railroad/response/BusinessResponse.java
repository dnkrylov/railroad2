package com.railroad.response;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * Response for business logic operations
 */
@Data
@Slf4j
public class BusinessResponse {
    private String message;

    public BusinessResponse(String message) {
        log.info("BUSINESS RESPONSE: " + message);
        this.message = message;
    }
}
