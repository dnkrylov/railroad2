package com.railroad.config;

import com.railroad.exceptions.CustomAuthenticationFailureHandler;
import com.railroad.service.UserDetailsServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;

/**
 * WebSecurity configuration class
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * Creates bean, implementation for UserDetailsService
     */
    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        return new UserDetailsServiceImpl();
    }

    /**
     * Custom AuthenticationFailureHandler for this application
     */

    @Bean
    public AuthenticationFailureHandler authenticationFailureHandler() {
        return new CustomAuthenticationFailureHandler();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService());
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth){
        auth.authenticationProvider(authenticationProvider());
    }

    /**
     * Configuration of request patterns for different roles
     * Setting custom AuthenticationFailureHandler, login and logout URL
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.exceptionHandling().authenticationEntryPoint(new Http403ForbiddenEntryPoint());
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/admin/**").hasRole("ADMIN")
                .antMatchers("/registered/**").hasAnyRole("ADMIN", "USER")
                .antMatchers("/unregistered/**", "/resources/**", "/").permitAll()
                .anyRequest().authenticated()

                .and()
                .formLogin()
                .failureHandler(authenticationFailureHandler())
                .loginProcessingUrl("/unregistered/tryToLogin")
                .defaultSuccessUrl("/registered/afterLogin", true)

                .and()
                .exceptionHandling().accessDeniedPage("/")

                .and()
                .logout()
                .logoutUrl("/unregistered/tryToLogout")
                .logoutSuccessUrl("/unregistered/logout")
                .deleteCookies("JSESSIONID");
    }
}

