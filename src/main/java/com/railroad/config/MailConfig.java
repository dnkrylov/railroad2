package com.railroad.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

/**
 * Configuration of email sending services
 * Uses application.properties file to get
 */
@Configuration
@PropertySource("classpath:application.properties")
public class MailConfig {
    @Value("${mail.mailFromAddress}")
    private String mailFromAddress;

    @Value("${mail.mailFromPassword}")
    private String mailFromPassword;

    @Value("${mail.host}")
    private String host;

    @Value("${mail.port}")
    private int port;

    /**
     * Configuration of JavaMailSenderBean
     */
    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(host);
        mailSender.setPort(port);
        mailSender.setUsername(mailFromAddress);
        mailSender.setPassword(mailFromPassword);
        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");
        return mailSender;
    }
}
