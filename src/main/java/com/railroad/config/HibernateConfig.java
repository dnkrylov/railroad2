package com.railroad.config;

import com.google.common.base.Preconditions;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Configuration of persistence layer.
 * Produces DataSource bean, which used id all repositories in application
 * Uses properties from application.properties file
 */
@Configuration
@PropertySource("classpath:application.properties")
@EnableTransactionManagement
public class HibernateConfig {

    @Value("${db.driver}")
    private String dbDriver;

    @Value("${db.url}")
    private String dbUrl;

    @Value("${db.username}")
    private String dbUserName;

    @Value("${db.password}")
    private String dbPassword;

    @Value("${hibernate.show_sql}")
    private Boolean showSql;

    /**
     * @throws NullPointerException if db.driver, db.db.url, db.username or db.password
     * properties are  missing
     * @return BasicDataSource
     */
    @Bean
    public DataSource dataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(Preconditions.checkNotNull(dbDriver));
        dataSource.setUrl(Preconditions.checkNotNull(dbUrl));
        dataSource.setUsername(Preconditions.checkNotNull(dbUserName));
        dataSource.setPassword(Preconditions.checkNotNull(dbPassword));
        return dataSource;
    }

    /**
     * Defines packages to scan for Entities, inject Datasource and applies Hibernate properties
     * @return LocalSessionFactoryBean that uses in application repositories
     */
    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan("com.railroad.domain");
        sessionFactory.setHibernateProperties(hibernateProperties());
        return sessionFactory;
    }

    /**
     * Creates HibernateTransactionManager and injects SessionFactory
     * @return HibernateTransactionManager
     */
    @Bean
    public PlatformTransactionManager hibernateTransactionManager() {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory().getObject());
        return transactionManager;
    }

    /**
     * Creates Properties which used by sessionFactory
     * @throws IllegalArgumentException if hibernate.show_sql is missed or empty
     */
    private Properties hibernateProperties() {
        Properties hibernateProperties = new Properties();
        hibernateProperties.put("hibernate.show_sql", showSql);
        return hibernateProperties;
    }
}
