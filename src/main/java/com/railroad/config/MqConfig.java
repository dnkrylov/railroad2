package com.railroad.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Configuration of MQ services
 * Uses application.properties file to get
 */
@Configuration
@PropertySource("classpath:application.properties")
public class MqConfig {
    @Value("${broker.uri}")
    private String wireLevelEndpoint;

    @Value("${broker.username}")
    private String jmsUser;

    @Value("${broker.password}")
    private String jmsPassword;

    /**
     * Creates and configure PolledConnectionFactory
     * @throws IllegalArgumentException if broker.uri, broker.username or broker.password
     * properties missing
     */
    @Bean
    public PooledConnectionFactory pooledConnectionFactory(){
        ActiveMQConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory(wireLevelEndpoint);
        connectionFactory.setUserName(jmsUser);
        connectionFactory.setPassword(jmsPassword);
        PooledConnectionFactory pooledConnectionFactory =
                new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(connectionFactory);
        pooledConnectionFactory.setMaxConnections(10);
        return pooledConnectionFactory;
    }
}
