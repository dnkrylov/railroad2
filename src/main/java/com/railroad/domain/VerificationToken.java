package com.railroad.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Verification token entity. References to User entity. Contains generated token for email verification
 */
@Entity
@Table(name = "js_verification_token")
@Data
@NoArgsConstructor
public class VerificationToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "token_id", unique = true, nullable = false)
    private Long id;

    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    @Column(name = "token_token")
    private String token;

    @Column(name = "token_expire_date")
    private LocalDateTime expireDateTime;

    private LocalDateTime calculateExpireDate() {
        return LocalDateTime.now().plusMinutes(1440);
    }

    public VerificationToken(User user, String token) {
        this.user = user;
        this.token = token;
        this.expireDateTime = calculateExpireDate();
    }
}
