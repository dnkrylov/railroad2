package com.railroad.domain;

import lombok.Data;

import javax.persistence.*;
/**
 * PricePart entity. References to Train entity by "Train" field and "Station" entities by
 * startStation and endStation fields
 * Contains information about price for current carriage type and 2 NEIGHBORING stations for current train
 */
@Data
@Entity
@Table(name = "js_price")
public class PricePart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "price_part_id")
    private Long id;
    @Column(name = "carriage_type")
    @Enumerated(EnumType.STRING)
    private CarriageType carriageType;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "train_id")
    private Train train;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "start_station_id")
    private Station startStation;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "finish_station_id")
    private Station endStation;
    @Column(name = "price")
    private Double price;
}
