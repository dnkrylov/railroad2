package com.railroad.domain;

/**
 * Carriage type enum
 * In current version of application there are no opportunity to add new carriage types
 */
public enum CarriageType {
    BUSINESS, COMFORT, ECONOMY
}
