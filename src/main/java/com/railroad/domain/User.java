package com.railroad.domain;

import com.railroad.dto.user.UserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Data
@Entity
@Table(name = "js_user")
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", unique = true, nullable = false)
    private Long id;
    @Column(name = "user_first_name", nullable = false)
    private String firstName;
    @Column(name = "user_last_name", nullable = false)
    private String lastName;
    @Column(name = "user_date_of_birth", nullable = false)
    private LocalDate dateOfBirth;
    @Column(name = "user_login", nullable = false, unique = true)
    @Pattern(regexp = "^[A-Za-z0-9]{3,16}$")
    private String login;
    @Column(name = "user_password", nullable = false)
    private String password;
    @Column(name = "user_email", nullable = false, unique = true)
    @Pattern(regexp = "^[A-Za-z0-9._%+-]{1,50}[@][A-Za-z0-9._%+-]{1,30}[.][A-Za-z]{2,10}$")
    private String email;
    @Column(name = "user_phone", nullable = false, unique = true)
    @Pattern(regexp = "^[+][0-9]{7,16}$")
    private String phone;
    @Column(name = "user_role", nullable = false)
    @Enumerated(EnumType.STRING)
    private UserRole userRole;
    @Column(name = "user_enabled")
    private boolean enabled = false;

    public User(UserDto userDto) {
            this.firstName = userDto.getFirstName();
            this.lastName = userDto.getLastName();
            this.dateOfBirth = LocalDate.parse(userDto.getDateOfBirth(), DateTimeFormatter.ofPattern("dd.MM.yyyy"));
            this.login = userDto.getLogin();
            this.email = userDto.getEmail();
            this.phone = userDto.getPhone();
            this.password = new BCryptPasswordEncoder().encode(userDto.getPassword());
            this.setUserRole(userDto.getUserRole());
    }
}
