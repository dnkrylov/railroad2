package com.railroad.domain;

/**
 * Available user roles
 */
public enum UserRole {
    ROLE_ADMIN, ROLE_USER
}
