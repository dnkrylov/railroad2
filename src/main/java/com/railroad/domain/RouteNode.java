package com.railroad.domain;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * RouteNode entity. References to Train entity by "train" field and Station entity by
 * "station" field
 * Contains information about time, when given train arriving or departing to given station
 */
@Data
@Entity
@Table(name = "js_route")
public class RouteNode {
    @Id
    @Column(name = "route_node_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "train_id")
    private Train train;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "station_id")
    private Station station;
    @Column(name = "arrival_time")
    private LocalDateTime arrivalTime;
    @Column(name = "departure_time")
    private LocalDateTime departureTime;
}
