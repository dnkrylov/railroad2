package com.railroad.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;


/**
 * Train entity contains information about train. References to Carriage and Station entities
 */
@Data
@Entity
@Table(name = "js_train")
public class Train {
    @Id
    @Column(name = "train_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "train_name")
    private String name;
    @OneToMany(mappedBy = "train")
    private List<Carriage> carriages;
    @OneToMany(mappedBy = "train")
    private List<RouteNode> routeNodes;
}
