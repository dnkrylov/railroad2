package com.railroad.controllers;

import com.railroad.dto.user.RegistrationUserDto;
import com.railroad.exceptions.CaptchaVerificationException;
import com.railroad.exceptions.SuchUserAlreadyExistsException;
import com.railroad.response.BusinessResponse;
import com.railroad.service.registraion.RegistrationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import javax.validation.Valid;


/**
 * Controller to process registration and email conformation requests
 */
@Controller
@RequiredArgsConstructor
@Slf4j
public class RegistrationController {

    private final RegistrationService registrationService;

    /**
     * Registering new user.
     * @param registrationUserDto DTO with information about registering user and Google captcha response
     * @param request request to get contextPath of application for generating email conformation ling
     * @throws CaptchaVerificationException if captcha verification failed
     * @throws SuchUserAlreadyExistsException if login/email/phone is already busy
     * @return BusinessResponse with success message
     */
    @PostMapping(value = "/unregistered/tryToRegister")
    @ResponseBody
    public BusinessResponse tryToRegister(@Valid @RequestBody RegistrationUserDto registrationUserDto,
                                          WebRequest request) {
        log.info("REGISTERING: " + registrationUserDto.getUserDto());
        return registrationService.addUser(registrationUserDto,request);
    }

    /**
     * Verifies token and redirect to main page. Adds a result of verification to Model attribute
     * @param token verification token
     * @param model
     */
    @GetMapping(value = "/unregistered/confirmEmail")
    public String confirmEmail(@RequestParam("token") String token, Model model) {
        log.info("EMAIL CONFORMATION");
        model.addAttribute("message", registrationService.verifyEmail(token));
        return "redirect:/";
    }
}



