package com.railroad.controllers;

import com.railroad.dto.UserDetailsDto;
import com.railroad.response.BusinessResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpSession;

/**
 * Controller to process logout request or configure session fo successfully logged in user
 */
@RestController
@Slf4j
public class LoginController {

    /**
     * Clears SecurityContext and sets session status to complete
     *
     * @param sessionStatus - current session status
     * @return BusinessResponse with success message
     */
    @GetMapping(value = "/unregistered/logout")
    public BusinessResponse logout(SessionStatus sessionStatus) {
        log.info("LOGGING OUT");
        SecurityContextHolder.getContext().setAuthentication(null);
        sessionStatus.setComplete();
        return new BusinessResponse("SUCCESS");
    }

    /**
     * Configuration session for successfully logged in user.
     * Sets session attributes: userFirsName, UserLastName, UserId
     *
     * @param session - current session
     * @return BusinessResponse with success message
     */

    @GetMapping(value = "/registered/afterLogin")
    public BusinessResponse setSessionConfig(HttpSession session) {
        UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder
                .getContext().getAuthentication();
        UserDetailsDto loggedInUser = (UserDetailsDto) authentication.getPrincipal();
        session.setAttribute("userFirstName", loggedInUser.getUser().getFirstName());
        session.setAttribute("userLastName", loggedInUser.getUser().getLastName());
        session.setAttribute("userId", loggedInUser.getUser().getId());
        log.info("LOGGED IN USER: " + loggedInUser.getUser().getId());
        return new BusinessResponse("SUCCESS");
    }
}
