package com.railroad.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Binds view for start application route.
 * If user comes by conformation email link-sets model attribute with conformation result message
 */
@Controller
@Slf4j
public class InitializationController {

    /**
     * Binds view for start application route.
     * If user comes by conformation email link-sets model attribute with conformation result message
     */
    @GetMapping("/")
    public String showMainPage(Model model, @Nullable @RequestParam(name = "message") String message) {
        log.info("START WORKING, MESSAGE: " + message);
        if(message != null) {
            model.addAttribute("message", message);
        }
        return "container";
    }
}
