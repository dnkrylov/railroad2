package com.railroad.controllers;

import com.railroad.domain.CarriageType;
import com.railroad.dto.admin.AdminTimeTableItemDto;
import com.railroad.dto.admin.TrainCreationDto;
import com.railroad.dto.admin.UserForAdminRequestDto;
import com.railroad.exceptions.InvalidRequestParamException;
import com.railroad.exceptions.SuchStationAlreadyExistsException;
import com.railroad.response.BusinessResponse;
import com.railroad.service.AdminService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Arrays;
import java.util.List;

/**
 * Controller to handle requests from users with ADMIN role only
 * Entry point for all admin functions
 */
@RestController
@RequiredArgsConstructor
@Slf4j
public class AdminController {
    private static final String DATE_FORMAT_REGEXP =
            "^(0[1-9]|1[0-9]|2[0-9]|3[0-1])([.])(0[1-9]|1[0-2])([.])([0-9][0-9][0-9][0-9])$";

    private final AdminService adminService;

    /**
     * Returns available carriage types for train creation function
     * @return List of available carriage types
     */
    @GetMapping(value = "/unregistered/getCarriageTypes")
    public List<CarriageType> getCarriageTypes() {
        log.info("GETTING CARRIAGE TYPES");
        return Arrays.asList(CarriageType.class.getEnumConstants());
    }

    /**
     * Adds a new station to database and sends a message to message topic with new station name
     * @param stationName - name of creating station
     * @throws SuchStationAlreadyExistsException if station with same name already exist
     * @return BusinessResponse with success message
     */
    @PostMapping(value = "/admin/addStation")
    public BusinessResponse saveStation(@NotBlank @RequestParam String stationName) {
        log.info("CREATING STATION: " + stationName);
        return adminService.addStation(stationName);
    }

    /**
     * Adds a new train to database and sends messages with route station names, if creation of this train
     * changes this day timetable.
     * If cant send messages - train will be created, but response message will contains information about it
     * @param trainCreationDto -contains full information about new train (name, carriages, route, ticket price...)
     * @throws InvalidRequestParamException if param contains unacceptable values (duplicating carriage numbers,
     * departing from station before arriving and other logical mistakes)
     * @return BusinessResponse with result message
     */
    @PostMapping(value = "/admin/addTrain")
    public BusinessResponse saveTrain(@RequestBody @Valid TrainCreationDto trainCreationDto) {
        log.info("CREATING TRAIN: " + trainCreationDto);
        return adminService.addTrain(trainCreationDto);
    }

    /**
     * Searches list of timetable rows with all train numbers and ids,
     * departing or arriving to given station during given day
     * and their arriving and departing time.
     * @param date - date for timetable
     * @param stationName - station name for timetable
     */
    @GetMapping(value = "/admin/getTrains")
    public List<AdminTimeTableItemDto> getTrains(@RequestParam @Pattern(regexp = DATE_FORMAT_REGEXP) String date,
                                                 @RequestParam @NotBlank String stationName) {
        log.info("SEARCHING TIMETABLE: " + date + stationName);
        return adminService.getTimeTable(stationName, date);
    }

    /**
     * Searches for all passengers, registered to train with given id and returns information about them
     * @param trainId - id of train
     */
    @GetMapping(value = "/admin/getPassengers")
    public List<UserForAdminRequestDto> getPassengers(@RequestParam @Pattern(regexp = "^[0-9]+$") String trainId) {
        log.info("SEARCHING PASSENGERS , TRAIN ID:" + trainId);
        return adminService.getPassengers(Long.parseLong(trainId));
    }
}
