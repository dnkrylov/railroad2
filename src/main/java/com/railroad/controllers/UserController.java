package com.railroad.controllers;

import com.railroad.dto.user.*;
import com.railroad.exceptions.DuplicatingTicketException;
import com.railroad.exceptions.SuchUserAlreadyExistsException;
import com.railroad.exceptions.ThisPlaceIsAlreadyBusyException;
import com.railroad.exceptions.ToLateException;
import com.railroad.response.BusinessResponse;
import com.railroad.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * Controller to handle requests from users with ADMIN or USER roles
 * Entry point for all user functions
 */
@RestController
@RequiredArgsConstructor
@Slf4j
public class UserController {
    private static final String DATE_FORMAT_REGEXP =
            "^(0[1-9]|1[0-9]|2[0-9]|3[0-1])([.])(0[1-9]|1[0-2])([.])([0-9][0-9][0-9][0-9])$";
    private static final String DIRECT_TICKET_INFO = "ticketInfo";
    private static final String TRANSFER_TICKET_INFO = "transferTicketInfo";

    private final UserService userService;

    /**
     * Search for all stations and returns their names
     * @return List of Station names
     */
    @GetMapping(value = "/unregistered/getStations")
    public List<String> getStations() {
        log.info("GETTING STATIONS");
        return userService.getStationNamesList();
    }

    /**
     * Return detailed information about current logged in user
     */
    @GetMapping(value = "/registered/getUserInfo")
    public UserDto getUserInfo() {
        log.info("GETTING USER INFO");
        return userService.getUserDto();
    }

    /**
     * Updates user information
     *
     * @param userDto - DTO with new information about logged in user
     * @return BusinessResponse with success message
     * @throws SuchUserAlreadyExistsException if new login, phone or email is busy
     */
    @PostMapping(value = "/registered/updateUserInfo")
    public BusinessResponse updateUser(@Valid @RequestBody UserDto userDto) {
        log.info("UPDATING USER " + userDto.getUserId());
        return userService.updateUser(userDto);
    }

    /**
     * Search list of timetable rows with all train numbers, departing or arriving to given station during given day
     * and their arriving and departing time.
     *
     * @param date        - date for timetable
     * @param stationName - station name for timetable
     */
    @GetMapping(value = "/unregistered/showTimeTable")
    public List<TimeTableItemDto> showTimeTable(@RequestParam @Pattern(regexp = DATE_FORMAT_REGEXP) String date,
                                                @RequestParam String stationName) {
        log.info("GETTING TIMETABLE " + stationName + " " + date);
        return userService.getTimeTableItems(stationName, date);
    }

    /**
     * Searches for trains, followings directly from start station to end station without transfer.
     * Returns information about such trains (departing and arriving time, trainID and train number)
     * Sets start and finish station to ticketInfo, and stores it in current session
     * for next steps of ticket purchase process.
     *
     * @param session          - current http session
     * @param startStationName - start route station
     * @param endStationName   - final route station
     * @param routeDate        - date, when passenger wanted to start/finish travel
     * @param isItArrivingDate - if false - method will return trains, that started at given date,
     *                         else - that arriving to finish station at given date
     */
    @GetMapping(value = "/unregistered/showDirectRoutes")
    public List<AvailableDirectRouteDto> findDirectTrains(HttpSession session,
                                                          @RequestParam @NotBlank String startStationName,
                                                          @RequestParam @NotBlank String endStationName,
                                                          @RequestParam @NotBlank @Pattern(regexp = DATE_FORMAT_REGEXP)
                                                                  String routeDate,
                                                          @RequestParam @NotNull boolean isItArrivingDate) {
        log.info("SEARCHING DIRECT TRAINS: " + startStationName + "-" + endStationName +
                " " + routeDate + " " + isItArrivingDate);
        TicketInfo ticketInfo = new TicketInfo();
        ticketInfo.setStartStationName(startStationName);
        ticketInfo.setEndStationName(endStationName);
        session.setAttribute(DIRECT_TICKET_INFO, ticketInfo);
        return userService.findDirectRoutes(startStationName, endStationName, routeDate, isItArrivingDate);
    }

    /**
     * Searches for trains followings from start station to end station with one transfer.
     * Returns information about such trains, departing/arriving times and transfer station
     * Sets start and finish station to ticketInfo, and stores it in current session
     * for next steps of ticket purchase process
     *
     * @param session          - current http session
     * @param startStationName - start route station
     * @param endStationName   - final route station
     * @param routeDate        - date, when passenger wanted to start/finish travel
     * @param isItArrivingDate - if false - method will return trains, that started at given date,
     *                         else - that arriving to finish station at given date
     */
    @GetMapping(value = "/unregistered/showTransferRoutes")
    public List<AvailableTransferRouteDto> findTransferTrains(HttpSession session,
                                                              @RequestParam @NotBlank String startStationName,
                                                              @RequestParam @NotBlank String endStationName,
                                                              @RequestParam @NotBlank @Pattern(regexp = DATE_FORMAT_REGEXP)
                                                                      String routeDate,
                                                              @RequestParam @NotNull boolean isItArrivingDate) {
        log.info("SEARCHING TRANSFER TRAINS: " + startStationName + "-" + endStationName +
                " " + routeDate + " " + isItArrivingDate);
        TransferTicketInfo transferTicketInfo = new TransferTicketInfo();
        transferTicketInfo.setStartStationName(startStationName);
        transferTicketInfo.setEndStationName(endStationName);
        session.setAttribute(TRANSFER_TICKET_INFO, transferTicketInfo);
        return userService.findTransferRoutes(startStationName, endStationName, routeDate, isItArrivingDate);
    }

    /**
     * Search for carriages of a train, selected to travel.
     * Read start and finish station from request parameters.
     * Return id, type, number, free places and travel price for each carriage
     * Removes temporary tickets (which was reserved more than to minutes ago and not bought)
     * If user already has booked tickets, they will be deleted too.
     * @param session Current session
     * @param trainId selected train ID
     */
    @PostMapping(value = "/registered/showDirectCarriages")
    public List<CarriageDto> getDirectCarriages(HttpSession session,
                                                long trainId) {
        log.info("GETTING DIRECT CARRIAGES, TRAIN ID:" + trainId);
        TicketInfo ticketInfo = (TicketInfo) session.getAttribute(DIRECT_TICKET_INFO);
        ticketInfo.setTrainId(trainId);
        return userService.getCarriages(ticketInfo);
    }

    /**
     * Search carriages of both trains (before and after transfer) for selected travel route.
     * Return DTO with two carriages lists (first and second train), each list contains information about
     * id, type, number, free places and travel price for each carriage
     * Remove temporary tickets (which was reserved more than to minutes ago and not bought)
     * If user already has booked tickets, they will be deleted too.
     * @param session current session
     * @param chosenTransferRouteDto information about selected route (trains ids, start, finish and transfer station)
     */
    @PostMapping(value = "/registered/showTransferCarriages")
    public TransferCarriages getTransferCarriages(HttpSession session,
                                                  @RequestBody AvailableTransferRouteDto chosenTransferRouteDto) {
        log.info("GETTING TRANSFER CARRIAGES, FOR: " + chosenTransferRouteDto);
        TransferTicketInfo transferTicketInfo = (TransferTicketInfo) session.getAttribute(TRANSFER_TICKET_INFO);
        transferTicketInfo.setMiddleStationName(chosenTransferRouteDto.getMiddleStationName());
        transferTicketInfo.setTrainId1(chosenTransferRouteDto.getTrainId1());
        transferTicketInfo.setTrainId2(chosenTransferRouteDto.getTrainId2());
        return userService.getTransferCarriages(transferTicketInfo);
    }

    /**
     * Reserve selected place for 10 minutes.
     * @param session     Current session
     * @param carriageId  selected carriage id
     * @param placeNumber selected place number
     * @return BusinessResponse with success message
     * @throws ThisPlaceIsAlreadyBusyException if selected place was booked by another user during place choosing
     *                                         or if two users booking one place at same time
     */
    @PostMapping(value = "/registered/showPaymentModal")
    public BusinessResponse preparePaymentPage(HttpSession session,
                                               @RequestParam long carriageId,
                                               @RequestParam int placeNumber) {
        TicketInfo ticketInfo = (TicketInfo) session.getAttribute(DIRECT_TICKET_INFO);
        ticketInfo.setCarriageId(carriageId);
        ticketInfo.setPlaceNumber(placeNumber);
        log.info("PREPARING PAYMENT PAGE: " + ticketInfo + " car:" + carriageId + " place:" + placeNumber);
        return userService.createTemporaryTicket(ticketInfo);
    }

    /**
     * Books selected places in each train for 10 minutes.
     * @param session      Current session
     * @param carriage1Id  selected carriage id in train 1
     * @param place1Number selected place number selected place number in carriage 1
     * @param carriage1Id  selected carriage id in train 2
     * @param place1Number selected place number in carriage 2
     * @return BusinessResponse with success message
     * @throws ThisPlaceIsAlreadyBusyException if one of selected places was booked by another
     * user during place choosing or if two users booking one place at same time
     */
    @PostMapping(value = "/registered/showTransferPaymentModal")
    public BusinessResponse prepareTransferPaymentPage(HttpSession session,
                                                       @RequestParam long carriage1Id, @RequestParam long carriage2Id,
                                                       @RequestParam int place1Number, @RequestParam int place2Number) {
        TransferTicketInfo transferTicketInfo = (TransferTicketInfo) session.getAttribute(TRANSFER_TICKET_INFO);
        transferTicketInfo.setCarriageId1(carriage1Id);
        transferTicketInfo.setPlaceNumber1(place1Number);
        transferTicketInfo.setCarriageId2(carriage2Id);
        transferTicketInfo.setPlaceNumber2(place2Number);
        log.info("PREPARING PAYMENT PAGE: " + transferTicketInfo + " car1:" + carriage1Id + " place1:" + place1Number +
                " car2:" + carriage1Id + " place2:" + place1Number);
        return userService.createTransferTemporaryTickets(transferTicketInfo);
    }

    /**
     * If get valid payment information - marked booked ticket as bought by user.
     * @param session current session
     * @param cardNumber credit card number
     * @param cardExp credit card expiration date
     * @param cardCVV credit card CVV
     * @throws ToLateException if train is already departed or departing in 10 minutes
     * or if ticket was booked more than 10 minutes ago, marked as free and bought by another user
     * @throws DuplicatingTicketException if user already has a ticket to selected train
     * @return BusinessResponse with success message
     */
    @PostMapping(value = "/registered/tryToBuyDirectTicket")
    public BusinessResponse tryToBuyDirectTicket(HttpSession session,
                                                 @RequestParam(name = "cardNumber") String cardNumber,
                                                 @RequestParam(name = "cardExp") String cardExp,
                                                 @RequestParam(name = "cardCVV") String cardCVV) {
        TicketInfo ticketInfo = (TicketInfo) session.getAttribute(DIRECT_TICKET_INFO);
        log.info("TRY TO BUY DIRECT TICKET " + ticketInfo);
        return userService.upgradeTemporaryDirectTicket(ticketInfo);
    }

    /**
     * If get valid payment information - marked booked tickets as bought by user.
     * @param session current session
     * @param cardNumber credit card number
     * @param cardExp credit card expiration date
     * @param cardCVV credit card CVV
     * @throws ToLateException if one of selected trains is already departed or departing in 10 minutes
     * or if one of selected tickets was booked more than 10 minutes ago, marked as free and bought by another user
     * @throws DuplicatingTicketException if user already has a ticket to one of selected train
     */
    @PostMapping(value = "/registered/tryToBuyTransferTicket")
    public BusinessResponse tryToBuyTransferTicket(HttpSession session, @RequestParam String cardNumber,
                                                   @RequestParam String cardExp,
                                                   @RequestParam String cardCVV) {
        TransferTicketInfo transferTicketInfo = (TransferTicketInfo) session.getAttribute(TRANSFER_TICKET_INFO);
        log.info("TRY TO BUY TRANSFER TICKET " + transferTicketInfo);
        return userService.upgradeTemporaryTransferTicket(transferTicketInfo);
    }

    /**
     * Searches for all ticket of logged in user and returns brief information about them
     */
    @GetMapping(value = "/registered/getTicketsPreview")
    public List<TicketPreviewDto> getTicketPreview() {
        log.info("GET TICKET PREVIEW");
        return userService.getTicketPreview();
    }

    /**
     * Validates, that logged in user is owner of requested ticket.
     * If true - returns detailed information about ticket, if false-returns null
     */
    @GetMapping(value = "/registered/getTicketDetails")
    public TicketDetailsDto getTicketDetails(@RequestParam long ticketId) {
        log.info("GET TICKET DETAILS: " + ticketId);
        return userService.getTicketDetails(ticketId);
    }

    /**
     * Validates, that logged in user is owner of requested ticket.
     * If true - sends ticket to user email, if false-returns null
     * @param ticketId - requested ticket id
     */
    @PostMapping(value = "/registered/sendTicketByEmail")
    public BusinessResponse sendTicketToEmail(@RequestParam long ticketId) {
        log.info("SENT TICKET TO EMAIL: " + ticketId);
        return userService.sendTicketByEmail(ticketId);
    }
}
