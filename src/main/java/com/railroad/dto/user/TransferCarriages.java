package com.railroad.dto.user;

import lombok.Data;

import java.util.List;

/**
 * DTO to transfer information from backend
 * about carriages in first and second train for route with one transfer station
 */
@Data
public class TransferCarriages {
    List<CarriageDto> train1Carriages;
    List<CarriageDto> train2Carriages;
}
