package com.railroad.dto.user;

import com.railroad.domain.User;
import com.railroad.domain.UserRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.time.format.DateTimeFormatter;

/**
 * DTO to transfer information from frontend about new registering user
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private Long userId;
    @NotBlank(message = "name is required")
    private String firstName;
    @NotBlank(message = "surname is required")
    private String lastName;
    @NotBlank(message = "date of birth is required")
    @Pattern(regexp="^(0[1-9]|1[0-9]|2[0-9]|3[0-1])([.])(0[1-9]|1[0-2])([.])([0-9][0-9][0-9][0-9])$",
            message="date of birth should be formatted dd.mm.yyyy")
    private String dateOfBirth;
    @NotBlank(message = "login is required")
    @Pattern(regexp = "^[A-Za-z0-9]{3,16}$",
            message="login should contain 3-16 letters and (or) digits")
    private String login;
    @NotBlank(message = "email is required")
    @Pattern(regexp = "^[A-Za-z0-9._%+-]{1,50}[@][A-Za-z0-9._%+-]{1,30}[.][A-Za-z]{2,10}$")
    private String email;
    @NotBlank(message = "phone is required")
    @Pattern(regexp = "^[+][0-9]{7,16}$",
            message="invalid phone format")
    private String phone;
    @NotBlank(message = "password is required")
    @Pattern(regexp = "^[A-Za-z0-9]{3,16}$")
    private String password;
    private UserRole userRole;

    public UserDto(User user) {
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.dateOfBirth = user.getDateOfBirth().format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        this.phone = user.getPhone();
        this.email = user.getEmail();
        this.login = user.getLogin();
    }
}
