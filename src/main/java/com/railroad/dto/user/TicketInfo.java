package com.railroad.dto.user;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Session attribute DTO, containing information about direct route and user for direct ticket purchase process
 */
@Data
@NoArgsConstructor
public class TicketInfo implements Serializable {
    private int placeNumber;
    private long carriageId;
    private long userId;
    private String startStationName;
    private String endStationName;
    private long startStationId;
    private long endStationId;
    private long trainId;
    private long ticketId;
    private double ticketPrice;
    private String carriageType;

    public TicketInfo(int placeNumber, long carriageId, long userId) {
        this.placeNumber = placeNumber;
        this.carriageId = carriageId;
        this.userId = userId;
    }

    public TicketInfo(long userId, long startStationId, long endStationId, long ticketId, double ticketPrice,
                      long trainId) {
        this.userId = userId;
        this.startStationId = startStationId;
        this.endStationId = endStationId;
        this.ticketId = ticketId;
        this.ticketPrice = ticketPrice;
        this.trainId = trainId;
    }
}
