package com.railroad.dto.user;

import lombok.Data;

import java.io.Serializable;
/**
 * Session attribute DTO, containing information about transfer route and user for fransfer ticket purchase process
 */
@Data
public class TransferTicketInfo implements Serializable {
    private String startStationName;
    private String middleStationName;
    private String endStationName;
    private long startStationId;
    private long middleStationId;
    private long endStationId;

    private long userId;

    private int placeNumber1;
    private long carriageId1;
    private String carriageType1;
    private long trainId1;
    private long ticketId1;
    private double ticketPrice1;

    private int placeNumber2;
    private long carriageId2;
    private String carriageType2;
    private long trainId2;
    private long ticketId2;
    private double ticketPrice2;

}
