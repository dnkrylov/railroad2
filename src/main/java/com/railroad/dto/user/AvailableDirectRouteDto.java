package com.railroad.dto.user;

import lombok.Data;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * DTO to transfer information from backend about available direct routes
 */
@Data
public class AvailableDirectRouteDto {
    String startTime;
    String trainNumber;
    Long trainId;
    String endTime;

    public AvailableDirectRouteDto(LocalDateTime startTime, String trainNumber, Long trainId, LocalDateTime endTime) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
        this.startTime = startTime.format(dtf);
        this.trainNumber = trainNumber;
        this.trainId = trainId;
        this.endTime = endTime.format(dtf);
    }
}
