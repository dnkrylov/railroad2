package com.railroad.dto.user;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
/**
 * DTO to transfer brief information about ticket
 */
@Data
@NoArgsConstructor
public class TicketPreviewDto {
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private long ticketId;
    private String startStationName;
    private String endStationName;
    private String departureDate;

    public TicketPreviewDto(long ticketId, String startStationName, String endStationName, LocalDateTime departureTime) {
        this.ticketId = ticketId;
        this.startStationName = startStationName;
        this.endStationName = endStationName;
        this.departureDate = departureTime.format(DATE_FORMATTER);
    }
}
