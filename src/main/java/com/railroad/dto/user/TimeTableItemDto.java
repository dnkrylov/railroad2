package com.railroad.dto.user;

import lombok.Data;

/**
 * DTO to transfer information from backend  about arriving and departing time of train for timetable
 */
@Data
public class TimeTableItemDto {
    private String arrivalTime;
    private String trainNumber;
    private String departureTime;
}
