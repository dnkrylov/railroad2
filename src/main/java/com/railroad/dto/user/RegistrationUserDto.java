package com.railroad.dto.user;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

/**
 * DTO to transfer information from from frontend. Contains information about new user
 * and captcha response from google service
 */
@Data
@NoArgsConstructor
public class RegistrationUserDto {
    @NotBlank
    private String captchaResponse;
    @Valid
    private UserDto userDto;
}
