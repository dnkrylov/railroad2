package com.railroad.dto.user;

import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * DTO to transfer detailed information from backend about selected ticket
 */
@Data
@NoArgsConstructor
public class TicketDetailsDto {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
    private Long ticketId;
    private String userFirstName;
    private String userLastName;
    private String startStation;
    private String endStation;
    private String trainNumber;
    private String carriageNumber;
    private Integer placeNumber;
    private String departingTime;
    private String arrivingTime;
    private Double price;

    public TicketDetailsDto(Long ticketId, String userFirstName, String userLastName, String startStation,
                            String endStation, String trainNumber, String carriageNumber, Integer placeNumber,
                            LocalDateTime departingTime, LocalDateTime arrivingTime, Double price) {
        this.ticketId = ticketId;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.startStation = startStation;
        this.endStation = endStation;
        this.trainNumber = trainNumber;
        this.carriageNumber = carriageNumber;
        this.placeNumber = placeNumber;
        this.departingTime = departingTime.format(DATE_TIME_FORMATTER);
        this.arrivingTime = arrivingTime.format(DATE_TIME_FORMATTER);
        this.price = price;
    }
}
