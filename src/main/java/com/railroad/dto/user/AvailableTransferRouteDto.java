package com.railroad.dto.user;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * DTO to transfer information about available transfer routes
 */
@Data
@NoArgsConstructor
public class AvailableTransferRouteDto {
    @NotBlank
    String startTime1;
    @NotBlank
    String trainNumber1;
    @NotBlank
    Long trainId1;
    String endTime1;
    String startTime2;
    @NotBlank
    String trainNumber2;
    @NotBlank
    Long trainId2;
    String endTime2;
    @NotBlank
    String middleStationName;

    public AvailableTransferRouteDto(LocalDateTime startTime1, String trainNumber1, Long trainId1, LocalDateTime endTime1,
                                     LocalDateTime startTime2, String trainNumber2, Long trainId2, LocalDateTime endTime2,
    String middleStationName) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
        this.startTime1 = startTime1.format(dtf);
        this.trainNumber1 = trainNumber1;
        this.trainId1 = trainId1;
        this.endTime1 = endTime1.format(dtf);
        this.startTime2 = startTime2.format(dtf);
        this.trainNumber2 = trainNumber2;
        this.trainId2 = trainId2;
        this.endTime2 = endTime2.format(dtf);
        this.middleStationName = middleStationName;
    }
}
