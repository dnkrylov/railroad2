package com.railroad.dto.user;

import com.railroad.domain.CarriageType;
import lombok.Data;

import java.util.List;
/**
 * DTO to transfer information from backend about carriage and travel price
 */
@Data
public class CarriageDto {
    private Long carriageId;
    private String number;
    private CarriageType carriageType;
    private int capacity;
    private List<Integer> freePlaceNumbers;
    private double ticketPrice;
}
