package com.railroad.dto.admin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO to transfer information from backend about train for admin passengers show function
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminTimeTableItemDto {
    private String trainNumber;
    private Long trainId;
}
