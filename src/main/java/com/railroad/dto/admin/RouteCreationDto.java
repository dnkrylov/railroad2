package com.railroad.dto.admin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * DTO to transfer information from frontend about neu route nodes and station ticket price for new train
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RouteCreationDto {
    private static final String TIME_FORMAT_REGEXP = "^(0[1-9]|1[0-9]|2[0-9]|3[0-1])([.])(0[1-9]|1[0-2])" +
            "([.])([0-9][0-9][0-9][0-9])([:])(0[0-9]|1[0-9]|2[0-3])([.])([0-5][0-9])$";
    private static final String PRICE_FORMAT_REGEXP = "^[0-9]{0,8}+([.][0-9]{2})?$";
    @NotBlank
    private String stationName;
    @Pattern(regexp = TIME_FORMAT_REGEXP)
    private String arrivingTime;
    @Pattern(regexp = TIME_FORMAT_REGEXP)
    private String departureTime;
    @Pattern(regexp = PRICE_FORMAT_REGEXP)
    private String economyPrice;
    @Pattern(regexp = PRICE_FORMAT_REGEXP)
    private String comfortPrice;
    @Pattern(regexp = PRICE_FORMAT_REGEXP)
    private String businessPrice;
}
