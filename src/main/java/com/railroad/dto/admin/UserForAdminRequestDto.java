package com.railroad.dto.admin;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * DTO to transfer information from backend about users, registered to selected trains
 */
@Data
@AllArgsConstructor
public class UserForAdminRequestDto implements Comparable {
    private String login;
    private String firstName;
    private String lastName;
    private String carriageNumber;
    private Integer placeNumber;
    private String startStationName;
    private String endStationName;
    private Double price;

    @Override
    public int compareTo(Object o) {

        return this.lastName.compareToIgnoreCase(((UserForAdminRequestDto)o).getLastName());
    }

    public UserForAdminRequestDto(String login, String firstName, String lastName,
                                  Integer placeNumber, String startStationName, String endStationName,
                                  Double price, String carriageNumber) {
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.placeNumber = placeNumber;
        this.startStationName = startStationName;
        this.endStationName = endStationName;
        this.price = price;
        this.carriageNumber = carriageNumber;
    }
}
