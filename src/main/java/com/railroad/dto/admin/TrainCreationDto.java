package com.railroad.dto.admin;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class TrainCreationDto {
    @NotBlank
    private String number;
    @Valid @Size(min = 2)
    List<RouteCreationDto> stationList;
    @Valid @Size(min = 1)
    List<CarriageForTrainCreationDto> carriages;
}
