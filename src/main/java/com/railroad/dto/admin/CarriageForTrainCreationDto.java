package com.railroad.dto.admin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * DTO for transfer information from frontend about carriage in new train
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class CarriageForTrainCreationDto {
    private static final String CAPACITY_FORMAT_REGEXP = "^([1-9][0-9]{0,2})$";
    @NotBlank
    private String carName;
    @NotBlank
    private String carType;
    @Pattern(regexp = CAPACITY_FORMAT_REGEXP)
    private String carCapacity;
}
