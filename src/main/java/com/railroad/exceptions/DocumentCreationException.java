package com.railroad.exceptions;

/**
 * Exception is thrown if application can not create pdf ticket
 */
public class DocumentCreationException extends RuntimeException {
    public DocumentCreationException(String message) {
        super(message);
    }
}
