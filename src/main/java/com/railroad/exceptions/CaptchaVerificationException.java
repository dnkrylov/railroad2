package com.railroad.exceptions;

/**
 * Exception is thrown if captcha verification fails
 */
public class CaptchaVerificationException extends RuntimeException {
    public CaptchaVerificationException(String message) {
        super(message);
    }
}
