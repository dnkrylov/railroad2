package com.railroad.exceptions;

/**
 * Exception is thrown if during registration or changing user information with values of LOGIN, PHONE or EMAIL
 * which is already busy by another user
 */
public class SuchUserAlreadyExistsException extends RuntimeException {
    public SuchUserAlreadyExistsException(String message) {
        super(message);
    }
}
