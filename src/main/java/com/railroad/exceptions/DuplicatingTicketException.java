package com.railroad.exceptions;

public class DuplicatingTicketException extends RuntimeException {
    public DuplicatingTicketException(String message) {
        super(message);
    }
}
