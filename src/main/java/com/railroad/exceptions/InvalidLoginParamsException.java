package com.railroad.exceptions;

/**
 * Exception is thrown if user entered nonexistent login or account is not confirmed
 */
public class InvalidLoginParamsException extends RuntimeException {
    public InvalidLoginParamsException(String message) {
        super(message);
    }
}
