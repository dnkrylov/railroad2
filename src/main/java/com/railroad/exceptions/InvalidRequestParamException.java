package com.railroad.exceptions;

/**
 * Exception is thrown if request contain wrong (logically) values
 */
public class InvalidRequestParamException extends RuntimeException {
    public InvalidRequestParamException(String message) {
        super(message);
    }
}
