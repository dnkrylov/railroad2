package com.railroad.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
@Slf4j
public class ErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {SuchStationAlreadyExistsException.class, SuchUserAlreadyExistsException.class,
            ThisPlaceIsAlreadyBusyException.class, DuplicatingTicketException.class,
            CaptchaVerificationException.class})
    protected ResponseEntity<Object> conflictHandle(RuntimeException e) {
        log.info(e.getMessage());
        return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = {InvalidRequestParamException.class, ToLateException.class})
    protected ResponseEntity<Object> invalidParamsHandle(RuntimeException e) {
        log.info(e.getMessage());
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {InvalidLoginParamsException.class})
    protected ResponseEntity<Object> invalidPasswordHandle(RuntimeException e) {
        log.info(e.getMessage());
        return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {ConnectionException.class})
    protected ResponseEntity<Object> connectionExceptionsHandler(ConnectionException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {Throwable.class})
    protected ResponseEntity<Object> unexpectedExceptionHandler(Throwable t) {
        log.error("UNEXPECTED EXCEPTION " + t.getMessage(), t);
        return new ResponseEntity<>("something is wrong. Try again later", HttpStatus.BAD_REQUEST);
    }


}
