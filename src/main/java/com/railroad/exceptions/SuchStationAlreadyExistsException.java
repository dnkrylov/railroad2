package com.railroad.exceptions;

/**
 * Exception is thrown if administrator is trying to create new station with name of already existing station
 */
public class SuchStationAlreadyExistsException extends RuntimeException {
    public SuchStationAlreadyExistsException(String message) {
        super(message);
    }
}
