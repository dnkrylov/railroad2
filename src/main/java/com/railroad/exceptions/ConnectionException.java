package com.railroad.exceptions;

/**
 * Exception is thrown application can not connect to external resources
 */
public class ConnectionException extends RuntimeException {
    public ConnectionException(String message) {
        super(message);
    }
}
