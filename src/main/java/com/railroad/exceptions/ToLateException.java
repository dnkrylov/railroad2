package com.railroad.exceptions;

/**
 * Exception is thrown if user is trying to buy ticket for train, which is already departed or departing in 10 minutes.
 * Also it can be thrown if user is trying  to buy booked ticket, which booking time is over and which if bought
 * by another user
 */
public class ToLateException extends RuntimeException {
    public ToLateException(String message) {
        super(message);
    }
}
