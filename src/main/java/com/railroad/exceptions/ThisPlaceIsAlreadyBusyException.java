package com.railroad.exceptions;

/**
 * Exception is thrown if user is trying to buy or book ticket, which is already bought or booked by another user
 */
public class ThisPlaceIsAlreadyBusyException extends RuntimeException {
    public ThisPlaceIsAlreadyBusyException(String message) {
        super(message);
    }
}
