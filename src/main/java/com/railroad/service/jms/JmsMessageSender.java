package com.railroad.service.jms;

public interface JmsMessageSender {
    boolean sendMessage(String topic, String message);
}
