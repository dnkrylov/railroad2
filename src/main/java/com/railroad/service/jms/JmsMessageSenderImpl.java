package com.railroad.service.jms;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.springframework.stereotype.Service;
import javax.jms.*;

/**
 * Sending messages to JMS topic service
 */

@Service
@RequiredArgsConstructor
@Slf4j
public class JmsMessageSenderImpl implements JmsMessageSender {
    private final PooledConnectionFactory pooledConnectionFactory;


    /**
     * Sends given message to given topic. Return true is message successfully sent or false
     * in cause of some error
     * @param topic topic, where we want to send a message
     * @param message message to sent
     */
    @Override
    public boolean sendMessage(String topic, String message) {
        log.info("SENDING MESSAGE: " + message + " TO TOPIC: " + topic);
        try{
            TopicConnection producerConnection = pooledConnectionFactory.createTopicConnection();
            producerConnection.start();
            TopicSession producerSession = producerConnection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
            Topic producerDestination = producerSession.createTopic(topic);
            TopicPublisher producer = producerSession.createPublisher(producerDestination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            TextMessage producerMessage = producerSession.createTextMessage(message);
            producer.send(producerMessage);
            producer.close();
            producerSession.close();
            producerConnection.close();
        } catch (JMSException e) {
            log.error("ERROR DURING SENDING MESSAGE: " + e.getMessage(), e);
            return false;
        }
        log.info("MESSAGE SENT");
        return true;
    }
}
