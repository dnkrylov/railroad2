package com.railroad.service;

import com.railroad.domain.User;
import com.railroad.dto.UserDetailsDto;
import com.railroad.exceptions.InvalidLoginParamsException;
import com.railroad.repo.UserRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Custom Implementation of UserDetailsService
 */
@Service("userDetailsService")
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepo userRepo;

    /**
     * Load user with given login from database, create UserDetailsDto from loaded user
     * @throws InvalidLoginParamsException if user with given login not found or his account is not confirmed
     */
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String login) {
        log.info("SEARCHING USER BY LOGIN: " + login);
        User user = userRepo.findUserByLogin(login);
        if (user == null) {
            log.info("USER NOT FOUND");
            throw new InvalidLoginParamsException("User with login: " + login + " not found");
        } else if(!user.isEnabled()) {
            log.info("USER NOT ENABLED");
            throw new InvalidLoginParamsException("ACCOUNT '" + login + "' IS NOT CONFIRMED, " +
                    "PLEASE ACTIVATE IT BY CLICKING ON CONFORMATION LINK IN YOUR EMAIL MESSAGE");
        }
        log.info("USER FOUND, ID: " + user.getId());
        return new UserDetailsDto(user);
    }
}
