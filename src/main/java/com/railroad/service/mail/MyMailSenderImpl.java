package com.railroad.service.mail;

import com.railroad.dto.user.TicketDetailsDto;
import com.railroad.exceptions.ConnectionException;
import com.railroad.response.BusinessResponse;
import com.railroad.service.pdf.PdfGenerator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;


/**
 * Email sending service
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class MyMailSenderImpl implements MyMailSender {

    private final JavaMailSender mailSender;
    private final PdfGenerator pdfGenerator;

    /**
     * Sends ticket information for user email
     * @param ticketDetailsDto ticket information
     * @param userEmail user email
     * @return BusinessResponse with success message
     * @throws ConnectionException if cant connect to mail server
     */
    @Override
    public BusinessResponse sendTicketByEmail(TicketDetailsDto ticketDetailsDto, String userEmail) {
        log.info("SENDING TICKET: " + ticketDetailsDto.getTicketId() + " TO: " + userEmail);
        String subject = "YOUR RAILROAD TICKET";
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setTo(userEmail);
            helper.setSubject(subject);
            helper.setText(ticketDetailsDto.toString());
            helper.addAttachment("ticket.pdf",
                    new ByteArrayResource(pdfGenerator.createPdfTicket(ticketDetailsDto).toByteArray()));
            mailSender.send(mimeMessage);
            log.info("EMAIL SENT");
        }catch (MessagingException | MailSendException e) {
            log.error("EMAIL NOT SENT: " + e.getMessage(), e);
            throw new ConnectionException("CANT CONNECT TO MAIL SERVER");
        }
        return new BusinessResponse("TICKET WAS SENT TO YOUR EMAIL");
    }

    /**
     * Sends conformation url for user email
     * @param conformationUrl conformation url
     * @param userEmail user email
     * @throws ConnectionException if cant connect to mail server
     */
    @Override
    public void sendConformationLink(String conformationUrl, String userEmail) {
        log.info("SENDING CONFORMATION LINK: " + conformationUrl + " TO: " + userEmail);
        try {
            String subject = "RAILROAD REGISTRATION CONFORMATION";
            SimpleMailMessage email = new SimpleMailMessage();
            email.setTo(userEmail);
            email.setSubject(subject);
            email.setText("Conformation link: " + conformationUrl);
            mailSender.send(email);
            log.info("EMAIL SENT");
        }catch (MailSendException e) {
            log.error("EMAIL NOT SENT " + e.getMessage(), e);
            throw new ConnectionException("CANT CONNECT TO MAIL SERVER");
        }
    }
}
