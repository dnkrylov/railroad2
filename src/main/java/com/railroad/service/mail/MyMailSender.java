package com.railroad.service.mail;

import com.railroad.dto.user.TicketDetailsDto;
import com.railroad.response.BusinessResponse;

public interface MyMailSender {
    BusinessResponse sendTicketByEmail(TicketDetailsDto ticketDetailsDto, String userEmail);
    void sendConformationLink(String conformationUrl, String userEmail);
}
