package com.railroad.service;

import com.railroad.domain.*;
import com.railroad.dto.admin.*;
import com.railroad.exceptions.InvalidRequestParamException;
import com.railroad.exceptions.SuchStationAlreadyExistsException;
import com.railroad.repo.*;
import com.railroad.response.BusinessResponse;
import com.railroad.service.jms.JmsMessageSender;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service for implementing admin functions
 */

@Service
@RequiredArgsConstructor
@Slf4j
public class AdminServiceImpl implements AdminService {
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy:HH.mm");

    private final StationRepo stationRepo;
    private final TrainRepo trainRepo;
    private final JmsMessageSender jmsMessageSender;
    private final Validator validator;
    private final RouteNodeRepo routeNodeRepo;
    private final PriceRepo priceRepo;
    private final TicketRepo ticketRepo;

    /**
     * Add a new station with given name to database and sends a message to message topic with new station name
     * If station will be created, but failed to sen message - reports about in in response message
     * @param stationName - name of creating station
     * @throws InvalidRequestParamException if station name contains only space symbols
     * @throws SuchStationAlreadyExistsException if station with same name already exist
     * @return BusinessResponse with result message
     */
    @Transactional
    @Override
    public BusinessResponse addStation(String stationName) {
        String trimedStationName = stationName.trim().toUpperCase();
        log.info("ADDING STATION: " + stationName);
        if (trimedStationName.equals("")) {
            log.info("EMPTY STATION NAME, RETURN");
            throw new InvalidRequestParamException("INVALID STATION NAME");
        }
        Station station = new Station();
        station.setName(trimedStationName);
        try {
            stationRepo.addStation(station);
        } catch (ConstraintViolationException e) {
            log.info("DUPLICATE STATION NAME, RETURN");
            throw new SuchStationAlreadyExistsException("STATION WITH SAME NAME IS ALREADY EXISTS");
        }
        return jmsMessageSender.sendMessage("TIMETABLE_CHANGED", trimedStationName) ?
                new BusinessResponse("STATION " + trimedStationName + " SUCCESSFULLY ADDED") :
                new BusinessResponse("STATION " + trimedStationName +
                        " ADDED, BUT MESSAGES TO ELECTRONIC TIMETABLES WAS NOT SENT ");
    }

    /**
     * Adds a new train to database and sends messages with route station names, if creation of this train
     * changes this day timetable.
     * If cant send messages - train will be created, but response message will contains information about it
     * @param trainCreationDto -contains full information about new train (name, carriages, route, ticket price...)
     * @throws InvalidRequestParamException if param contains unacceptable values (duplicating carriage numbers,
     * departing from station before arriving and other logical mistakes)
     * @return BusinessResponse with result message
     */
    @Transactional
    @Override
    public BusinessResponse addTrain(TrainCreationDto trainCreationDto) {
        log.info("CREATING TRAIN: " + trainCreationDto);
        validator.validateTrain(trainCreationDto);
        Train train = new Train();
        train.setName(trainCreationDto.getNumber());
        train.setId(trainRepo.addTrain(train));
        Station previousStation = null;
        for (int i = 0; i < trainCreationDto.getStationList().size(); i++) {
            RouteCreationDto stationDto = trainCreationDto.getStationList().get(i);
            RouteNode routeNode = fillRouteNodeInfo(stationDto, train);
            routeNodeRepo.addRouteNode(routeNode);
            if (i == 0) {
                previousStation = routeNode.getStation();
                continue;
            }
            Station currentStation = routeNode.getStation();
            priceRepo.addPricePart(fillPricePart(previousStation, currentStation, stationDto, train,
                    CarriageType.BUSINESS));
            priceRepo.addPricePart(fillPricePart(previousStation, currentStation, stationDto, train,
                    CarriageType.COMFORT));
            priceRepo.addPricePart(fillPricePart(previousStation, currentStation, stationDto, train,
                    CarriageType.ECONOMY));
            previousStation = currentStation;
        }
        trainCreationDto.getCarriages().forEach(dto -> trainRepo.addCarriage(fillCarriageInfo(dto, train)));
        boolean messagesSent = trainCreationDto.getStationList().stream().filter(this::changesTodayTimetable)
                .allMatch(station -> jmsMessageSender.sendMessage("TIMETABLE_CHANGED", station.getStationName()));
        return messagesSent ? new BusinessResponse("Train successfully created. Train id: " + train.getId()) :
                new BusinessResponse("Train created. Train id: " + train.getId() +
                        ", BUT MESSAGES TO ELECTRONIC TIMETABLES WAS NOT SENT");
    }


    /**
     * Searches list of timetable rows with all train numbers and ids,
     * departing or arriving to given station during given day
     * and their arriving and departing time.
     * @param date - date for timetable
     * @param stationName - station name for timetable
     */
    @Transactional(readOnly = true)
    @Override
    public List<AdminTimeTableItemDto> getTimeTable(String stationName, String date) {
        log.info("GETTING TIMETABLE: " +stationName + " " + date);
        LocalDate localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        List<RouteNode> routeNodeList = routeNodeRepo
                .findRouteNodesByStationAndDate(stationRepo.findStationByName(stationName), localDate);
        return createAdminTimeTableItemDto(routeNodeList);
    }

    /**
     * Searches for all passengers, registered to train with given id and returns information about them
     * @param trainId - id of train
     */
    @Transactional(readOnly = true)
    @Override
    @SuppressWarnings("unchecked")
    public List<UserForAdminRequestDto> getPassengers(long trainId) {
        List<Long> carriageIds = trainRepo.findCarriagesByTrainId(trainId).stream()
                .map(Carriage::getId).collect(Collectors.toList());
        List<UserForAdminRequestDto> result = ticketRepo.findTicketsByCarriageIds(carriageIds);
        Collections.sort(result);
        return result;
    }

    private RouteNode fillRouteNodeInfo(RouteCreationDto station, Train train) {
        RouteNode routeNode = new RouteNode();
        routeNode.setArrivalTime(LocalDateTime.parse(station.getArrivingTime(), formatter));
        routeNode.setDepartureTime(LocalDateTime.parse(station.getDepartureTime(), formatter));
        routeNode.setStation(stationRepo.findStationByName(station.getStationName()));
        routeNode.setTrain(train);
        return routeNode;
    }

    private Carriage fillCarriageInfo(CarriageForTrainCreationDto dto, Train train) {
        Carriage carriage = new Carriage();
        carriage.setNumber(dto.getCarName());
        carriage.setCapacity(Integer.parseInt(dto.getCarCapacity()));
        carriage.setCarriageType(CarriageType.valueOf(dto.getCarType()));
        carriage.setTrain(train);
        return carriage;
    }

    private PricePart fillPricePart(Station previousStation, Station currentStation,
                                    RouteCreationDto dto, Train train, CarriageType carType) {
        PricePart newPricePart = new PricePart();
        newPricePart.setStartStation(previousStation);
        newPricePart.setEndStation(currentStation);
        switch (carType) {
            case BUSINESS:
                newPricePart.setPrice(Double.parseDouble(dto.getBusinessPrice()));
                break;
            case COMFORT:
                newPricePart.setPrice(Double.parseDouble(dto.getComfortPrice()));
                break;
            case ECONOMY:
                newPricePart.setPrice(Double.parseDouble(dto.getEconomyPrice()));
                break;
        }
        newPricePart.setTrain(train);
        newPricePart.setCarriageType(carType);
        return newPricePart;
    }

    private List<AdminTimeTableItemDto> createAdminTimeTableItemDto(List<RouteNode> routeNodeList) {
        return routeNodeList.stream().map(routeNode -> {
            AdminTimeTableItemDto adminTimeTableItemDto = new AdminTimeTableItemDto();
            adminTimeTableItemDto.setTrainNumber(routeNode.getTrain().getName());
            adminTimeTableItemDto.setTrainId(routeNode.getTrain().getId());
            return adminTimeTableItemDto;
        }).collect(Collectors.toList());
    }

    private boolean changesTodayTimetable(RouteCreationDto station) {
        LocalDateTime arrivalTime = LocalDateTime.parse(station.getArrivingTime(), formatter);
        LocalDateTime departureTime = LocalDateTime.parse(station.getDepartureTime(), formatter);
        LocalDateTime todayStart = LocalDate.now().atStartOfDay();
        LocalDateTime todayEnd = LocalDate.now().plusDays(1).atStartOfDay();
        return (arrivalTime.isAfter(todayStart) && arrivalTime.isBefore(todayEnd))
                || (departureTime.isAfter(todayStart) && departureTime.isBefore(todayEnd));
    }
}
