package com.railroad.service;

import com.railroad.dto.admin.TrainCreationDto;
import com.railroad.dto.user.TicketInfo;

public interface Validator {
    void validateTrain(TrainCreationDto trainCreationDto);
    boolean isPlaceFree(long carriageId, int placeNumber, long startStationId, long endStationId);
    void validateTime(TicketInfo ticketInfo);
    void validateSameTrainTickets(TicketInfo ticketInfo);
    void validateUserLoginFree(String login);
    void validateUserPhoneFree(String phone);
    void validateUserEmailFree(String email);
}
