package com.railroad.service.registraion;

import com.railroad.domain.User;
import com.railroad.service.mail.MyMailSender;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@PropertySource("classpath:application.properties")
@RequiredArgsConstructor
@Slf4j
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    private final RegistrationService registrationService;
    private final MyMailSender myMailSender;

    @Value("${mail.deploymentHostUrl}")
    private String deploymentHostUrl;

    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(OnRegistrationCompleteEvent event) {
        log.info("CREATING TOKEN FOR USER: " + event.getUser().getId());
        User user = event.getUser();
        String token = UUID.randomUUID().toString();
        registrationService.createVerificationToken(user, token);
        String conformationUrl =
                deploymentHostUrl + event.getAppUrl() + "/unregistered/confirmEmail?token=" + token;
        myMailSender.sendConformationLink(conformationUrl, user.getEmail());
    }


}
