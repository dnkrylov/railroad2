package com.railroad.service.registraion;

import com.railroad.domain.User;
import com.railroad.domain.UserRole;
import com.railroad.domain.VerificationToken;
import com.railroad.dto.user.RegistrationUserDto;
import com.railroad.dto.user.UserDto;
import com.railroad.exceptions.CaptchaVerificationException;
import com.railroad.exceptions.SuchUserAlreadyExistsException;
import com.railroad.repo.UserRepo;
import com.railroad.response.BusinessResponse;
import com.railroad.service.Validator;
import com.railroad.service.captcha.CaptchaVerificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
@Slf4j
public class RegistrationServiceImpl implements RegistrationService {

    private final ApplicationEventPublisher applicationEventPublisher;
    private final UserRepo userRepo;
    private final Validator validator;
    private final CaptchaVerificationService captchaVerificationService;

    @Transactional
    @Override
    public BusinessResponse addUser(RegistrationUserDto registrationUserDto, WebRequest request) {
        if (!captchaVerificationService.verifyCaptchaResponse(registrationUserDto.getCaptchaResponse())) {
            log.info("CAPTCHA VERIFICATION FAILED FOR: " + registrationUserDto);
            throw new CaptchaVerificationException("CAPTCHA VERIFICATION FAILED");
        }
        UserDto userDto = registrationUserDto.getUserDto();
        userDto.setUserRole(UserRole.ROLE_USER);
        validator.validateUserLoginFree(userDto.getLogin());
        validator.validateUserPhoneFree(userDto.getPhone());
        validator.validateUserEmailFree(userDto.getEmail());
        User user = new User(userDto);
        try {
            user.setId(userRepo.addUser(user));
        } catch (ConstraintViolationException e) {
            log.error(e.getMessage(), e);
            throw new SuchUserAlreadyExistsException("SUCH USER ALREADY EXISTS");
        }
        String appUrl = request.getContextPath();
        applicationEventPublisher.publishEvent(new OnRegistrationCompleteEvent(user, appUrl));
        log.info("USER CREATED, ID: " + user.getId());
        return new BusinessResponse("User created");
    }

    @Transactional
    @Override
    public void createVerificationToken(User user, String token) {
        log.info("CREATING VERIFICATION TOKEN FOR USER: " + user.getId());
        VerificationToken verificationToken = new VerificationToken(user, token);
        userRepo.addVerificationToken(verificationToken);
    }

    @Transactional
    @Override
    public String verifyEmail(String token) {
        log.info("VERIFYING EMAIL, TOKEN: " + token);
        VerificationToken verificationToken = userRepo.getVerificationToken(token);
        if(verificationToken == null) {
            return "TOKEN NOT FOUND";
        }
        User user = verificationToken.getUser();
        if(verificationToken.getExpireDateTime().isBefore(LocalDateTime.now())) {
            return "TOKEN NOT FOUND";
        }
        user.setEnabled(true);
        userRepo.updateUser(user);
        return "EMAIL CONFIRMED";
    }
}
