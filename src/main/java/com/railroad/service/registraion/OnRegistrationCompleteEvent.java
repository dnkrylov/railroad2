package com.railroad.service.registraion;

import com.railroad.domain.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.context.ApplicationEvent;

@EqualsAndHashCode(callSuper = true)
@Data
class OnRegistrationCompleteEvent extends ApplicationEvent {
    private User user;
    private String appUrl;

    OnRegistrationCompleteEvent(User user, String appUrl) {
        super(user);
        this.user = user;
        this.appUrl = appUrl;
    }
}
