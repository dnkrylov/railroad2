package com.railroad.service.registraion;


import com.railroad.domain.User;
import com.railroad.dto.user.RegistrationUserDto;
import com.railroad.response.BusinessResponse;
import org.springframework.web.context.request.WebRequest;

public interface RegistrationService {
    BusinessResponse addUser(RegistrationUserDto registrationUserDto, WebRequest request);
    void createVerificationToken(User user, String token);
    String verifyEmail(String token);
}
