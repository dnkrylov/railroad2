package com.railroad.service;

import com.railroad.domain.Carriage;
import com.railroad.domain.RouteNode;
import com.railroad.domain.Station;
import com.railroad.domain.User;
import com.railroad.dto.user.*;
import com.railroad.exceptions.*;
import com.railroad.repo.RouteNodeRepo;
import com.railroad.repo.StationRepo;
import com.railroad.repo.TrainRepo;
import com.railroad.repo.UserRepo;
import com.railroad.response.BusinessResponse;
import com.railroad.service.mail.MyMailSender;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service for implementing admin functions
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    private final StationRepo stationRepo;
    private final RouteNodeRepo routeNodeRepo;
    private final TicketService ticketService;
    private final TrainRepo trainRepo;
    private final Validator validator;
    private final UserRepo userRepo;
    private final SecurityService securityService;
    private final MyMailSender myMailSender;

    /**
     * Search for all stations and returns their names
     * @return List of Station names
     */
    @Transactional(readOnly = true)
    @Override
    public List<String> getStationNamesList() {
        return stationRepo.findAllStations().stream()
                .map(Station::getName).sorted().collect(Collectors.toList());
    }

    /**
     * Search list of timetable rows with all train numbers, departing or arriving to given station during given day
     * and their arriving and departing time.
     * @param date        - date for timetable
     * @param stationName - station name for timetable
     */
    @Transactional(readOnly = true)
    @Override
    public List<TimeTableItemDto> getTimeTableItems(String stationName, String date) {
        log.info("GETTING TIMETABLE: " + stationName + " " + date);
        LocalDate localDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        List<RouteNode> routeNodeList = routeNodeRepo
                .findRouteNodesByStationAndDate(stationRepo.findStationByName(stationName), localDate);
        return getTimeTableItemDtos(routeNodeList);
    }

    /**
     * Searches for trains, followings directly from start station to end station without transfer.
     * Returns information about such trains (departing and arriving time, trainID and train number)
     *
     * @param startStationName - start route station
     * @param endStationName   - final route station
     * @param routeDate        - date, when passenger wanted to start/finish travel
     * @param isItArrivingDate - if false - method will return trains, that started at given date,
     *                         else - that arriving to finish station at given date
     */
    @Transactional(readOnly = true)
    @Override
    public List<AvailableDirectRouteDto> findDirectRoutes(String startStationName, String endStationName,
                                                          String routeDate, boolean isItArrivingDate) {
        log.info("SEARCHING DIRECT ROUTES: START STATION, END STATION, DATE, ARRIVING TIME : " +
                startStationName + " " + endStationName + " " + routeDate + " " + isItArrivingDate);
        LocalDate date = LocalDate.parse(routeDate, DATE_FORMATTER);
        return isItArrivingDate ? routeNodeRepo.findDirectTrainsByStartStationEndStationAndFinishDate(
                startStationName, endStationName, date) :
                routeNodeRepo.findDirectTrainsByStartStationEndStationAndStartDate(
                        startStationName, endStationName, date);
    }

    /**
     * Searches for trains followings from start station to end station with one transfer.
     * Returns information about such trains, departing/arriving times and transfer station
     *
     * @param startStationName - start route station
     * @param endStationName   - final route station
     * @param routeDate        - date, when passenger wanted to start/finish travel
     * @param isItArrivingDate - if false - method will return trains, that started at given date,
     *                         else - that arriving to finish station at given date
     */
    @Transactional(readOnly = true)
    @Override
    public List<AvailableTransferRouteDto> findTransferRoutes(String startStationName, String endStationName,
                                                              String routeDate, boolean isItArrivingDate) {
        log.info("SEARCHING TRANSFER ROUTES: START STATION, END STATION, DATE, ARRIVING TIME : " +
                startStationName + " " + endStationName + " " + routeDate + " " + isItArrivingDate);
        LocalDate date = LocalDate.parse(routeDate, DATE_FORMATTER);
        return isItArrivingDate ? routeNodeRepo.findTransferTrainsByStartStationEndStationAndFinishDate(
                startStationName, endStationName, date) :
                routeNodeRepo.findTransferTrainsByStartStationEndStationAndStartDate(
                        startStationName, endStationName, date);
    }

    /**
     * Searches for carriages of a train, selected to travel.

     * Returns list of carriage DTO objects (id, type, number, free places and travel price) for each carriage
     * Removes temporary tickets (which was reserved more than to minutes ago and not bought)
     * If user already has booked tickets, they will be deleted too.
     * @param ticketInfo contains information about route and selected train
     */
    @Transactional
    @Override
    public List<CarriageDto> getCarriages(TicketInfo ticketInfo) {
        log.info("SEARCHING DIRECT CARRIAGES: " + ticketInfo);
        ticketInfo.setUserId(securityService.getLoggedInUser().getId());
        ticketService.deleteTemporaryTicketsForCurrentUser(ticketInfo.getUserId());
        Station startStation = stationRepo.findStationByName(ticketInfo.getStartStationName());
        Station endStation = stationRepo.findStationByName(ticketInfo.getEndStationName());
        List<CarriageDto> carriageDtos =
                getCarriageDtosFromCarriages(trainRepo.findCarriagesByTrainId(ticketInfo.getTrainId()));
        prepareCarriageDtos(carriageDtos, startStation, endStation, ticketInfo.getTrainId());
        return carriageDtos;
    }

    /**
     * Searches carriages of both trains (before and after transfer) for selected travel route.
     * Returns DTO with two carriages lists (first and second train), each list contains information about
     * id, type, number, free places and travel price for each carriage
     * Removes temporary tickets (which was reserved more than to minutes ago and not bought)
     * If user already has booked tickets, they will be deleted too.
     * @param transferTicketInfo information about selected route (trains ids, start, finish and transfer station)
     */
    @Transactional
    @Override
    public TransferCarriages getTransferCarriages(TransferTicketInfo transferTicketInfo) {
        log.info("SEARCHING DIRECT CARRIAGES: " + transferTicketInfo);
        transferTicketInfo.setUserId(securityService.getLoggedInUser().getId());
        ticketService.deleteTemporaryTicketsForCurrentUser(transferTicketInfo.getUserId());
        Station startStation = stationRepo.findStationByName(transferTicketInfo.getStartStationName());
        Station middleStation = stationRepo.findStationByName(transferTicketInfo.getMiddleStationName());
        Station endStation = stationRepo.findStationByName(transferTicketInfo.getEndStationName());
        List<CarriageDto> carriageDtos1 =
                getCarriageDtosFromCarriages(trainRepo.findCarriagesByTrainId(transferTicketInfo.getTrainId1()));
        prepareCarriageDtos(carriageDtos1, startStation, middleStation, transferTicketInfo.getTrainId1());
        List<CarriageDto> carriageDtos2 =
                getCarriageDtosFromCarriages(trainRepo.findCarriagesByTrainId(transferTicketInfo.getTrainId2()));
        prepareCarriageDtos(carriageDtos2, middleStation, endStation, transferTicketInfo.getTrainId2());
        TransferCarriages transferCarriages = new TransferCarriages();
        transferCarriages.setTrain1Carriages(carriageDtos1);
        transferCarriages.setTrain2Carriages(carriageDtos2);
        return transferCarriages;
    }

    /**
     * Reserve selected place for 10 minutes by creating temporary ticket with negative price.
     * if user will not buy reserved ticket in 10 minute, such ticket can be deleted and place will be able to book
     * by another user
     * @param ticketInfo - information about selected carriage, train and place
     * @throws ThisPlaceIsAlreadyBusyException if selected place was booked by another user during place choosing
     *                                         or if two users booking one place at same time
     */
    @Transactional
    public BusinessResponse createTemporaryTicket(TicketInfo ticketInfo) {
        log.info("CREATING DIRECT TEMPORARY TICKET " + ticketInfo);
        ticketInfo.setUserId(securityService.getLoggedInUser().getId());
        ticketInfo.setStartStationId(stationRepo.findStationByName(ticketInfo.getStartStationName()).getId());
        ticketInfo.setEndStationId(stationRepo.findStationByName(ticketInfo.getEndStationName()).getId());
        ticketService.deleteTemporaryTicketsForCurrentUser(ticketInfo.getUserId());
        ticketInfo.setTrainId(trainRepo.findTrainIdByCarriageId(ticketInfo.getCarriageId()));
        ticketInfo.setTicketPrice(ticketService.getPrice(ticketInfo.getTrainId(),
                trainRepo.findCarriageById(ticketInfo.getCarriageId()).getCarriageType(),
                ticketInfo.getStartStationId(), ticketInfo.getEndStationId()));
        if (!validator.isPlaceFree(ticketInfo.getCarriageId(), ticketInfo.getPlaceNumber(),
                ticketInfo.getStartStationId(), ticketInfo.getEndStationId())) {
            log.info("TEMPORARY TICKET NOT CREATED, THIS TICKET WAS ALREADY BOUGHT BY ANOTHER USER");
            throw new ThisPlaceIsAlreadyBusyException("IT LOOKS LIKE THIS TICKET WAS ALREADY BOUGHT." +
                    " TRY TO SELECT ANOTHER ONE");
        }
        ticketInfo.setTicketId(ticketService.createTemporaryTicket(ticketInfo));
        log.info("TICKET CREATED, ID: " + ticketInfo.getTicketId());
        return new BusinessResponse("TICKET PRICE: " + ticketInfo.getTicketPrice());
    }

    /**
     * Reserve selected places for 10 minutes by creating temporary tickets with negative prices.
     * if user will not buy reserved tickets in 10 minute, such tickets can be deleted and places will be able to book
     * by another user
     * @param transferTicketInfo - information about chosen trains, carriages and places
     * @throws ThisPlaceIsAlreadyBusyException if selected place was booked by another user during place choosing
     *                                         or if two users booking one place at same time
     */
    @Transactional
    @Override
    public BusinessResponse createTransferTemporaryTickets(TransferTicketInfo transferTicketInfo) {
        log.info("CREATING TRANSFER TEMPORARY TICKET " + transferTicketInfo);
        transferTicketInfo.setStartStationId(stationRepo.findStationByName(
                transferTicketInfo.getStartStationName()).getId());
        transferTicketInfo.setMiddleStationId(stationRepo.findStationByName(
                transferTicketInfo.getMiddleStationName()).getId());
        transferTicketInfo.setEndStationId(stationRepo.findStationByName(
                transferTicketInfo.getEndStationName()).getId());
        transferTicketInfo.setUserId(securityService.getLoggedInUser().getId());
        ticketService.deleteTemporaryTicketsForCurrentUser(transferTicketInfo.getUserId());

        transferTicketInfo.setTicketPrice1(ticketService.getPrice(trainRepo.findTrainIdByCarriageId(
                transferTicketInfo.getCarriageId1()),
                trainRepo.findCarriageById(transferTicketInfo.getCarriageId1()).getCarriageType(),
                transferTicketInfo.getStartStationId(), transferTicketInfo.getMiddleStationId()));
        if (!validator.isPlaceFree(transferTicketInfo.getCarriageId1(), transferTicketInfo.getPlaceNumber1(),
                transferTicketInfo.getStartStationId(), transferTicketInfo.getMiddleStationId())) {
            log.info("TEMPORARY TICKET1 NOT CREATED, THIS TICKET WAS ALREADY BOUGHT BY ANOTHER USER");
            throw new ThisPlaceIsAlreadyBusyException("IT LOOKS LIKE CHOSEN TICKET IN FROM " +
                    transferTicketInfo.getStartStationName() + " TO " + transferTicketInfo.getMiddleStationName() +
                    " WAS ALREADY BOUGHT \n TRY TO SELECT ANOTHER ONE");
        }

        transferTicketInfo.setTicketPrice2(ticketService.getPrice(
                trainRepo.findTrainIdByCarriageId(transferTicketInfo.getCarriageId2()),
                trainRepo.findCarriageById(transferTicketInfo.getCarriageId2()).getCarriageType(),
                transferTicketInfo.getMiddleStationId(), transferTicketInfo.getEndStationId()));
        if (!validator.isPlaceFree(transferTicketInfo.getCarriageId2(), transferTicketInfo.getPlaceNumber2(),
                transferTicketInfo.getMiddleStationId(), transferTicketInfo.getEndStationId())) {
            log.info("TEMPORARY TICKET2 NOT CREATED, THIS TICKET WAS ALREADY BOUGHT BY ANOTHER USER");
            throw new ThisPlaceIsAlreadyBusyException("IT LOOKS LIKE CHOSEN TICKET IN FROM " +
                    transferTicketInfo.getMiddleStationName() + " TO " + transferTicketInfo.getEndStationName() +
                    " WAS ALREADY BOUGHT \n TRY TO SELECT ANOTHER ONE");
        }

        transferTicketInfo.setTicketId1(ticketService.createTemporaryTicket(
                new TicketInfo(transferTicketInfo.getPlaceNumber1(), transferTicketInfo.getCarriageId1(),
                        transferTicketInfo.getUserId())));
        transferTicketInfo.setTicketId2(ticketService.createTemporaryTicket(
                new TicketInfo(transferTicketInfo.getPlaceNumber2(), transferTicketInfo.getCarriageId2(),
                        transferTicketInfo.getUserId())));
        log.info("TEMPORARY TICKETS CREATED, ID1: " + transferTicketInfo.getTicketId1() +
                "ID2: " + transferTicketInfo.getTicketId2());
        return new BusinessResponse("TICKET PRICE: " +
                (transferTicketInfo.getTicketPrice1() + transferTicketInfo.getTicketPrice2()));
    }

    /**
     * validates that reserved ticket is not bought by another user (after 10 minutes reservation), train has
     * not departed or not departing in 10 minutes and user dont have another tickets to same train.
     * if validate successfully, method changes ticket price from negative to actual.
     * Such tickets cant be deleted by "deleteTrashTickets" and "deleteTemporaryTicketsForCurrentUser"
     * methods.
     *
     * @throws ToLateException if ticket was booked more than 10 minutes ago, reservations was finished
     * and ticket was bought  by another user or train has already departed
     * @throws DuplicatingTicketException if user already have ticket for given train
     * @param ticketInfo - information about ticket
     */
    @Transactional
    @Override
    public BusinessResponse upgradeTemporaryDirectTicket(TicketInfo ticketInfo) {
        log.info("UPGRADING TEMP TICKET: " + ticketInfo.getTicketId());
        validator.validateTime(ticketInfo);
        validator.validateSameTrainTickets(ticketInfo);
        return ticketService.upgradeTemporaryTicket(ticketInfo);
    }

    /**
     * validates that reserved tickets is not bought by another user (after 10 minutes reservation), trains
     * have not departed or not departing in 10 minutes and user dont have another tickets to same trains.
     * if validate successfully, method changes ticket prices price from negative to actual.
     * Such tickets cant be deleted by "deleteTrashTickets" and "deleteTemporaryTicketsForCurrentUser"
     * methods.
     *
     * @throws ToLateException if tickets was booked more than 10 minutes ago, reservations was finished
     * and one or both tickets were bought  by another user or train has already departed
     * @throws DuplicatingTicketException if user already have ticket for one of given trains
     * @param transferTicketInfo - information about tickets
     */
    @Transactional
    @Override
    public BusinessResponse upgradeTemporaryTransferTicket(TransferTicketInfo transferTicketInfo) {
        log.info("UPGRADING TEMP TICKETS, ID1: " + transferTicketInfo.getTicketId1() +
                " ID2: " + transferTicketInfo.getTicketId2());
        TicketInfo firstTicket = new TicketInfo(transferTicketInfo.getUserId(), transferTicketInfo.getStartStationId(),
                transferTicketInfo.getMiddleStationId(), transferTicketInfo.getTicketId1(),
                transferTicketInfo.getTicketPrice1(), transferTicketInfo.getTrainId1());
        TicketInfo secondTicket = new TicketInfo(transferTicketInfo.getUserId(), transferTicketInfo.getMiddleStationId(),
                transferTicketInfo.getEndStationId(), transferTicketInfo.getTicketId2(),
                transferTicketInfo.getTicketPrice2(), transferTicketInfo.getTrainId2());
        upgradeTemporaryDirectTicket(firstTicket);
        return upgradeTemporaryDirectTicket(secondTicket);
    }

    /**
     * Return detailed information about current logged in user
     */
    @Override
    public UserDto getUserDto() {
        return new UserDto(securityService.getLoggedInUser().getUser());
    }

    /**
     * Updates user information
     *
     * @param userDto - DTO with new information about logged in user
     * @return BusinessResponse with success message
     * @throws SuchUserAlreadyExistsException if new login, phone or email is busy
     */
    @Transactional
    @Override
    public BusinessResponse updateUser(UserDto userDto) {
        log.info("UPDATING USER: " + userDto);
        User oldUser = securityService.getLoggedInUser().getUser();
        if(!userDto.getEmail().equals(oldUser.getEmail())) {
            validator.validateUserEmailFree(userDto.getEmail());
        }
        if(!userDto.getPhone().equals(oldUser.getPhone())) {
            validator.validateUserPhoneFree(userDto.getPhone());
        }
        if(!userDto.getLogin().equals(oldUser.getLogin())) {
            validator.validateUserLoginFree(userDto.getLogin());
        }
        User user = new User(userDto);
        user.setUserRole(oldUser.getUserRole());
        user.setEnabled(true);
        user.setId(oldUser.getId());
        try{
            userRepo.updateUser(user);
        } catch (ConstraintViolationException e){
            log.info("USER NOT UPDATED" + e.getMessage(), e);
            throw new SuchUserAlreadyExistsException("Such user already exists");
        }
        securityService.getLoggedInUser().setUser(user);
        log.info("USER UPDATED");
        return new BusinessResponse("information updated");
    }

    /**
     * Searches for all ticket of logged in user and returns brief information about them
     */
    @Override
    public List<TicketPreviewDto> getTicketPreview() {
        long userId = securityService.getLoggedInUser().getId();
        log.info("GETTING TICKETS FOR USER: " + userId);
        return ticketService.getTicketsPreview(userId);
    }

    /**
     * Validates, that logged in user is owner of requested ticket.
     * If true - returns detailed information about ticket, if false-returns null
     */
    @Override
    public TicketDetailsDto getTicketDetails(long ticketId) {
        long userId = securityService.getLoggedInUser().getId();
        log.info("GETTING TICKETS DETAILS,  USER: " + userId + "TICKET: " + ticketId);
        return ticketService.getTicketDetails(ticketId, userId);
    }

    /**
     * Validates, that logged in user is owner of requested ticket.
     * If true - sends ticket to user email, if false-returns null
     * @param ticketId - requested ticket id
     */
    @Override
    public BusinessResponse sendTicketByEmail(long ticketId) {
        log.info("SENDING TICKET TO EMAIL, ID: " + ticketId);
        User loggedInUser = securityService.getLoggedInUser().getUser();
        TicketDetailsDto ticketDetailsDto = ticketService.getTicketDetails(ticketId, loggedInUser.getId());
        String email = loggedInUser.getEmail();
        return ticketDetailsDto==null ? null : myMailSender.sendTicketByEmail(ticketDetailsDto, email);
    }

    private List<TimeTableItemDto> getTimeTableItemDtos(List<RouteNode> routeNodeList) {
        return routeNodeList.stream().map(routeNode -> {
            TimeTableItemDto timeTableItemDto = new TimeTableItemDto();
            timeTableItemDto.setArrivalTime(routeNode.getArrivalTime().format(DATE_TIME_FORMATTER));
            timeTableItemDto.setDepartureTime(routeNode.getDepartureTime().format(DATE_TIME_FORMATTER));
            timeTableItemDto.setTrainNumber(routeNode.getTrain().getName());
            return timeTableItemDto;
        }).collect(Collectors.toList());
    }

    private List<CarriageDto> getCarriageDtosFromCarriages(List<Carriage> carriages) {
        List<CarriageDto> result = new ArrayList<>();
        for (Carriage carriage : carriages) {
            CarriageDto carriageDto = new CarriageDto();
            carriageDto.setCarriageId(carriage.getId());
            carriageDto.setCapacity(carriage.getCapacity());
            carriageDto.setNumber(carriage.getNumber());
            carriageDto.setCarriageType(carriage.getCarriageType());
            result.add(carriageDto);
        }
        return result;
    }

    private void prepareCarriageDtos(List<CarriageDto> carriageDtos, Station startStation, Station endStation,
                                     long trainId) {
        if (carriageDtos.isEmpty()) {
            throw new InvalidRequestParamException("Invalid train");
        }
        for (CarriageDto carriage : carriageDtos) {
            List<Integer> freePlacesNumbers = new ArrayList<>();
            ticketService.deleteTrashTickets(carriage.getCarriageId());
            for (int i = 1; i <= carriage.getCapacity(); i++) {
                if (validator.isPlaceFree(carriage.getCarriageId(), i,
                        startStation.getId(), endStation.getId())) {
                    freePlacesNumbers.add(i);
                }
            }
            carriage.setFreePlaceNumbers(freePlacesNumbers);
            carriage.setTicketPrice(ticketService.getPrice(trainId, carriage.getCarriageType(), startStation.getId(),
                    endStation.getId()));
        }
    }


}
