package com.railroad.service.captcha;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Class for mapping Google captcha service response
 * Uses only "success" field we are interested in
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {
    private Boolean success;
}
