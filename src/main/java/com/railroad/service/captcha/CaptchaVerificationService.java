package com.railroad.service.captcha;

public interface CaptchaVerificationService {
    boolean verifyCaptchaResponse(String captchaResponse);
}
