package com.railroad.service.captcha;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.railroad.exceptions.ConnectionException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Service for verification Google Captcha
 */
@Component
@PropertySource("classpath:application.properties")
@Slf4j
public class CaptchaVerificationServiceImpl implements CaptchaVerificationService {

    @Value("${google.recaptcha.key.secret}")
    private String key = "6LdVd7UUAAAAAAkxCnop-fM3jDJKyIntzw5vdKfF";

    /**
     * Sends HTTP request to Google captcha service with response from ui.
     * If Google response success field is empty - return false, or return its value
     * @param captchaResponse response of captcha UI plugin
     * @throws ConnectionException if cant connect to google service
     */
    @Override
    public boolean verifyCaptchaResponse(String captchaResponse) {
        log.info("VERIFYING CAPTCHA");
        try (CloseableHttpClient httpClient = HttpClients.createDefault()){
            String uri = "https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s";
            HttpPost httpPost = new HttpPost(String.format(uri,key,captchaResponse));
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity entity = httpResponse.getEntity();
            String responseStr = IOUtils.toString(entity.getContent(), StandardCharsets.UTF_8);
            Response response = new ObjectMapper().readValue(responseStr, Response.class);
            log.info("RESULT: " + response.getSuccess());
            return response.getSuccess() == null ? false : response.getSuccess();
        } catch (IOException e) {
            log.error("CAPTCHA VERIFICATION ERROR: " + e.getMessage(), e);
            throw new ConnectionException("Can not connect to google service");
        }
    }
}
