package com.railroad.service;

import com.railroad.dto.UserDetailsDto;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * Service for getting current logged in user
 */
@Service
public class SecurityServiceImpl implements SecurityService {
    /**
     *
     * @return logged in user from SecurityContext
     */
    public UserDetailsDto getLoggedInUser() {
        return (UserDetailsDto) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
