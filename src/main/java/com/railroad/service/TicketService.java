package com.railroad.service;

import com.railroad.domain.CarriageType;
import com.railroad.dto.user.TicketDetailsDto;
import com.railroad.dto.user.TicketInfo;
import com.railroad.dto.user.TicketPreviewDto;
import com.railroad.response.BusinessResponse;

import java.util.List;

public interface TicketService {
    long createTemporaryTicket(TicketInfo ticketInfo);
    void deleteTrashTickets(long carriageId);
    BusinessResponse upgradeTemporaryTicket(TicketInfo ticketInfo);
    void deleteTemporaryTicketsForCurrentUser(long userId);
    double getPrice(long trainId, CarriageType carriageType, long startStationId, long endStationId);
    List<TicketPreviewDto> getTicketsPreview(long userId);
    TicketDetailsDto getTicketDetails(long ticketId, long userId);
}
