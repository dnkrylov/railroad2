package com.railroad.service;


import com.railroad.domain.Carriage;
import com.railroad.domain.CarriageType;
import com.railroad.domain.Ticket;
import com.railroad.dto.admin.CarriageForTrainCreationDto;
import com.railroad.dto.admin.RouteCreationDto;
import com.railroad.dto.admin.TrainCreationDto;
import com.railroad.dto.user.TicketInfo;
import com.railroad.exceptions.DuplicatingTicketException;
import com.railroad.exceptions.InvalidRequestParamException;
import com.railroad.exceptions.SuchUserAlreadyExistsException;
import com.railroad.exceptions.ToLateException;
import com.railroad.repo.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ValidatorImpl implements Validator {
    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy:HH.mm");

    private final StationRepo stationRepo;
    private final RouteNodeRepo routeNodeRepo;
    private final TicketRepo ticketRepo;
    private final TrainRepo trainRepo;
    private final UserRepo userRepo;

    @Transactional(readOnly = true)
    @Override
    public void validateTrain(TrainCreationDto trainCreationDto) {
        log.info("VALIDATING TRAIN");
        validateCarriages(trainCreationDto.getCarriages());
        validateStations(trainCreationDto.getStationList());
    }

    private void validateCarriages(List<CarriageForTrainCreationDto> carriages) {
        List<String> carriageNames = new ArrayList<>();
        List<String> carriageTypes = new ArrayList<>();
        for (CarriageType type : CarriageType.values()) {
            carriageTypes.add(type.toString());
        }
        for (CarriageForTrainCreationDto carriage : carriages) {
            if (carriageNames.contains(carriage.getCarName())) {
                throw new InvalidRequestParamException("duplicate carriage numbers: " + carriage.getCarName());
            }
            if (!carriageTypes.contains(carriage.getCarType())) {
                throw new InvalidRequestParamException("carriage " + carriage.getCarName() + ": invalid type value");
            }
            carriageNames.add(carriage.getCarName());
        }
    }

    @Transactional(readOnly = true)
    public void validateStations(List<RouteCreationDto> stations) {
        List<String> stationNames = new ArrayList<>();
        for (int i = 0; i < stations.size(); i++) {
            RouteCreationDto station = stations.get(i);
            if (i != 0) {
                if (stationNames.contains(station.getStationName())) {
                    throw new InvalidRequestParamException("duplicate stations: " + station.getStationName());
                }
                if (!LocalDateTime.parse(station.getArrivingTime(), dtf)
                        .isAfter(LocalDateTime.parse(stations.get(i - 1).getDepartureTime(), dtf))) {
                    throw new InvalidRequestParamException(
                            "arriving to station " + (i+1) + " is before departing from station " + (i));
                }
            } else {
                if (LocalDateTime.parse(station.getArrivingTime(), dtf)
                        .isBefore(LocalDateTime.now())) {
                    throw new InvalidRequestParamException(
                            "invalid " + station.getStationName() + " arrival time (PAST)");
                }
            }
            if (LocalDateTime.parse(station.getDepartureTime(),dtf).isBefore(
                    LocalDateTime.parse(station.getArrivingTime(), dtf))) {
                throw new InvalidRequestParamException(station.getStationName() + ": departing before arriving");
            }
            if (stationRepo.findStationByName(station.getStationName()) == null) {
                throw new InvalidRequestParamException(station.getStationName() + ": station does not exist");
            }
            stationNames.add(station.getStationName());
        }
    }

    @Transactional(readOnly = true)
    @Override
    public boolean isPlaceFree(long carriageId, int placeNumber, long startStationId, long endStationId) {
        List<Ticket> tickets = ticketRepo.findTicketByCarriageIdAndPlaceNumber(carriageId, placeNumber);
        if (tickets.isEmpty()) {
            return true;
        }
        List<Long> sortedStationIdList = routeNodeRepo.findSortedStationIdsByTrainId(
                trainRepo.findTrainIdByCarriageId(carriageId));
        for (Ticket ticket : tickets) {
            if (ticket.getPrice() < 0) {
                return false;
            }
            int ticketStartIndex = sortedStationIdList.indexOf(ticket.getStartStation().getId());
            int ticketEndIndex = sortedStationIdList.indexOf(ticket.getFinishStation().getId());
            int requiredStartIndex = sortedStationIdList.indexOf(startStationId);
            int requiredEndIndex = sortedStationIdList.indexOf(endStationId);
            if (ticketStartIndex < requiredEndIndex && ticketEndIndex > requiredStartIndex) {
                return false;
            }
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void validateTime(TicketInfo ticketInfo) {
        LocalDateTime departureTime = routeNodeRepo
                .findDepartureTimeByStationIdAndTrainId(ticketInfo.getStartStationId(), ticketInfo.getTrainId());
        LocalDateTime now = LocalDateTime.now();
        if (now.plusMinutes(10).isAfter(departureTime)) {
            throw new ToLateException("To late to buy this ticket. Drain is departing / already departed");
        }
    }

    @Transactional(readOnly = true)
    @Override
    public void validateSameTrainTickets(TicketInfo ticketInfo) {
        List<Carriage> carriages = trainRepo.findCarriagesByTrainId(ticketInfo.getTrainId());
        for (Carriage carriage : carriages) {
            List<Ticket> tickets = ticketRepo
                    .findTicketsByCarriageIdAndUserId(carriage.getId(), ticketInfo.getUserId());
            if (tickets.size() > 1 || (tickets.size() == 1 && tickets.get(0).getPrice() > 0)) {
                throw new DuplicatingTicketException("YOU ALREADY HAVE A TICKET TO THIS TRAIN");
            }
        }
    }

    @Override
    public void validateUserLoginFree(String login) {
        if (userRepo.findUserByLogin(login) != null) {
            log.info("LOGIN " + login + " IS BUSY, RETURN");
            throw new SuchUserAlreadyExistsException("User with same LOGIN is already registered");
        }
    }

    @Override
    public void validateUserPhoneFree(String phone) {
        if (userRepo.findUserByPhone(phone) != null) {
            log.info("PHONE " + phone + " IS BUSY, RETURN");
            throw new SuchUserAlreadyExistsException("User with same PHONE is already registered");
        }
    }

    @Override
    public void validateUserEmailFree(String email) {
        if (userRepo.findUserByEmail(email) != null) {
            log.info("EMAIL " + email + " IS BUSY, RETURN");
            throw new SuchUserAlreadyExistsException("User with same EMAIL is already registered");
        }
    }
}
