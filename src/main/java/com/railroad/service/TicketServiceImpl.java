package com.railroad.service;

import com.railroad.domain.CarriageType;
import com.railroad.domain.Ticket;
import com.railroad.dto.user.TicketDetailsDto;
import com.railroad.dto.user.TicketInfo;
import com.railroad.dto.user.TicketPreviewDto;
import com.railroad.exceptions.InvalidRequestParamException;
import com.railroad.exceptions.ThisPlaceIsAlreadyBusyException;
import com.railroad.exceptions.ToLateException;
import com.railroad.repo.*;
import com.railroad.response.BusinessResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * service implements operations with tickets (creating, searching and other)
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class TicketServiceImpl implements TicketService {

    private final TicketRepo ticketRepo;
    private final UserRepo userRepo;
    private final TrainRepo trainRepo;
    private final StationRepo stationRepo;
    private final RouteNodeRepo routeNodeRepo;
    private final PriceRepo priceRepo;


    /**
     * Create temporary ticket with negative price and station names. If user dont input payment info and
     * buy this ticket in 10 minutes, or choose another ticket,
     * reservation will be finished and this ticket can be delete
     * @param ticketInfo - information about user, place, train and carriage
     * @throws ThisPlaceIsAlreadyBusyException if two users will book same place in one time
     */
    @Transactional
    @Override
    public long createTemporaryTicket(TicketInfo ticketInfo) {
        log.info("CREATING TEMPORARY TICKET, TICKETINFO: " + ticketInfo);
        Ticket ticket = new Ticket();
        ticket.setUser(userRepo.findUserById(ticketInfo.getUserId()));
        ticket.setCarriage(trainRepo.findCarriageById(ticketInfo.getCarriageId()));
        ticket.setPlaceNumber(ticketInfo.getPlaceNumber());
        ticket.setPrice(-1.00);
        ticket.setCreationTime(LocalDateTime.now());
        try {
            return ticketRepo.addTemporaryTicket(ticket);
        } catch (DataIntegrityViolationException | ConstraintViolationException e) {
            log.info("CANT CREATE TEMPORARY TICKET: " + e.getMessage(), e);
            throw new ThisPlaceIsAlreadyBusyException("IT LOOKS LIKE THIS TICKET WAS ALREADY BOUGHT \n" +
                    " TRY TO SELECT ANOTHER ONE");
        }
    }

    /**
     * delete all tickets to given carriage, which was reserved more than 10 minutes ago and not bought
     * @param carriageId id of given carriage
     */
    @Transactional
    @Override
    public void deleteTrashTickets(long carriageId) {
        ticketRepo.deleteTrashTickets(carriageId);
    }

    /**
     * If reserved ticket was successfully bought by user, method changes is price from negative to actual.
     * Such tickets cant be deleted by "deleteTrashTickets" and "deleteTemporaryTicketsForCurrentUser"
     * methods.
     * @param ticketInfo information about upgrading ticket
     * @return
     * @throws ToLateException if ticket was booked more than 10 minutes ago,
     * reservations was finished and ticket was bought  by another user
     */
    @Transactional
    @Override
    public BusinessResponse upgradeTemporaryTicket(TicketInfo ticketInfo) {
        log.info("UPGRADING TEMPORARY TICKET, TICKET INFO: " + ticketInfo.getTicketId());
        Ticket ticket = new Ticket();
        ticket.setStartStation(stationRepo.findStationById(ticketInfo.getStartStationId()));
        ticket.setFinishStation(stationRepo.findStationById(ticketInfo.getEndStationId()));
        ticket.setPrice(ticketInfo.getTicketPrice());
        ticket.setId(ticketInfo.getTicketId());
        List<Ticket> ticketForUpgrade = ticketRepo.findTicketByIdForUpgrade(ticketInfo.getTicketId());
        if (ticketForUpgrade.isEmpty()) {
            log.info("TICKET WAS ALREADY BOUT BY ANOTHER USER, RETURN");
            throw new ToLateException("THIS TICKET WAS ALREADY BOUGHT");
        } else {
            return ticketRepo.upgradeTemporaryTicket(ticket);
        }
    }

    /**
     * Delete all temporary (tickets with negative price) ticket for user with given ID
     */
    @Transactional
    @Override
    public void deleteTemporaryTicketsForCurrentUser(long userId) {
        log.info("DELETING TEMPORARY TICKETS FOR USER: " + userId);
        ticketRepo.deleteTemporaryTicketsForCurrentUser(userId);
    }

    /**
     * Calculate price for ticket from one to another station in given train in carriage of given type
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public double getPrice(long trainId, CarriageType carriageType, long startStationId, long endStationId) {
        log.info("SEARCHING FOR PRICE, TRAIN ID: " + trainId + " CARRIAGE TYPE: " + carriageType +
                " START STATION: " + startStationId + " END STATION: " + endStationId);
        List<Long> sortedStationIds = routeNodeRepo.findSortedStationIdsByTrainId(trainId);
        double price = 0;
        for (int i = sortedStationIds.indexOf(startStationId);
             i < sortedStationIds.indexOf(endStationId); i++) {
            price += priceRepo.findPriceByTrainIdCarriageTypeStartStationIdEndStationId(trainId,
                    carriageType, sortedStationIds.get(i), sortedStationIds.get(i + 1));
        }
        log.info("PRICE: " + price);
        return price;
    }

    /**
     * return short information about all tickets bought by user with given id
     */
    @Transactional
    @Override
    public List<TicketPreviewDto> getTicketsPreview(long userId) {
        return ticketRepo.getTicketsPreview(userId);
    }

    /**
     * return detailed information about given ticket
     * validate that requested ticket belong to logged in user. If this validation fails-return null
     */
    @Transactional(readOnly = true)
    @Override
    public TicketDetailsDto getTicketDetails(long ticketId, long userId) {
        log.info("GETTING TICKETS DETAILS: TICKET ID: " + ticketId + " USER ID: " + userId);
        long ticketOwnerId = ticketRepo.findTicketById(ticketId).getUser().getId();
        return ticketOwnerId == userId ? ticketRepo.getTicketDetails(ticketId) : null;
    }
}
