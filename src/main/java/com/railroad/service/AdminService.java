package com.railroad.service;

import com.railroad.dto.admin.AdminTimeTableItemDto;
import com.railroad.dto.admin.TrainCreationDto;
import com.railroad.dto.admin.UserForAdminRequestDto;
import com.railroad.response.BusinessResponse;
import java.util.List;

public interface AdminService {
    BusinessResponse addStation(String stationName);
    BusinessResponse addTrain(TrainCreationDto trainCreationDto);
    List<AdminTimeTableItemDto> getTimeTable(String stationName, String date);
    List<UserForAdminRequestDto> getPassengers(long trainId);
}
