package com.railroad.service.pdf;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.railroad.dto.user.TicketDetailsDto;
import com.railroad.exceptions.DocumentCreationException;
import com.railroad.service.pdf.qr.QRCodeGenerator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import java.io.ByteArrayOutputStream;

/**
 * PDF file generator
 */
@Component
@Slf4j
public class PdfGeneratorImpl implements PdfGenerator {
    private final QRCodeGenerator qrCodeGenerator = new QRCodeGenerator();

    /**
     * Generates pdf with ticket information and returns it as byteArrayOutputStream
     * @param ticketDetailsDto ticket information for sending
     */
    @Override
    public ByteArrayOutputStream createPdfTicket(TicketDetailsDto ticketDetailsDto) {
        log.info("CREATING PDF TICKET FO ID: " + ticketDetailsDto.getTicketId());
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            Document document = new Document();
            document.setPageSize(PageSize.A5);
            PdfWriter pdfWriter = PdfWriter.getInstance(document, outputStream);
            document.open();
            Paragraph paragraph1 = new Paragraph();
            paragraph1.add("RailRoad Ticket");
            paragraph1.setAlignment(Element.ALIGN_CENTER);
            Paragraph paragraph2 = new Paragraph();
            paragraph2.add("Route: " + ticketDetailsDto.getStartStation() + " - " + ticketDetailsDto.getEndStation());
            paragraph2.setAlignment(Element.ALIGN_CENTER);
            Paragraph paragraph3 = new Paragraph();
            paragraph3.add("Departing from " + ticketDetailsDto.getStartStation() +
                    ": " + ticketDetailsDto.getDepartingTime() + " Arriving to " + ticketDetailsDto.getEndStation() +
                    ": " + ticketDetailsDto.getArrivingTime());
            paragraph3.setAlignment(Element.ALIGN_CENTER);
            Paragraph paragraph4 = new Paragraph();
            paragraph4.add("Passenger: " + ticketDetailsDto.getUserFirstName() +
                    " " + ticketDetailsDto.getUserLastName());
            paragraph4.setAlignment(Element.ALIGN_CENTER);
            Paragraph paragraph5 = new Paragraph();
            paragraph4.add("Train/carriage/place: " + ticketDetailsDto.getTrainNumber() +
                    "/" + ticketDetailsDto.getCarriageNumber() + "/" + ticketDetailsDto.getPlaceNumber());
            paragraph4.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph1);
            document.add(paragraph2);
            document.add(paragraph3);
            document.add(paragraph4);
            document.add(paragraph5);
            Jpeg jpeg = new Jpeg(qrCodeGenerator.generateQRCodeImage(ticketDetailsDto.getTicketId().toString()));
            jpeg.setAlignment(Element.ALIGN_CENTER);
            document.add(jpeg);
            document.close();
        } catch (Exception e) {
            log.error("ERROR DURING PDF TICKET CREATION " + e.getMessage(), e);
            throw new DocumentCreationException("CANT CREATE PDF FILE. WE ARE WORKING AT THIS PROBLEM");
        }
        log.info("PDF TICKET CREATED");
        return outputStream;
    }
}
