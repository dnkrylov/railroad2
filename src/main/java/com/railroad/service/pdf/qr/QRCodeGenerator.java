package com.railroad.service.pdf.qr;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.qrcode.QRCodeWriter;
import com.railroad.exceptions.DocumentCreationException;
import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * QR generator
 */
@Slf4j
public class QRCodeGenerator {

    /**
     * Generate qr image with given text
     * @param text - text to code to qr image
     */
    public byte[] generateQRCodeImage(String text) throws WriterException {
        log.info("GENERATING QR CODE FOR TEXT: " + text);
        return toByteArray(generateQRCodeImage(text, 256, 256));
    }

    /**
     * Generates qr code BufferedImage format
     */
    private BufferedImage generateQRCodeImage(String text, int width, int height) throws WriterException {
        return MatrixToImageWriter.toBufferedImage(
                new QRCodeWriter().encode(text, BarcodeFormat.QR_CODE, width, height)
        );
    }

    /**
     * Converts qr code to byte array
     */
    private byte[] toByteArray(BufferedImage image) {
        try(ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            ImageIO.write(image,  "jpg", baos);
            baos.flush();
            return baos.toByteArray();
        } catch (IOException ex) {
            log.error("Exception during image serialization", ex);
            throw new DocumentCreationException("CANT CREATE PDF FILE. WE ARE WORKING AT THIS PROBLEM");
        }
    }
}