package com.railroad.service.pdf;

import com.railroad.dto.user.TicketDetailsDto;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public interface PdfGenerator {
    ByteArrayOutputStream createPdfTicket(TicketDetailsDto ticketDetailsDto);
}
