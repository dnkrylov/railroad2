package com.railroad.service;

import com.railroad.dto.UserDetailsDto;

public interface SecurityService {
    UserDetailsDto getLoggedInUser();
}
