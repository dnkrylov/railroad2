package com.railroad.service;


import com.railroad.dto.user.*;
import com.railroad.response.BusinessResponse;

import java.util.List;

public interface UserService {
    List<String> getStationNamesList();
    List<TimeTableItemDto> getTimeTableItems(String stationName, String date);
    List<AvailableDirectRouteDto> findDirectRoutes(String startStationName, String endStationName,
                                                   String routeDate, boolean isItArrivingDate);
    List<CarriageDto> getCarriages(TicketInfo ticketInfo);
    BusinessResponse createTemporaryTicket(TicketInfo ticketInfo);
    BusinessResponse upgradeTemporaryDirectTicket(TicketInfo ticketInfo);
    List<AvailableTransferRouteDto> findTransferRoutes(String startStationName, String endStationName,
                                                       String routeDate, boolean isItArrivingDate);
    TransferCarriages getTransferCarriages(TransferTicketInfo transferTicketInfo);
    BusinessResponse createTransferTemporaryTickets(TransferTicketInfo transferTicketInfo);
    BusinessResponse upgradeTemporaryTransferTicket(TransferTicketInfo transferTicketInfo);
    UserDto getUserDto();
    BusinessResponse updateUser(UserDto userDto);
    List<TicketPreviewDto> getTicketPreview();
    TicketDetailsDto getTicketDetails(long ticketId);

    BusinessResponse sendTicketByEmail(long ticketId);
}
