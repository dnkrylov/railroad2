<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <title>Russian Railroad</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<c:url value="/resources/bootstrap/css/bootstrap.min.css"/>">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="<c:url value="/resources/css/container.css"/>">
    <!-- Bootstrap and Jquery core JavaScript -->
    <script src="<c:url value="/resources/jquery/jquery.min.js"/>"></script>
    <script src="<c:url value="/resources/bootstrap/js/bootstrap.bundle.min.js"/>"></script>
    <%--    DateTimePicker--%>
    <script src="<c:url value="/resources/datetimepicker/jquery.datetimepicker.full.min.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/resources/datetimepicker/jquery.datetimepicker.min.css"/>">
    <%--Autocomplete--%>
    <link rel="stylesheet" href="<c:url value="/resources/easyautocomplete/easy-autocomplete.min.css"/>">
    <script src="<c:url value="/resources/easyautocomplete/jquery.easy-autocomplete.min.js"/>"></script>
    <%-- Page scripts--%>
    <script src="<c:url value="/resources/script/container.js"/> "></script>
    <%--Variables--%>
    <script type="text/javascript">let path = "${pageContext.request.contextPath}"</script>
    <%-- Global scripts--%>
    <script src="<c:url value="/resources/script/global/validation.js"/> "></script>
    <script src="<c:url value="/resources/script/global/clearForms.js"/> "></script>
    <script src="<c:url value="/resources/script/global/popUp.js"/> "></script>
    <script src="<c:url value="/resources/script/global/dataReceive.js"/> "></script>
</head>
<body>
<jsp:include page="/resources/modal/modalWindow.jsp"/>
<jsp:include page="/resources/modal/modalLogin.jsp"/>
<jsp:include page="/resources/modal/modalRegister.jsp"/>
<jsp:include page="/resources/modal/modalPayment.jsp"/>
<jsp:include page="/resources/modal/modalTicketDetails.jsp"/>


<div id="navbarSupportedContent">

</div>
<div class="row" style="margin-top: 55px">

    <div class="col-sm-2 text-center" id="sidebar-wrapper">

    </div>
    <div class="col" id="content">


    </div>

<c:if test="${not empty message}">
<input type="hidden" id="initialMessage" value="${message}"/>
</c:if>


</div>

</body>
</html>
