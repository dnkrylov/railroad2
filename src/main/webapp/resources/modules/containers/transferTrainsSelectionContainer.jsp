<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="<c:url value="/resources/script/containersscripts/transferTrainContainer.js"/> "></script>

<div class="container" id="transferTrainSelectionContainer">
    <div class="container text-center" id="transferTrainTable"></div>
</div>
