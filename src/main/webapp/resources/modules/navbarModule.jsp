<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="<c:url value="/resources/script/modules/navbar.js"/> "></script>
<nav class="navbar navbar-expand-lg navbar-light bg-light shadow fixed-top" id="navbar">
    <div class="container">
        <a class="navbar-brand" href="#">RUSSIAN RAILROAD</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <security:authorize access="hasAnyRole('ROLE_USER', 'ROLE_ADMIN')">
                    <li class="nav-item">
                        <a class="nav-link" href="#" id="personalAreaButton">${userFirstName} ${userLastName}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" id="logoutButton">LOGOUT</a>
                    </li>
                </security:authorize>
                <security:authorize access="anonymous">
                    <li class="nav-item">
                        <a class="nav-link" href="#" id="loginButton">LOGIN</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" id="registerButton">REGISTER</a>
                    </li>
                </security:authorize>
            </ul>
        </div>
    </div>
</nav>


<%--<ul class="navbar-nav ml-auto mt-2 mt-lg-0" id="navbar">--%>
<%--    <security:authorize access="hasAnyRole('ROLE_USER', 'ROLE_ADMIN')">--%>
<%--        <li class="nav-item">--%>
<%--            <a class="nav-link" href="#" id="personalAreaButton">${userFirstName} ${userLastName}</a>--%>
<%--        </li>--%>
<%--        <li class="nav-item active">--%>
<%--            <a class="nav-link" href="#" id="logoutButton">LOGOUT</a>--%>
<%--        </li>--%>
<%--    </security:authorize>--%>
<%--    <security:authorize access="anonymous">--%>
<%--        <li class="nav-item active">--%>
<%--            <a class="nav-link" href="#" id="loginButton">LOGIN</a>--%>
<%--        </li>--%>
<%--        <li class="nav-item">--%>
<%--            <a class="nav-link"> or </a>--%>
<%--        </li>--%>
<%--        <li class="nav-item active">--%>
<%--            <a class="nav-link" href="#" id="registerButton">REGISTER</a>--%>
<%--        </li>--%>
<%--    </security:authorize>--%>
<%--</ul>--%>