<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="<c:url value="/resources/script/modules/cabinet.js"/> "></script>

<div class="row">
    <div class="col text-center userCabinetCol">
        <span class="label-primary">PERSONAL INFO</span>
        <div class="form-group text-center">
            <label for="userFirstNameCabinet">Name:</label>
            <input type="text" disabled class="form-control text-center userInfoItem" id="userFirstNameCabinet">
            <div class="alert-danger text-center alertError alertErrorUserFirstName">

            </div>
        </div>
        <div class="form-group text-center">
            <label for="userLastNameCabinet">Surname:</label>
            <input type="text" disabled class="form-control text-center  userInfoItem" id="userLastNameCabinet">
            <div class="alert-danger text-center alertError alertErrorUserLastName">

            </div>
        </div>

        <div class="form-group text-center">
            <label for="userDateOfBirthCabinet">Date of birth:</label>
            <input type="text" disabled class="form-control text-center  userInfoItem" id="userDateOfBirthCabinet">
            <div class="alert-danger text-center alertError alertErrorUserDateOfBirth">
            </div>
        </div>
        <div class="form-group text-center">
            <label for="userPhoneCabinet">Phone number:</label>
            <input type="text" disabled class="form-control text-center  userInfoItem" id="userPhoneCabinet">
            <div class="alert-danger text-center alertError alertErrorUserPhone">
            </div>
        </div>
        <div class="form-group text-center">
            <label for="userEmailCabinet">Email:</label>
            <input type="text" disabled class="form-control text-center userInfoItem" id="userEmailCabinet">
            <div class="alert-danger text-center alertError alertErrorUserEmail">
            </div>
        </div>

        <div class="form-group text-center">
            <label for="userLoginCabinet">Login:</label>
            <input type="text" disabled class="form-control text-center  userInfoItem" id="userLoginCabinet">
            <div class="alert-danger text-center alertError alertErrorUserLogin">

            </div>

        </div>
        <div class="form-group text-center">
            <label for="userPasswordCabinet">Password:</label>
            <input type="password" disabled class="form-control text-center  userInfoItem" id="userPasswordCabinet">
            <div class="alert-danger text-center alertError alertErrorUserPassword">

            </div>
        </div>
        <button type="button" class="butt cabinetInfoButton" id="changeUserInfo">CHANGE</button>
        <button type="button" class="butt cabinetInfoButton" style="display: none" id="saveUserInfoChanges">SAVE</button>
    </div>

    <div class="col userCabinetCol">
    </div>
    <div class="col text-center userCabinetCol" id="ticketCol">

    </div>
    <div class="col" style="max-width: 10px"></div>
</div>