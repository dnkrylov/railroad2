<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="<c:url value="/resources/script/modules/showPassenger.js"/> "></script>
<div class="row">
    <div class="col">
        <div class="row">
            <div class="col text-center d-flex justify-content-center">
                <div class="form-group">
                    <label for="dateForAdminTimeTable">INPUT DATE: </label>
                    <input type="text" class="form-control text-center showPassengerModuleInput" autocomplete="off"
                           id="dateForAdminTimeTable"
                           placeholder="dd.mm.yyyy format"/>
                </div>
            </div>
            <div class="col text-center d-flex justify-content-center">
                <div class="form-group">
                    <label for="stationForAdminTimeTable">SELECT STATION: </label>
                    <input class="form-control stationSelectAuto text-center showPassengerModuleInput"
                           autocomplete="off"
                           id="stationForAdminTimeTable"
                           placeholder="start typing"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col text-center">
                <button type="button" class="butt showPassengerModuleButt" id="getTrainsForAdmin">FIND
                    TRAINS
                </button>
            </div>
        </div>
    </div>
    <div class="col text-center" id="adminTrainSelectionContainer">
        <div class="row">
            <div class="col text-center d-flex justify-content-center" >
                <div class="form-group text-center" id="adminTrainSelectionRow">

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col text-center d-flex justify-content-center" id="adminTrainSelectionButtonRow">

            </div>
        </div>
        <%--        <div class="container" id="adminTrainSelectionContainer">--%>

        <%--        </div>--%>
    </div>
</div>
<div class="row">
    <div class="col" id="passengerTableContainer"></div>
</div>
