<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="<c:url value="/resources/script/modules/timetable.js"/> "></script>
<div class="row">
    <div class="col text-center">
        <div class="form-group timeTableFormsInput">
            <label for="selectStationForTimeTable">SELECT STATION: </label>
            <input class="form-control stationSelectAuto text-center" autocomplete="off"
                   id="selectStationForTimeTable"
                   placeholder="start typing"/>
        </div>
    </div>

    <div class="col text-center">
        <div class="form-group timeTableFormsInput">
            <label for="timeTableDate">INPUT DATE</label>
            <input type="text" class="form-control text-center" id="timeTableDate"
                   placeholder="dd.mm.yyyy format"/>
        </div>
    </div>
    <div class="col text-center my-auto">
        <button type="button" style="width: 60%" class="butt" id="showTimeTableButton">
            SHOW TIMETABLE
        </button>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="container" id="timeTableContainer">

        </div>
    </div>
</div>


