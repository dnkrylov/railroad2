<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="<c:url value="/resources/script/modules/addTrain.js"/> "></script>

<div class="row">
    <div class="col"></div>
    <div class="col text-center">
        <div class="form-group">
            <label for="trainNumber">INPUT TRAIN NUMBER</label>
            <input type="text" class="form-control text-center" id="trainNumber"
                   placeholder="enter train name"/>
        </div>
        <button type="button" class="butt" id="submitTrain">
            SUBMIT TRAIN
        </button>
    </div>
    <div class="col"></div>
</div>
<div class="row">
    <div class="col text-center">
        <div class="container" id="stationTableContainer">

        </div>
        <button type="button" class="butt creatingTrainButton" id="addStation">ADD STATION</button>
        <button type="button" class="butt creatingTrainButton" id="removeStation">REMOVE STATION</button>
    </div>
</div>
<div class="row">
    <div class="col text-center">
        <div class="container" id="carTableContainer">

        </div>
        <button type="button" class="butt creatingTrainButton" id="addCarriage">ADD CARRIAGE</button>
        <button type="button" class="butt creatingTrainButton" id="removeCarriage">REMOVE CARRIAGE</button>
    </div>
</div>

