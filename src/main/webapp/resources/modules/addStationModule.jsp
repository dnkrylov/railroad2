<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="<c:url value="/resources/script/modules/addStation.js"/> "></script>

<div class="row">
    <div class="col"></div>
    <div class="col text-center">
        <div class="form-group">
            <label for="newStationName">INPUT NAME FOR NEW STATION: </label>
            <input type="text" class="form-control text-center" id="newStationName"
                   placeholder="station name"/>
        </div>
    </div>
    <div class="col"></div>
</div>
<div class="row">
    <div class="col"></div>
    <div class="col d-flex justify-content-center">
        <button type="button" class="butt" style="width: 60%" id="submitStation">
            CREATE STATION
        </button>
    </div>
    <div class="col"></div>
</div>
