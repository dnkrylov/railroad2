<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="<c:url value="/resources/script/modules/findTrains.js"/> "></script>

<form class="userForm" id="routeTableForm">
    <div class="row">
        <div class="col text-center d-flex justify-content-center">
            <div class="form-group routeCreationFormsInput text-center ">
                <label for="selectStartStation">SELECT DEPARTING STATION: </label>
                <input class="form-control stationSelectAuto text-center routeCreationStartInformation"
                       autocomplete="off" id="selectStartStation"
                       placeholder="start typing"/>
            </div>

        </div>
        <div class="col text-center d-flex justify-content-center">
            <div class="form-group routeCreationFormsInput">
                <label for="routeDate">SELECT DATE:</label>
                <input type="text" class="form-control text-center routeCreationStartInformation" id="routeDate"
                       placeholder="dd.mm.yyyy format"/>
            </div>
        </div>
        <div class="col d-flex justify-content-center my-auto">
            <button type="button" class="butt" style="width: 60%" id="showRoutes">FIND DIRECT ROUTE</button>
        </div>
    </div>
    <div class="row">
        <div class="col text-center d-flex justify-content-center">
            <div class="form-group routeCreationFormsInput text-center">
                <label for="selectEndStation">SELECT ARRIVING STATION: </label>
                <input class="form-control stationSelectAuto text-center routeCreationStartInformation"
                       autocomplete="off" id="selectEndStation"
                       placeholder="start typing"/>
            </div>
        </div>
        <div class="col d-flex justify-content-center my-auto">
            <div class="form-check routeCreationFormsInput">
                <p>
                    <input type="checkbox" id="isItArrivingDate" />
                    <label for="isItArrivingDate">ARRIVING DATE</label>
                </p>

            </div>
        </div>
        <div class="col d-flex justify-content-center my-auto">
            <button type="button" class="butt" style="width: 60%" id="showTransferRoutes">FIND TRANSFER ROUTE
            </button>
        </div>
    </div>
</form>
<div class="container" id="trainsForRouteContainer">

</div>
