<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="<c:url value="/resources/script/modules/sidebar.js"/> "></script>

<div class="sidebar list-group list-group-flush">
    <a href="#" class="list-group-item list-group-item-action bg-light contentChangeButton" id="timeTableModuleButton">TIMETABLE</a>
    <a href="#" class="list-group-item list-group-item-action bg-light contentChangeButton" id="findTicketsModuleButton">FIND TICKETS</a>
<%--    <a href="#" class="list-group-item list-group-item-action bg-light contentChangeButton">NEWS</a>--%>
<%--    <a href="#" class="list-group-item list-group-item-action bg-light contentChangeButton">ABOUT COMPANY</a>--%>
    <security:authorize access="hasRole('ROLE_ADMIN')">
        <a href="#" class="list-group-item list-group-item-action bg-light contentChangeButton" id="addStationModuleButton">ADD STATION</a>
        <a href="#" class="list-group-item list-group-item-action bg-light contentChangeButton" id="addTrainModuleButton">ADD TRAIN</a>
        <a href="#" class="list-group-item list-group-item-action bg-light contentChangeButton" id="showPassengerButton">SHOW PASSENGERS</a>
    </security:authorize>

</div>
