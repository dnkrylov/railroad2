let userDto = {};

$(document).ready(function () {
    clearRegisterValues();

    $('#registerForm').submit(function (e) {
        e.preventDefault();
        register();
    });
    $('#userDateOfBirth').datetimepicker({
        timepicker: false,
        format: 'd.m.Y'
    });
});

function register() {
    $('.alertError').empty();
    userDto["firstName"] = $("#userFirstName").val();
    userDto["lastName"] = $("#userLastName").val();
    userDto["dateOfBirth"] = $("#userDateOfBirth").val();
    userDto["phone"] = $("#userPhone").val();
    userDto["email"] = $("#userEmail").val();
    userDto["login"] = $("#userLogin").val();
    userDto["password"] = $("#userPassword").val();
    if (!validateRegisterParameters(userDto)) {
        return;
    }
    let promise = new Promise(function (resolve, reject) {
        let response = grecaptcha.getResponse();
        if (!response) {
            reject($('.alertErrorUserCaptcha').html("INPUT CAPTCHA"));
        } else {
            resolve(sendValues(response));
        }
    });
    promise.finally(()=> {
        grecaptcha.reset()
    });
}


function sendValues(response) {
    let registrationUserDto = {};
    registrationUserDto["userDto"] = userDto;
    registrationUserDto["captchaResponse"] = response;
    $.ajax({
        type: 'POST',
        url: path + '/unregistered/tryToRegister',
        contentType: "application/json",
        data: JSON.stringify(registrationUserDto),
        dataType: "json",
        success: function (data) {
            $('.alertError').empty();
            clearRegisterValues();
            $('#registerModal').modal('hide');
            showModalInfo(data.message, 'YOU HAVE SUCCESSFULLY REGISTERED! BEFORE LOGIN PLEASE CHECK YOUR EMAIL TO VERIFY IT ');
        },
        error: function (response) {
            if (response.status === 400) {
                $('.alertErrorUserPassword').append("INVALID REGISTRATION PARAMETERS");
            }
            if (response.status === 409) {
                if (response.responseText.includes('LOGIN')) {
                    $('.alertErrorUserLogin').append(response.responseText.toUpperCase());
                } else if (response.responseText.includes('PHONE')) {
                    $('.alertErrorUserPhone').append(response.responseText.toUpperCase());
                } else if (response.responseText.includes('EMAIL')) {
                    $('.alertErrorUserEmail').append(response.responseText.toUpperCase());
                } else if (response.responseText.includes('CAPTCHA')) {
                    $('.alertErrorUserCaptcha').append(response.responseText.toUpperCase());
                } else {
                    clearRegisterValues();
                    showError(response.responseText.toUpperCase())
                }
            }
            if(response.status === 500) {
                $('.alertError').empty();
                clearRegisterValues();
                $('#registerModal').modal('hide');
                showError(response.responseText.toUpperCase())
            }
        }
    });
}

