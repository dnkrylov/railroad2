$(document).ready(function () {
    $('#buyTicket').click(function () {
        $('.alertError').empty();
        var cardNumber = $('#cardNumber').val();
        var cardExp = $('#cardExp').val();
        var cardCVV = $('#cardCVV').val();
        if (!validateCardNumber(cardNumber) || !validateCardExp(cardExp) || !validateCVV(cardCVV)) {
            return;
        }
        var pathToMethod;
        if (directTicket) {
            pathToMethod = path + "/registered/tryToBuyDirectTicket/";
        } else {
            pathToMethod = path + "/registered/tryToBuyTransferTicket/";
        }
        $.ajax({
            type: "POST",
            url: pathToMethod,
            data: {
                cardCVV: cardCVV,
                cardNumber: cardNumber,
                cardExp: cardExp
            },
            success: function (data) {
                $('#paymentModal').modal('hide');
                clearPaymentValues();
                $('#trainsForRouteContainer').empty();
                showModalInfo('SUCCESS', data.message.toUpperCase());
            },
            error: function (response) {
                $('#paymentModal').modal('hide');
                clearPaymentValues();
                $('#trainsForRouteContainer').empty();
                showModalInfo('TICKET NOT BOUGHT', response.responseText.toUpperCase());
            }
        });
    });
})