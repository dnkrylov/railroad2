$(document).ready(function () {
    $('#sendTicketByEmail').click(function () {
        $.ajax({
            type: 'POST',
            url: path + '/registered/sendTicketByEmail',
            data: {
                ticketId: $('#printingTicket').val()
            },
            accept: 'json',
            success: function (response) {
                $('#ticketDetailsModal').modal('hide');
                showModalInfo("SUCCESS", response.message)
            },
            error: function (response) {
                $('#ticketDetailsModal').modal('hide');
                showError(response.responseText.toUpperCase());
            }
        })
    })
});