$(document).ready(function () {
    $('#submitLoginButton').click(function () {
        login();
    });
});

function login() {
    $('.alertError').empty();
    let login = $('#login').val();
    if (!validateLoginInfo(login)) {
        return;
    }
    var password = $('#pwd').val();
    $.ajax({
        type: 'POST',
        url: path + '/unregistered/tryToLogin',
        data: {
            username: login,
            password: password
        },
        accept: 'json',
        success: function () {
            $('.alertError').empty();
            $('#loginModal').modal('hide');
            clearLoginValues();
            $('#navbarSupportedContent').empty();
            $('#navbarSupportedContent').load(path + "/resources/modules/navbarModule.jsp");
            $('#sidebar-wrapper').empty();
            $('#sidebar-wrapper').load(path + "/resources/modules/sidebarModule.jsp");
        },
        error: function (response) {
            $('#alertErrorPassword').append(response.responseText.toUpperCase());
        }
    })
}
