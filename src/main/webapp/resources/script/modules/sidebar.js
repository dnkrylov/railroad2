$(document).ready(function () {
    $('#timeTableModuleButton').click(function () {
        $('#content').empty();
        $('#content').load(path + "/resources/modules/timetableModule.jsp");
    });
    $('#addStationModuleButton').click(function () {
        $('#content').empty();
        $('#content').load(path + "/resources/modules/addStationModule.jsp");
    });
    $('#addTrainModuleButton').click(function () {
        $('#content').empty();
        $('#content').load(path + "/resources/modules/addTrainModule.jsp");
    });
    $('#findTicketsModuleButton').click(function () {
        $('#content').empty();
        $('#content').load(path + "/resources/modules/findTrainsModule.jsp");
    });
    $('#showPassengerButton').click(function () {
        $('#content').empty();
        $('#content').load(path + "/resources/modules/showPassengersModule.jsp");
    });

    $('.contentChangeButton').click(function () {
        clearGlobalVariables();
    })
});