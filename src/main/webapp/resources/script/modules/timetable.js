$(document).ready(function () {
    $('.stationSelectAuto').easyAutocomplete(stations);
    $('#timeTableDate').datetimepicker({
        timepicker: false,
        format: 'd.m.Y'
    });
});

$(document).ready(function () {
    $('#showTimeTableButton').click(function () {
        let station = $('#selectStationForTimeTable').val().toUpperCase();
        date = $('#timeTableDate').val();
        if (!validateTimeTableInfo(date, station)) {
            return;
        }
        $.ajax({
            type: 'GET',
            url: path + '/unregistered/showTimeTable',
            data: {
                stationName: station,
                date: date
            },
            success: function (data) {
                showTimeTable(data);
            },
            error: function (response) {
                showError(response.responseText.toUpperCase());
            }
        })
    })
});

function showTimeTable(data) {
    $('#timeTableContainer').empty();
    if (data.length === 0) {
        showModalInfo("NOT FOUND", "NO TRAINS FOUND DEPARTING OR ARRIVING FROM THIS STATION THIS DATE");
        return;
    }
    let table = document.createElement('table');
    table.className = "table table-hover text-center";
    table.id = "timeTable";
    $('#timeTableContainer').append(table);

    let header = document.createElement('thead');
    header.className = "text-center";
    header.id = "timeTable";
    header.innerHTML = '<thead class="text-center">' +
        '<th>TRAIN NUMBER</th><th>ARRIVAL TIME</th><th>DEPARTURE TIME</th>' +
        '</thead>';
    table.append(header);

    let body = document.createElement('tbody');
    body.id = "timeTableBody";
    table.append(body);

    for (let i = 0; i < data.length; i++) {
        let tr = document.createElement('tr');
        tr.innerHTML = `<td>${data[i].trainNumber}</td><td>${data[i].arrivalTime}</td><td>${data[i].departureTime}</td>`;
        body.append(tr);
    }
}