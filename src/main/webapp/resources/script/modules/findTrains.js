$(document).ready(function () {
    $('.stationSelectAuto').easyAutocomplete(stations);
    $('#routeDate').datetimepicker({
        timepicker: false,
        format: 'd.m.Y'
    });

    $(".routeCreationStartInformation").change(function () {
        $('#trainsForRouteContainer').empty();
    });

    $('#showRoutes').click(function () {
        $('#trainsForRouteContainer').empty();
        getRouteParameters();
        if(!validateRouteInfo(startStation, endStation, routeDate, isItArrivingDate)) {
            return;
        }
        $.ajax({
            type: "GET",
            url: path + "/unregistered/showDirectRoutes",
            data: {
                startStationName: startStation,
                endStationName: endStation,
                routeDate: routeDate,
                isItArrivingDate: isItArrivingDate
            },
            success: function (data) {
                showAvailableDirectTrains(data);
            },
            error: function (response) {
                showError(response.responseText.toUpperCase());
            }
        });
    });

    $('#showTransferRoutes').click(function () {
        $('#trainsForRouteContainer').empty();
        getRouteParameters();
        if(!validateRouteInfo(startStation, endStation, routeDate, isItArrivingDate)) {
            return;
        }
        $.ajax({
            type: "GET",
            url: path + "/unregistered/showTransferRoutes",
            data: {
                startStationName: startStation,
                endStationName: endStation,
                routeDate: routeDate,
                isItArrivingDate: isItArrivingDate
            },
            success: function (data) {
                showAvailableTransferTrains(data);
            },
            error: function (response) {
                showError(response.responseText.toUpperCase());
            }
        });
    });
});

function getRouteParameters() {
    startStation = $("#selectStartStation").val().toUpperCase();
    endStation = $("#selectEndStation").val().toUpperCase();
    routeDate = $("#routeDate").val();
    isItArrivingDate = false;
    if ($('#isItArrivingDate').is(":checked")) {
        isItArrivingDate = true;
    }
}

function showAvailableDirectTrains(data) {
    $('#trainsForRouteContainer').empty();
    directTrains = data;
    if (directTrains.length === 0) {
        showModalInfo("NOT FOUND", "NO AVAILABLE DIRECT TRAINS FOR THIS ROUTE AND DATE. TRY TO CHANGE PARAMETERS OR SEARCH FOR ROUTES WITH TRANSFER")
    } else {
        $('#trainsForRouteContainer').load(path + "/resources/modules/containers/directTrainsSelectionContainer.jsp");
    }
}

function showAvailableTransferTrains(data) {
    $('#trainsForRouteContainer').empty();
    transferTrains = data;
    if (transferTrains.length === 0) {
        let str = `NO AVAILABLE TRAINS FOR THIS ROUTE AND DATE.
         TRY TO SELECT ANOTHER DATE`;
        showModalInfo("NOT FOUND", str);
    } else {
        $('#trainsForRouteContainer').load(path + "/resources/modules/containers/transferTrainsSelectionContainer.jsp");
    }
}