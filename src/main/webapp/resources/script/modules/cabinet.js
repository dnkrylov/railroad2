$(document).ready(function () {
    $('#userDateOfBirthCabinet').datetimepicker({
        timepicker: false,
        format: 'd.m.Y'
    });

    getAndFillUserInfo();

    getAndFillTicketsPreview();

    $('#changeUserInfo').click(function () {
        $('.userInfoItem').prop('disabled', false);
        $('#userPasswordCabinet').val('');
        $('#saveUserInfoChanges').show();
    });
    $('#saveUserInfoChanges').click(function () {
        updateUserInfo();
    });

    $('#changeUserInfo').change(function () {
        userInfoChanged = true;
    })
});

function getAndFillUserInfo() {
    $.get('registered/getUserInfo', function (data) {
        $('#userFirstNameCabinet').val(data.firstName);
        $('#userLastNameCabinet').val(data.lastName);
        $('#userDateOfBirthCabinet').val(data.dateOfBirth);
        $('#userPhoneCabinet').val(data.phone);
        $('#userEmailCabinet').val(data.email);
        $('#userLoginCabinet').val(data.login);
        $('#userPasswordCabinet').val('*********');
    });
}

function updateUserInfo() {
    $('.alertError').empty();
    let userDto = {};
    userDto["firstName"] = $("#userFirstNameCabinet").val();
    userDto["lastName"] = $("#userLastNameCabinet").val();
    userDto["dateOfBirth"] = $("#userDateOfBirthCabinet").val();
    userDto["phone"] = $("#userPhoneCabinet").val();
    userDto["email"] = $("#userEmailCabinet").val();
    userDto["login"] = $("#userLoginCabinet").val();
    userDto["password"] = $("#userPasswordCabinet").val();
    if (!validateRegisterParameters(userDto)) {
        return;
    }
    $.ajax({
        type: 'POST',
        url: path + '/registered/updateUserInfo',
        contentType: "application/json",
        data: JSON.stringify(userDto),
        dataType: "json",
        success: function (data) {
            $('.alertError').empty();
            $('.userInfoItem').prop('disabled', true);
            $('#saveUserInfoChanges').hide();
            showModalInfo("SUCCESS", data.message.toUpperCase());
            getAndFillUserInfo();
            userInfoChanged = false;
        },
        error: function (response) {
            if(response.status === 400) {
                showError("INVALID VALUES");
            }
            if(response.status === 409){
                if(response.responseText.includes('LOGIN')){
                    $('.alertErrorUserLogin').append(response.responseText.toUpperCase());
                } else if(response.responseText.includes('PHONE')){
                    $('.alertErrorUserPhone').append(response.responseText.toUpperCase());
                } else if(response.responseText.includes('EMAIL')){
                    $('.alertErrorUserEmail').append(response.responseText.toUpperCase());
                } else {
                    showError(response.responseText.toUpperCase())
                }
            }
        }
    });
}

function getAndFillTicketsPreview() {
    $.ajax({
        type: 'GET',
        url: path + '/registered/getTicketsPreview',
        contentType: "application/json",
        success: function (data) {
            fillTicketsTable(data);
        },
        error: function (response) {
            showError(response.responseText.toUpperCase())
        }
    });
}

function fillTicketsTable(data) {
    let container = document.getElementById("ticketCol");
    if(data.length === 0) {
        container.innerHTML = `<span class="label-primary">YOUR TICKETS: \n YOU HAVE NO TICKETS</span>`;
        return;
    } else {
        container.innerHTML = `<span class="label-primary">YOUR TICKETS: \n CLICK AT THE TICKET TO SEE DETAILS</span>`;
    }
    for (let i = 0; i < data.length; i++) {
        let ticketRow = document.createElement('div');
        ticketRow.className = 'row ticketRow text-center butt';
        ticketRow.id = data[i].ticketId;
        ticketRow.innerHTML = `<div class="col text-center">${data[i].startStationName}-${data[i].endStationName}, ${data[i].departureDate}</div>`;
        container.append(ticketRow);
    }
    $('.ticketRow').click(function () {
        showTicketDetails(this.id);
    });
}

function showTicketDetails(ticketId) {
    $.ajax({
        type: 'GET',
        url: path + '/registered/getTicketDetails',
        contentType: "application/json",
        data: {
            ticketId: ticketId,
        },
        success: function (data) {
            showDetailedTicketInfo(data);
        },
        error: function (response) {
            showError(response.responseText.toUpperCase())
        }
    });
}

function showDetailedTicketInfo(data) {
    $('#ticketNumber').val(data.ticketId);
    $('#detailsPerson').val(data.userFirstName + ' ' + data.userLastName);
    $('#detailsRoute').val(data.startStation + '-' + data.endStation);
    $('#detailsDeparting').val(data.departingTime);
    $('#detailsArriving').val(data.arrivingTime);
    $('#detailsTrain').val(data.trainNumber);
    $('#detailsCar').val(data.carriageNumber + '/' + data.placeNumber);
    $('#detailsPrice').val(data.price);
    $('#printingTicket').val(data.ticketId);
    $('#ticketDetailsModal').modal('show');
}