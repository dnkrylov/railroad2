$(document).ready(function () {
    $('#submitStation').click(function () {
        if (!$('#newStationName').val()) {
            showError('STATION NAME SHOULD NOT BE EMPTY');
            return false;
        }

        let newStation = $('#newStationName').val();
        if(!validateStationNotExists(newStation)) {
            showError('STATION WITH SAME NAME ALREADY EXIST');
            return;
        }
        newStation.trim();
        $.ajax({
            type: 'POST',
            url: path + '/admin/addStation',
            data: {
                stationName: newStation
            },
            success: function (data) {
                $('#newStationName').val('');
                showModalInfo('CREATED', data.message.toUpperCase());
                updateStations();
            },
            error: function (response) {
                if (response.status === 403) {
                    showError("YOU HAVE NO PERMISSIONS TO MAKE THIS ACTION");
                    return;
                }
                showModalInfo('STATION IS NOT CREATED', response.responseText.toUpperCase());
            }
        });
    });
});