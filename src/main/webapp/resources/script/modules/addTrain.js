
$(document).ready(function () {
    updateCarTypes();
    clearTrainCreationForm();

    $('#addCarriage').click(function () {
        if (carTableReady === false) {
            createCarTable();
            carTableReady = true;
        }
        $('#carriageTable').show();
        $('#carriageTableBody').append('<tr id="carriageTableRow' + carriageCounter + '"></tr>');
        appendCarName();
        appendCarriageSelector();
        appendCapacity();
        carriageCounter++;
    });

    $('#removeCarriage').click(function () {
        if (carriageCounter > 0) {
            carriageCounter--;
            $('#carriageTableRow' + carriageCounter).remove();
        }
        if (carriageCounter === 0) {
            $('#carriageTable').hide();
        }
    });

    $('#addStation').click(function () {
        if (stationTableReady === false) {
            createStationTable();
            stationTableReady = true;
        }
        $('#stationTable').show();
        $('#stationTableBody').append('<tr id="stationTableRow' + stationCounter + '"></tr>');
        appendStationSelector();
        appendArrivalTime();
        appendDepartureTime();
        appendBusinessPrice();
        appendComfortPrice();
        appendEconomyPrice();
        $('.dateTime').datetimepicker({
            format: 'd.m.Y:H.i'
        });
        stationCounter++;
    });

    $('#removeStation').click(function () {
        if (stationCounter > 0) {
            stationCounter--;
            $('#stationTableRow' + stationCounter).remove();
        }
        if (stationCounter === 0) {
            $('#stationTable').hide();
        }
    });


    $('#submitTrain').click(function () {
        let train = createTrainDto();
        if (!validateTrain(train)) {
            return;
        }
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: path + "/admin/addTrain",
            data: JSON.stringify(train),
            dataType: "json",
            success: function (data) {
                showModalInfo('CREATED', data.message);
                clearTrainCreationForm();
            },
            error: function (response) {
                if (response.status === 403) {
                    showError("YOU HAVE NO PERMISSIONS TO MAKE THIS ACTION");
                    return;
                }
                showModalInfo('TRAIN IS NOT CREATED', response.responseText.toUpperCase());
            }
        });
    });
});

function createCarTable() {
    $('#carTableContainer').append('<table class="table table-bordered trainTable" id="carriageTable">' +
        '<thead><tr><th>CARRIAGE NUMBER</th><th>CARRIAGE TYPE</th><th>CARRIAGE CAPACITY</th></tr></thead>' +
        '<tbody id="carriageTableBody"></tbody>' +
        '</table>');
}

function appendCarName() {
    $('#carriageTableRow' + carriageCounter).append('<td>' +
        '<input type="text" class="text-center" placeholder="number" id="carriageName' + carriageCounter + '"/>');
}

function appendCarriageSelector() {
    $('#carriageTableRow' + carriageCounter).append('<td>' +
        '<select class="form-control" class="text-center" ' +
        'id="carriageType' + carriageCounter + '">');
    appendCarriageTypeOptions();
    $('#carriageTableRow' + carriageCounter).append('</select></td>');
}

function appendCarriageTypeOptions() {
    for (var i = 0; i < carriageTypes.length; i++) {
        $('#carriageType' + carriageCounter).append('<option value=' + carriageTypes[i] + '>' + carriageTypes[i] + '</option>');
    }
}

function appendCapacity() {
    $('#carriageTableRow' + carriageCounter).append('<td>' +
        '<input type="text" placeholder="capacity" class="text-center" id="carriageCapacity' + carriageCounter + '"/>' +
        '</tr>');
}

function createStationTable() {
    $('#stationTableContainer').append('<table class="table table-bordered trainTable" id="stationTable">' +
        '<thead><tr><th>SELECT STATION</th><th>ARRIVING TIME</th><th>DEPARTURE TIME</th>' +
        '<th>PRICE (BUSINESS)</th><th>PRICE (COMFORT)</th>' +
        '<th>PRICE (ECONOMY)</th></tr></thead>' +
        '<tbody id="stationTableBody"></tbody></table>');
}


function appendStationSelector() {
    $('#stationTableRow' + stationCounter).append('<td>' +
        '<input class="form-control text-center" autocomplete="off" ' +
        'id="selectedStation' + stationCounter + '" placeholder="start typing"/>' +
        '</td>');
    fillStationList(stationCounter);
}

function appendArrivalTime() {
    $('#stationTableRow' + stationCounter).append('<td>' +
        '<input type="text" class="text-center dateTime" placeholder="dd.mm.yyyy:hh.mm" ' +
        'id="arrivingTime' + stationCounter + '"' +
        '</td>');
}

function appendDepartureTime() {
    $('#stationTableRow' + stationCounter).append('<td>' +
        '<input type="text" class="text-center dateTime" placeholder="dd.mm.yyyy:hh.mm" ' +
        'id="departureTime' + stationCounter + '"' +
        '</td>');
}

function appendBusinessPrice() {
    if (stationCounter === 0) {
        $('#stationTableRow' + stationCounter).append('<td>' +
            '<input type="text" class="text-center priceInput"  disabled value="0" ' +
            'id="businessPrice' + stationCounter + '"' +
            '</td>');
    } else {
        $('#stationTableRow' + stationCounter).append('<td>' +
            '<input type="text" class="text-center priceInput"  ' +
            'id="businessPrice' + stationCounter + '"' +
            '</td>');
    }
}

function appendComfortPrice() {
    if (stationCounter === 0) {
        $('#stationTableRow' + stationCounter).append('<td>' +
            '<input type="text" class="text-center priceInput"  disabled value="0" ' +
            'id="comfortPrice' + stationCounter + '"</td>');
    } else {
        $('#stationTableRow' + stationCounter).append('<td>' +
            '<input type="text" class="text-center priceInput"  ' +
            'id="comfortPrice' + stationCounter + '"' +
            '</td>');
    }
}

function appendEconomyPrice() {
    if (stationCounter === 0) {
        $('#stationTableRow' + stationCounter).append('<td>' +
            '<input type="text" class="text-center priceInput"  disabled value="0" ' +
            'id="economyPrice' + stationCounter + '"' +
            '</td>');
    } else {
        $('#stationTableRow' + stationCounter).append('<td>' +
            '<input type="text" class="text-center priceInput"  ' +
            'id="economyPrice' + stationCounter + '"' +
            '</td>');
    }
}

function fillStationList(index) {
    $('#selectedStation' + index).easyAutocomplete(stations);
}

function createTrainDto() {
    let trainDto = {};
    trainDto["number"] = $('#trainNumber').val();
    trainDto["stationList"] = [];
    trainDto["carriages"] = [];
    for (let i = 0; i < stationCounter; i++) {
        trainDto.stationList.push({
            "stationName": $("#selectedStation" + i).val(),
            "arrivingTime": $("#arrivingTime" + i).val(),
            "departureTime": $("#departureTime" + i).val(),
            "businessPrice": $("#businessPrice" + i).val(),
            "comfortPrice": $("#comfortPrice" + i).val(),
            "economyPrice": $("#economyPrice" + i).val()
        });
    }
    for (let i = 0; i < carriageCounter; i++) {
        trainDto.carriages.push({
            "carName": $("#carriageName" + i).val(),
            "carType": $("#carriageType" + i).val(),
            "carCapacity": $("#carriageCapacity" + i).val()
        });
    }
    return trainDto;
}






