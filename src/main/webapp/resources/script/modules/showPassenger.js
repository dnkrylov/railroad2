$(document).ready(function () {
    $('.stationSelectAuto').easyAutocomplete(stations);
    $('#dateForAdminTimeTable').datetimepicker({
        timepicker: false,
        format: 'd.m.Y'
    });

    $('#getTrainsForAdmin').click(function () {
        getTrainsForPassengersShow();
    });
});

function fillTimeTable(data) {
    $('#adminTrainSelectionRow').empty();
    $('#adminTrainSelectionButtonRow').empty();
    if(data.length === 0) {
        showModalInfo("TRAINS NOT FOUND", "NO TRAINS DEPARTING FROM SELECTED STATION AT SELECTED DATE. TRY TO CHANGE DATE OR STATION");
        return;
    }
    $('#adminTrainSelectionRow').append('<label for="availableTrains">SELECT TRAIN</label>')
    $('#adminTrainSelectionRow').append('<select class="form-control showPassengerModuleInput" id="availableTrains"></select>');
    for (let i = 0; i < data.length; i++) {
        $('#availableTrains').append(($('<option class="text-center"></option>'))
            .attr('value', data[i].trainId).text(data[i].trainNumber));
    }
    $('#adminTrainSelectionButtonRow').append('<button type="button" class="butt showPassengerModuleButt" ' +
        'id="showPassengersTableButton">SHOW PASSENGERS</button>');

    $('#showPassengersTableButton').click(function () {
        getPassengersTable();
    });
}

function getPassengersTable() {
    var trainId = $("#availableTrains").val();
    if (trainId === '') {
        showError('SELECT TRAIN');
        return;
    }
    $.ajax({
        type: "GET",
        url: path + "/admin/getPassengers",
        data: {
            trainId: trainId
        },
        success: function (data) {
            fillPassengersTable(data);
        },
        error: function (response) {
            if (response.status === 403) {
                showError("YOU HAVE NO PERMISSIONS TO MAKE THIS ACTION");
                return;
            }
            showError(response.responseText.toUpperCase())
        }
    });
}

function getTrainsForPassengersShow() {
    let station = $("#stationForAdminTimeTable").val();
    let date = $("#dateForAdminTimeTable").val();
    if (!validateTimeTableInfo(date ,station)) {
        return;
    }
    $.ajax({
        type: "GET",
        url: path + "/admin/getTrains",
        data: {
            stationName: station,
            date: date
        },
        success: function (data) {
            fillTimeTable(data);
        },
        error: function (response) {
            showError(response.responseText.toUpperCase())
        }
    });
}

function fillPassengersTable(data) {
    $('#passengerTable').remove();
    if(data.length === 0) {
        showModalInfo("EMPTY TRAIN", "NO PASSENGERS REGISTERED FOR THIS TRAIN");
        return;
    }
    $('#passengerTableContainer').append('<table class="table table-bordered text-center" id="passengerTable">' +
        '<thead>' +
        '<th>Surname</th>' +
        '<th>Name</th>' +
        '<th>Login</th>' +
        '<th>Car.</th>' +
        '<th>Place</th>' +
        '<th>Start</th>' +
        '<th>End</th>' +
        '<th>Price</th>' +
        '</thead>' +
        '<tbody id="passengerTableBody"></tbody>' +
        '</table>');
    for(let i = 0; i < data.length; i++){
        $('#passengerTableBody').append('<tr>' +
            '<td>' + data[i].lastName + '</td><td>' + data[i].firstName + '</td><td>' + data[i].login + '</td><td>' + data[i].carriageNumber + '</td>' +
            '<td>' + data[i].placeNumber + '</td><td>' + data[i].startStationName + '</td>' +
            '<td>' + data[i].endStationName + '</td><td>' + data[i].price + '</td></tr>');
    }
}
