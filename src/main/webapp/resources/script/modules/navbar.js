$(document).ready(function () {
    $('#loginButton').click(function () {
        showLoginModal();
    });

    $('#logoutButton').click(function (){
        logout();
        $('#content').empty();
    });

    $('#registerButton').click(function () {
        showRegisterModal();
    });

    $('#personalAreaButton').click(function () {
        $('#content').empty();
        $('#content').load(path + "/resources/modules/cabinetModule.jsp");
    });
});

function logout() {
    $.ajax({
        type: 'GET',
        url: path + '/unregistered/tryToLogout',
        success: function () {
            $('#navbarSupportedContent').empty();
            $('#navbarSupportedContent').load(path + "/resources/modules/navbarModule.jsp");
            $('#sidebar-wrapper').empty();
            $('#sidebar-wrapper').load(path + "/resources/modules/sidebarModule.jsp");
        },
        error: function () {
            showError('SOMETHING IS WRONG. <br/><s>WE HAVE NO IDEA ABOUT IT</s><br/>' +
                ' WE ARE WORKING ON THIS PROBLEM');
        }
    });
}