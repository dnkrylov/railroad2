$(document).ready(function () {
    $('#navbarSupportedContent').load(path + "/resources/modules/navbarModule.jsp");
    $('#sidebar-wrapper').load(path + "/resources/modules/sidebarModule.jsp");
    updateStations();

    let initialMessage = $('#initialMessage').val();
    if(initialMessage) {
        if(initialMessage.includes("CONFIRMED")){
            showModalInfo("SUCCESS", initialMessage);
        } else {
            showError(initialMessage)
        }

        $('#initialMessage').remove();
        window.history.pushState("String", "RAILROAD", "/")
    }
});

// globalvars
//ADD TRAIN
let stationCounter = 0;
let carriageCounter = 0;
let carTableReady = false;
let stationTableReady = false;

//FIND TRAINS
let startStation;
let endStation;
let routeDate;
let isItArrivingDate;
let directTrains = [];
let transferTrains = [];
let directTicket = true;

//CABINET
let userInfoChanged = false;
