$(document).ready(function () {
    fillTransferTrainsTable();
    $('.transferTrainRow').click(function () {
        let selectedRoute = this.id;
        let chosenTransferRouteDto = transferTrains[selectedRoute];
        createTransferCarriagesContainer(this.id);
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: path + "/registered/showTransferCarriages",
            data: JSON.stringify(chosenTransferRouteDto),
            dataType: "json",
            success: function (data) {
                showTransferPlaces(data);
            },
            error: function (response) {
                if (response.status === 403) {
                    $('#loginModal').modal('show');
                }
            }
        });
    });
});

function fillTransferTrainsTable() {
    $('#transferTrainTable').empty();
    let transferTrainTable = document.getElementById('transferTrainTable');
    if (transferTrains.length > 0) {
        let titleRow = document.createElement('div');
        titleRow.className = 'row';
        let startStationName = $("#selectStartStation").val();
        let middleStationName = 'TRANSFER STATION';
        let endStationName = $("#selectEndStation").val();
        titleRow.innerHTML = `<div class="col text-center">${startStationName} DEPARTURE TIME</div>
            <div class="col text-center">TRAIN NUMBER</div>
            <div class="col text-center">ARRIVAL TIME</div>
            <div class="col text-center">${middleStationName}</div>
            <div class="col text-center">DEPARTURE TIME</div>
            <div class="col text-center">TRAIN NUMBER</div>
            <div class="col">${endStationName} ARRIVAL TIME</div>`;
        transferTrainTable.append(titleRow);
    }
    for (let i = 0; i < transferTrains.length; i++) {
        let trainRow = document.createElement('div');
        trainRow.className = 'row transferTrainRow text-center butt lightTrainRow';
        trainRow.id = `${i}`;
        trainRow.innerHTML = `<div class="col">${transferTrains[i].startTime1}</div>
        <div class="col">${transferTrains[i].trainNumber1}</div>
        <div class="col">${transferTrains[i].endTime1}</div>
        <div class="col">${transferTrains[i].middleStationName}</div>
        <div class="col">${transferTrains[i].startTime2}</div>
        <div class="col">${transferTrains[i].trainNumber2}</div>
        <div class="col">${transferTrains[i].endTime2}</div>`;
        transferTrainTable.append(trainRow);
    }
}

function createTransferCarriagesContainer(id) {
    $('.transferCarriagesContainer').remove();
    let trainRow = document.getElementById(id);
    let transferCarriageContainer = document.createElement('div');
    transferCarriageContainer.id = `transferCarriagesContainer`;
    transferCarriageContainer.className = 'row transferCarriagesContainer d-flex justify-content-center text-center';
    trainRow.after(transferCarriageContainer);
}


function showTransferPlaces(data) {



    // $('.buyTransferTicketButton').remove();


    $('#transferCarriagesContainer').append('<div class="row carriageContainer text-center" id="innerTransferCarriagesContainer"></div>');
    let carriageContainer = document.getElementById(`innerTransferCarriagesContainer`);


    // let carriageContainer = document.getElementById(`transferCarriagesContainer`);
    let train1Col = document.createElement('div');
    let train2Col = document.createElement('div');
    train1Col.className = 'col text-center carColTransfer';
    train2Col.className = 'col text-center carColTransfer';
    train1Col.id = 'trainCol1';
    train2Col.id = 'trainCol2';
    let middleCol = document.createElement('div');
    middleCol.className = 'col col-sm middleCol';
    carriageContainer.append(train1Col);
    carriageContainer.append(middleCol);
    carriageContainer.append(train2Col);
    fillCarriageRow(data.train1Carriages, 1);
    fillCarriageRow(data.train2Carriages, 2);


    let buyTransferTicketButton = document.createElement('button');
    buyTransferTicketButton.id = 'buyTransferTicketButton';
    buyTransferTicketButton.className = 'butt buyTransferTicketButton buyTicketButt';
    buyTransferTicketButton.innerText = 'BUY TICKET';
    // $('#transferCarriagesContainer').append(buyTransferTicketButton);
     carriageContainer.after(buyTransferTicketButton);
    $('#buyTransferTicketButton').click(function () {
        buyTransferTicket();
    });
}

function fillCarriageRow(carriages, colNumber) {
    let header = document.createElement('div');
    header.className = 'row';
    header.innerHTML = '<div class = "col col-sm-1 text-right checkboxCol"></div><div class = "col text-center">TYPE</div><div class = "col text-center">PRICE</div><div class = "col text-center">PLACE</div>';
    let carriageContainer = document.getElementById(`trainCol${colNumber}`);
    carriageContainer.append(header);
    for (let i = 0; i < carriages.length; i++) {
        let carriage = carriages[i];
        let carriageRow = document.createElement('div');
        carriageRow.className = `row carriageRow`;
        carriageRow.innerHTML = `<div class="col col-sm-1 text-right checkboxCol" id="radioCol${colNumber}${i}"></div><div class="col text-center">${carriage.carriageType}</div><div class="col text-center">${carriage.ticketPrice}</div><div class="col col-sm-3" id="selectCol${colNumber}${i}"></div>`;
        carriageContainer.append(carriageRow);
        let radio = document.createElement('input');
        radio.type = 'radio';
        radio.className = 'radio';
        radio.name = `carriageRadio${colNumber}`;
        radio.value = carriage.carriageId;
        if (carriage.freePlaceNumbers.length === 0) {
            document.getElementById(`selectCol${colNumber}${i}`).append('-');
            radio.disabled = true;
        } else {
            let select = document.createElement('select');
            select.id = `placeSelect${carriage.carriageId}`;
            select.className = 'form-control placeSelect';
            radio.disabled = false;
            document.getElementById(`selectCol${colNumber}${i}`).append(select);
            carriage.freePlaceNumbers.forEach(function (placeNumber) {
                let option = document.createElement('option');
                option.innerText = placeNumber;
                select.append(option);
            });
        }
        document.getElementById(`radioCol${colNumber}${i}`).append(radio);
    }
}

function buyTransferTicket() {
    let carriage1Id = $('input[name = carriageRadio1]:checked').val();
    let carriage2Id = $('input[name = carriageRadio2]:checked').val();
    if(carriage1Id === undefined || carriage2Id === undefined){
        showError("SELECT CARRIAGES")
        return;
    }
    let place1Number = $('#placeSelect' + carriage1Id).val();
    let place2Number = $('#placeSelect' + carriage2Id).val();
    directTicket = false;
    $.ajax({
        type: "POST",
        url: path + "/registered/showTransferPaymentModal",
        data: {
            carriage1Id: carriage1Id,
            carriage2Id: carriage2Id,
            place1Number: place1Number,
            place2Number: place2Number
        },
        success: function (data) {
            $('#ticketPriceInfo').html(data.message);
            $('#paymentModal').modal('show');
        },
        error: function (response) {
            if (response.status === 403) {
                $('#loginModal').modal('show');
            }
            if (response.status === 409) {
                showModalInfo('WE ARE SORRY', response.responseText.toUpperCase())
            }
        }
    });
}
