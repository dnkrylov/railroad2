$(document).ready(function () {
    fillDirectTrainsTable();
    $('.trainRow').click(function () {
        let trainId = this.id;
        createCarriagesContainer(trainId);
        $.ajax({
            type: "POST",
            url: path + "/registered/showDirectCarriages",
            data: {
                trainId: trainId
            },
            success: function (data) {
                showPlaces(data);
            },
            error: function (response) {
                if (response.status === 403) {
                    $('#loginModal').modal('show');
                }
            }
        });
    });
});

function fillDirectTrainsTable() {
    $('#directTrainTable').empty();
    let directTrainsTable = document.getElementById('directTrainTable');
    if (directTrains.length > 0) {
        let titleRow = document.createElement('div');
        titleRow.className = 'row text-center';
        let startStationName = $("#selectStartStation").val();
        let endStationName = $("#selectEndStation").val();
        titleRow.innerHTML = `<div class="col">TRAIN NUMBER</div>
            <div class="col">${startStationName} DEPARTURE TIME</div>
            <div class="col">${endStationName} ARRIVAL TIME</div>`;
        directTrainsTable.append(titleRow);
    }
    for (let i = 0; i < directTrains.length; i++) {
        let trainRow = document.createElement('div');
        trainRow.className = 'row trainRow text-center butt lightTrainRow';
        trainRow.id = directTrains[i].trainId;
        trainRow.innerHTML = `<div class="col">${directTrains[i].trainNumber}</div>
            <div class="col">${directTrains[i].startTime}</div>
            <div class="col">${directTrains[i].endTime}</div>`;
        directTrainsTable.append(trainRow);
    }
}

function createCarriagesContainer(trainId) {
    $('.directCarriagesContainer').remove();
    let trainRow = document.getElementById(trainId);
    let carriageContainer = document.createElement('div');
    carriageContainer.id = `directCarriagesContainer`;
    carriageContainer.className = 'row directCarriagesContainer d-flex justify-content-center';
    trainRow.after(carriageContainer);
}

function showPlaces(data) {
    $('#directCarriagesContainer').append('<div class="col carriageContainer text-center" id="innerDirectCarriagesContainer"></div>');
    let carriageContainer = document.getElementById(`innerDirectCarriagesContainer`);
    let header = document.createElement('div');
    header.className = 'row';
    header.innerHTML = '<div class = "col col-sm-1 text-right checkboxCol"></div>' +
        '<div class = "col text-center">NUMBER</div><div class = "col text-center">TYPE</div><div class = "col text-center">PRICE</div><div class = "col text-center">PLACE</div>';
    carriageContainer.append(header);
    for (let i = 0; i < data.length; i++) {
        let carriage = data[i];
        let carriageRow = document.createElement('div');
        carriageRow.className = `row carriageRow`;
        carriageRow.innerHTML = `<div class="col col-sm-1 text-right checkboxCol" id="radioCol${i}"></div><div class="col text-center">${carriage.number}</div><div class="col text-center">${carriage.carriageType}</div><div class="col text-center">${carriage.ticketPrice}</div><div class="col text-center " id="selectCol${i}"></div>`;
        carriageContainer.append(carriageRow);
        let radio = document.createElement('input');
        radio.type = 'radio';
        radio.className = 'form-check-input';
        radio.name = 'carriageRadio';
        radio.value = carriage.carriageId;
        if (carriage.freePlaceNumbers.length === 0) {
            document.getElementById(`selectCol${i}`).append('-');
            radio.disabled = true;
        } else {
            let select = document.createElement('select');
            select.id = `placeSelect${carriage.carriageId}`;
            select.className = 'form-control';
            radio.disabled = false;
            document.getElementById(`selectCol${i}`).append(select);
            carriage.freePlaceNumbers.forEach(function (placeNumber) {
                let option = document.createElement('option');
                option.innerText = placeNumber;
                select.append(option);
            });
        }
        document.getElementById(`radioCol${i}`).append(radio);
    }
    let buyTicketButton = document.createElement('button');
    buyTicketButton.id = 'buyThisTicketButton';
    buyTicketButton.className = 'butt buyTicketButt';
    buyTicketButton.innerText = 'BUY TICKET';
    carriageContainer.append(buyTicketButton);
    $('#buyThisTicketButton').click(function () {
        buyTicket();
    });
}

function buyTicket() {
    let carriageId = $('input[name = carriageRadio]:checked').val();
    let selectedPlace = $(`#placeSelect${carriageId}`).val();
    if (carriageId === undefined) {
        showError("SELECT CARRIAGE")
        return;
    }
    directTicket = true;
    $.ajax({
        type: "POST",
        url: path + "/registered/showPaymentModal",
        data: {
            carriageId: carriageId,
            placeNumber: selectedPlace
        },
        success: function (data) {
            $('#ticketPriceInfo').html(data.message);
            $('#paymentModal').modal('show');
        },
        error: function (response) {
            if (response.status === 403) {
                $('#loginModal').modal('show');
            }
            if (response.status === 409) {
                $('#myModalTitle').html('WE ARE SORRY');
                $('#myModalBody').html(response.responseText.toUpperCase());
                $('#myModal').modal('show');
            }
        }
    });
}
