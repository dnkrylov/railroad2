function clearPaymentValues() {
    $("#cardNumber").val('');
    $("#cardExp").val('');
    $("#cardCVV").val('');
    $('#trainsForRouteContainer').empty();
}

function clearLoginValues() {
    $("#login").val('');
    $("#pwd").val('');
}

function clearTrainCreationForm() {
    $('#carriageTableBody').empty();
    $('#stationTableBody').empty();
    $('#stationTable').hide();
    $('#carriageTable').hide();
    $('#trainNumber').val('');
    stationCounter = 0;
    carriageCounter = 0;
}

function clearRegisterValues() {
    $("#userFirstName").val('');
    $("#userLastName").val('');
    $("#userDateOfBirth").val('');
    $("#userLogin").val('');
    $("#userPassword").val('');
    $("#userEmail").val('');
    $("#userPhone").val('');
}

function clearGlobalVariables() {
//ADD TRAIN
    stationCounter = 0;
    carriageCounter = 0;
    carTableReady = false;
    stationTableReady = false;

//FIND TRAINS
    startStation ='';
    endStation ='';
    routeDate = '';
    isItArrivingDate = false;
    directTrains = [];
    transferTrains = [];
    directTicket = true;

//CABINET
    userInfoChanged = false;
}