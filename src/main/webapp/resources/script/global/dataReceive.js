let stations = {
    data: [],
    list: {
        match: {
            enabled: true
        }
    }
};
let carriageTypes = {};

function updateStations() {
    stations.data = [];
    $.get('unregistered/getStations', function (data) {
        for (let i = 0; i < data.length; i++) {
            stations.data.push(data[i]);
        }
    });
}

function updateCarTypes() {
    $.get('unregistered/getCarriageTypes', function (data) {
        carriageTypes = data;
    });
}

