function showError(globalErrorMessage) {
    $('#myModalTitle').html('ERROR');
    $('#myModalBody').html(globalErrorMessage);
    $('#myModal').modal('show');
}

function showModalInfo(header, body) {
    $('#myModalTitle').text(header);
    $('#myModalBody').text(body);
    $('#myModal').modal('show');
}

function showLoginModal() {
    $('#registerModal').modal('hide');
    clearLoginValues();
    $('#loginModal').modal('show');
    $('.alertError').empty();
}

function showRegisterModal() {
    clearRegisterValues();
    $('#loginModal').modal('hide');
    $('#registerModal').modal('show');
    $('.alertError').empty();
}

