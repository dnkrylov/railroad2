let cardPattern = /^[0-9]{16}/;
let expPattern = /^(0[1-9]|1[0-2])([\/])([0-9][0-9])$/;
let cvvPattern = /^([0-9][0-9][0-9])$/;

let datePattern = /^(0[1-9]|1[0-9]|2[0-9]|3[0-1])([.])(0[1-9]|1[0-2])([.])([0-9][0-9][0-9][0-9])$/;
let timePattern = /^(0[1-9]|1[0-9]|2[0-9]|3[0-1])([.])(0[1-9]|1[0-2])([.])([0-9][0-9][0-9][0-9])([:])(0[0-9]|1[0-9]|2[0-3])([.])([0-5][0-9])$/;

let pricePattern = /^[0-9]{0,8}([.][0-9]{2})?$/;
let capacityPattern = /^([1-9][0-9]{0,2})$/;

let loginPattern = /^[A-Za-z0-9]{3,16}$/;
let passwordPattern = /^[A-Za-z0-9]{3,16}$/;
let phonePattern = /^[+][0-9]{7,16}$/;
let emailPattern = /^[A-Za-z0-9._%+-]{1,50}[@][A-Za-z0-9._%+-]{1,30}[.][A-Za-z]{2,10}$/;

function validateStationExists(station) {
    if (!stations.data.includes(station) || station === '') {
        showError('STATION WITH NAME ' + station + '\n DOES NOT EXISTS');
        return false;
    }
    return true;
}

function validateStationNotExists(station) {
    if (stations.data.includes(station)) {
        return false;
    }
    return true;
}

function validateDate(date) {
    if (!datePattern.test(date)) {
        showError('INVALID DATE FORMAT');
        return false;
    }
    return true;
}

// CREDIT CARD VALIDATION
function validateCardNumber(cardNumber) {
    if (!cardPattern.test(cardNumber)) {
        $('#alertCardNumber').html('INVALID CREDIT CARD NUMBER');
        return false;
    }
    return true;
}

function validateCardExp(cardExp) {
    if (!expPattern.test(cardExp)) {
        $('#alertCardExp').html('INVALID CREDIT CARD EXPIRATION');
        return false;
    }
    return true;
}

function validateCVV(cardCVV) {
    if (!cvvPattern.test(cardCVV)) {
        $('#alertCardCVV').html('INVALID CREDIT CARD CVV');
        return false;
    }
    return true;
}

//TRAIN VALIDATION
function validateTrain(train) {
    if (!$('#trainNumber').val()) {
        showError('INPUT TRAIN NUMBER');
        return false;
    }
    if (train.stationList.length < 2) {
        showError('ROUTE SHOULD CONTAIN AT LEAST 2 STATIONS');
        return false;
    }
    if (!validateTrainStations(train.stationList)) {
        return false;
    }
    if (train.carriages.length < 1) {
        showError('TRAIN SHOULD HAVE AT LEAST 1 CARRIAGE');
        return false;
    }
    if (!validateTrainCarriages(train.carriages)) {
        return false;
    }
    return true;
}

function validateTrainStations(stationList) {
    for (var i = 0; i < stationList.length; i++) {
        if(!validateStationExists(stationList[i].stationName)) {
            showError("STATION " + stationList[i].stationName + " DOES NOT EXIST");
            return false;
        }

        if (!timePattern.test(stationList[i].arrivingTime)) {
            showError("INVALID " + stationList[i].stationName + " ARRIVALTIME");
            return false;
        }
        if (!timePattern.test(stationList[i].departureTime)) {
            showError("INVALID " + stationList[i].stationName + " DEPARTURETIME");
            return false;
        }
        if (!pricePattern.test(stationList[i].businessPrice)) {
            showError("INVALID " + stationList[i].stationName + " BUSINESS PRICE \n " +
                "SELECT PRICE BETWEEN 0.00 AND 99999999.99");
            return false;
        }
        if (!pricePattern.test(stationList[i].comfortPrice)) {
            showError("INVALID " + stationList[i].stationName + " COMFORT PRICE  \n" +
                "SELECT PRICE BETWEEN 0.00 AND 99999999.99");
            return false;
        }
        if (!pricePattern.test(stationList[i].economyPrice)) {
            showError("INVALID " + stationList[i].stationName + " ECONOMY PRICE \n " +
                "SELECT PRICE BETWEEN 0.00 AND 99999999.99");
            return false;
        }
        if (!$("#economyPrice" + i).val() || !$("#comfortPrice" + i).val() || !$("#businessPrice" + i).val()) {
            showError("INVALID " + stationList[i].stationName + " PRICE \n " +
                "SELECT PRICE BETWEEN 0.00 AND 99999999.99");
            return false;
        }
    }
    return true;
}

function validateTrainCarriages(carriageList) {

    for (var i = 0; i < carriageList.length; i++) {
        if (carriageList[i].carName === '') {
            showError("INVALID CARRIAGE " + (i + 1) + " NUMBER");
            return false;
        }
        if (!capacityPattern.test(carriageList[i].carCapacity)) {
            showError("INVALID CARRIAGE " + carriageList[i].carName + " CAPACITY" +
                " \n SHOULD BE BETWEEN 1 AND 999");
            return false;
        }
    }
    return true;
}

//TIMETABLE VALIDATION
function validateTimeTableInfo(date, station) {
    if(!validateDate(date) || !validateStationExists(station)){
        return false;
    }
    return true;
}

//FIND ROUTES VALIDATION
function validateRouteInfo(station1, station2, date, flag) {
    if (!validateStationExists(station1) || !validateStationExists(station2) || !validateDate(date)) {
        return false;
    }
    if (station1 === station2) {
        showError("SELECT DIFFERENT STATIONS");
        return false;
    }
    return true;
}

//LOGIN VALIDATION
function validateLoginInfo(login) {
    if (!loginPattern.test(login)) {
        $('#alertErrorLogin').append('LOGIN SHOULD CONTAIN 3-16 LETTERS');
        return false;
    }
    if (!$("#pwd").val()) {
        $('#alertErrorPassword').append('INCORRECT PASSWORD');
        return false;
    }
    return true;
}

//REGISTER VALIDATION
function validateRegisterParameters(userDto) {

    if (userDto.firstName === '') {
        $('.alertErrorUserFirstName').append('INPUT NAME');
        return false;
    }
    if (userDto.lastName === '') {
        $('.alertErrorUserLastName').append('INPUT SURNAME');
        return false;
    }
    if (!datePattern.test(userDto.dateOfBirth)) {
        $('.alertErrorUserDateOfBirth').append('INCORRECT DATE OF BIRTH FORMAT');
        return false;
    }
    if (!phonePattern.test(userDto.phone)) {
        $('.alertErrorUserPhone').append('INCORRECT PHONE FORMAT. INPUT + AND DIGITS');
        return false;
    }
    if (!emailPattern.test(userDto.email)) {
        $('.alertErrorUserEmail').append('INCORRECT EMAIL');
        return false;
    }
    if (!loginPattern.test(userDto.login)) {
        $('.alertErrorUserLogin').append('LOGIN SHOULD CONTAIN 3-16 LETTERS');
        return false;
    }
    if (!passwordPattern.test(userDto.password)) {
        $('.alertErrorUserPassword').append('PASSWORD SHOULD CONTAIN FROM 3 TO 16 LETTERS OR DIGITS');
        return false;
    }
    return true;
}