<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="<c:url value="/resources/script/modal/login.js"/> "></script>

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="loginModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body text-center" id="modalLoginBody">
                <div class="form-group text-center">
                    <label for="login">Login:</label>
                    <input type="text" autocomplete="off" required class="form-control text-center" id="login"
                           placeholder="3-16 letters or digits">
                    <div class="alert-danger text-center alertError" id="alertErrorLogin">

                    </div>
                </div>

                <div class="form-group text-center">
                    <label for="pwd">Password:</label>
                    <input type="password" required autocomplete="off" class="form-control text-center" id="pwd"
                           placeholder="Enter your password">
                    <div class="alert-danger text-center alertError" id="alertErrorPassword">

                    </div>
                </div>

                <button type="submit" class="butt modal-butt" id="submitLoginButton">
                    LOG IN!
                </button>
            </div>
        </div>
    </div>
</div>
