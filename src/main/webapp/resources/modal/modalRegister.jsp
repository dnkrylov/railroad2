<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="<c:url value="/resources/script/modal/registration.js"/> "></script>
<script src='https://www.google.com/recaptcha/api.js?hl=ru'></script>

<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="registerModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body text-center" id="modalLoginBody">
                <form id="registerForm">
                    <div class="form-group text-center">
                        <label for="userFirstName">Name:</label>
                        <input type="text" required autocomplete="off" class="form-control text-center"
                               id="userFirstName"
                               placeholder="Enter your name">
                        <div class="alert-danger text-center alertError alertErrorUserFirstName">

                        </div>
                    </div>
                    <div class="form-group text-center">
                        <label for="userLastName">Surname:</label>
                        <input type="text" required autocomplete="off" class="form-control text-center"
                               id="userLastName"
                               placeholder="Enter your surname">
                        <div class="alert-danger text-center alertError alertErrorUserLastName">

                        </div>
                    </div>

                    <div class="form-group text-center">
                        <label for="userDateOfBirth">Date of birth:</label>
                        <input type="text" autocomplete="off" class="form-control text-center"
                               id="userDateOfBirth"
                               placeholder="Enter your dateOfBirth">
                        <div class="alert-danger text-center alertError alertErrorUserDateOfBirth">
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <label for="userPhone">Phone number:</label>
                        <input type="text" required autocomplete="off" class="form-control text-center" id="userPhone"
                               placeholder="+XXXXXXXXXX">
                        <div class="alert-danger text-center alertError alertErrorUserPhone">
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <label for="userEmail">Email:</label>
                        <input type="text" required autocomplete="off" class="form-control text-center" id="userEmail"
                               placeholder="input your email">
                        <div class="alert-danger text-center alertError alertErrorUserEmail">
                        </div>
                    </div>

                    <div class="form-group text-center">
                        <label for="userLogin">Login:</label>
                        <input type="text" required autocomplete="off" class="form-control text-center" id="userLogin"
                               placeholder="3-16 letters or digits">
                        <div class="alert-danger text-center alertError alertErrorUserLogin">

                        </div>

                    </div>
                    <div class="form-group text-center">
                        <label for="userPassword">Password:</label>
                        <input type="password" required class="form-control text-center" id="userPassword"
                               placeholder="Enter your password">
                        <div class="alert-danger text-center alertError alertErrorUserPassword">

                        </div>
                    </div>

                    <div class="form-group text-center">
                        <div class="g-recaptcha"
                             data-sitekey="6LdVd7UUAAAAALPWVZQKCcIjq9ZoV2SYC_oDzY8I"></div>
                        <div class="alert-danger text-center alertError alertErrorUserCaptcha">

                        </div>

                        <button type="submit" class="butt modal-butt" id="submitRegisterButton">
                            REGISTER!
                        </button>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
