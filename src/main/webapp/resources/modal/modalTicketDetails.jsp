<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="<c:url value="/resources/script/modal/ticketDetails.js"/> "></script>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="ticketDetailsModal">
    <div class="modal-dialog">
        <div class="modal-content text-center" id="ticketDetailsModalContent">
            <div class="modal-body text-center">
                <div class="form-group text-center ticketDetailsFormGroup">
                    <label for="detailsPerson">TICKET NUMBER:</label>
                    <input type="text" disabled class="form-control text-center" id="ticketNumber">
                </div>
                <div class="form-group text-center ticketDetailsFormGroup">
                    <label for="detailsPerson">PERSON:</label>
                    <input type="text" disabled class="form-control text-center" id="detailsPerson">
                </div>
                <div class="form-group text-center ticketDetailsFormGroup">
                    <label for="detailsRoute">ROUTE:</label>
                    <input type="text" disabled class="form-control text-center" id="detailsRoute">
                </div>
                <div class="form-group text-center ticketDetailsFormGroup">
                    <label for="detailsDeparting">DEPARTING:</label>
                    <input type="text" disabled class="form-control text-center" id="detailsDeparting">
                </div>
                <div class="form-group text-center ticketDetailsFormGroup">
                    <label for="detailsArriving">ARRIVING:</label>
                    <input type="text" disabled class="form-control text-center" id="detailsArriving">
                </div>
                <div class="form-group text-center ticketDetailsFormGroup">
                    <label for="detailsTrain">TRAIN:</label>
                    <input type="text" disabled class="form-control text-center" id="detailsTrain">
                </div>
                <div class="form-group text-center ticketDetailsFormGroup">
                    <label for="detailsCar">CARRIAGE/PLACE:</label>
                    <input type="text" disabled class="form-control text-center" id="detailsCar">
                </div>
                <div class="form-group text-center ticketDetailsFormGroup">
                    <label for="detailsPrice">PRICE:</label>
                    <input type="text" disabled class="form-control text-center" id="detailsPrice">

                </div>
                <button type="button" class="butt" id="sendTicketByEmail">SENT TO EMAIL</button>
                <input type="hidden" id="printingTicket" value="">
            </div>
        </div>
    </div>
</div>
