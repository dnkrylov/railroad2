<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="<c:url value="/resources/script/modal/payment.js"/> "></script>
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="paymentModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body text-center" id="paymentModalBody">
                <div class="col text-center" id="paymentInfoCol">
                    <h4 class="simpleInfoField">INPUT PAYMENT INFO</h4>
                    <h4 id="ticketPriceInfo"></h4>
                    <div class="form-group text-center">
                        <label for="cardNumber">credit card number: </label>
                        <input type="text" class="form-control text-center" autocomplete="off" id="cardNumber"
                               placeholder="XXXXXXXXXXXXXXXX"/>
                        <div class="alert-danger text-center alertError" id="alertCardNumber">
                        </div>
                        <div class="form-group text-center">
                            <label for="cardExp">credit card expiration: </label>
                            <input type="text" class="form-control text-center" autocomplete="off" id="cardExp"
                                   placeholder="mm/yy"/>
                            <div class="alert-danger text-center alertError" id="alertCardExp">
                            </div>
                            <div class="form-group text-center">
                                <label for="cardCVV">CVV: </label>
                                <input type="text" class="form-control text-center" autocomplete="off" id="cardCVV"
                                       placeholder="XXX"/>
                                <div class="alert-danger text-center alertError" id="alertCardCVV">
                                </div>
                                <button type="button" class="butt" id="buyTicket">BUY TICKET</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
