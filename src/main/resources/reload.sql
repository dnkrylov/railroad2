DROP TABLE IF EXISTS js_verification_token;
DROP TABLE IF EXISTS js_price;
DROP TABLE IF EXISTS js_route;
DROP TABLE IF EXISTS js_ticket;
DROP TABLE IF EXISTS js_carriage;
DROP TABLE IF EXISTS js_train;
DROP TABLE IF EXISTS js_station;
DROP TABLE IF EXISTS js_user;

CREATE TABLE js_user
(
    user_id bigserial not null,
    user_first_name varchar(100) not null,
    user_last_name varchar(100) not null,
    user_date_of_birth timestamp not null,
    user_login varchar(100) unique not null,
    user_password varchar(100) not null,
    user_email varchar(100) unique not null,
    user_phone varchar (100) unique not null,
    user_role varchar(100) not null,
    user_enabled boolean not null,
    PRIMARY KEY (user_id)
);

CREATE TABLE js_verification_token
(
    token_id bigserial not null,
    user_id bigint not null,
    token_token varchar (100),
    token_expire_date timestamp not null,
    PRIMARY KEY (token_id),
    FOREIGN KEY (user_id) REFERENCES js_user(user_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE js_train
(
    train_id bigserial not null,
    train_name varchar(100) not null,
    PRIMARY KEY (train_id)
);

CREATE TABLE js_carriage
(
    carriage_id bigserial not null,
    train_id bigint not null,
    carriage_number varchar(100) not null,
    carriage_type varchar(100) not null,
    carriage_capacity integer not null,
    PRIMARY KEY (carriage_id),
    FOREIGN KEY (train_id) REFERENCES js_train(train_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE js_station
(
    station_id bigserial not null,
    station_name varchar(300) unique not null,
    PRIMARY KEY (station_id)
);


CREATE TABLE js_ticket
(
    ticket_id bigserial not null,
    carriage_id bigint not null,
    place_number integer not null,
    user_id bigint not null,
    start_station_id bigint,
    finish_station_id bigint,
    ticket_price decimal,
    creation_time timestamp not null,
    PRIMARY KEY (ticket_id),
    FOREIGN KEY (carriage_id) REFERENCES js_carriage(carriage_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (start_station_id) REFERENCES js_station(station_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (finish_station_id) REFERENCES js_station(station_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (user_id) REFERENCES js_user(user_id) ON DELETE RESTRICT ON UPDATE CASCADE
);
CREATE UNIQUE INDEX ticket_constraint ON js_ticket (carriage_id, place_number) WHERE (ticket_price < 0);

CREATE TABLE js_price
(
    price_part_id bigserial not null,
    carriage_type varchar(100) not null,
    train_id bigint not null,
    start_station_id bigint not null,
    finish_station_id bigint not null,
    price decimal,
    PRIMARY KEY (price_part_id),
    FOREIGN KEY (train_id) REFERENCES js_train(train_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (start_station_id) REFERENCES js_station(station_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (finish_station_id) REFERENCES js_station(station_id) ON DELETE RESTRICT ON UPDATE CASCADE,
    UNIQUE (carriage_type, train_id, start_station_id, finish_station_id)
);


CREATE TABLE js_route
(
    route_node_id bigserial not null,
    train_id bigserial not null,
    station_id bigint not null,
    arrival_time timestamp not null,
    departure_time timestamp not null,
    PRIMARY KEY (route_node_id),
    FOREIGN KEY (train_id) REFERENCES js_train(train_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (station_id) REFERENCES js_station(station_id) ON DELETE CASCADE ON UPDATE CASCADE,
    UNIQUE (train_id, station_id)
);


INSERT INTO js_train ("train_name")
VALUES ('SPB-VORONEZH'),
       ('TVER-TULA'),
       ('MOSCOW-ANAPA'),
       ('SPB-ANAPA'),
       ('ROSTOV-SPB'),
       ('VORONEZH-SPB'),
       ('ROSTOV-TVER');

INSERT INTO js_carriage ("train_id", "carriage_type", "carriage_number", "carriage_capacity")
VALUES
       (1, 'BUSINESS', '1 carriage', 15),
       (1, 'COMFORT', '2 carriage', 30),
       (1, 'ECONOMY', '3 carriage', 35),
       (1, 'COMFORT', '4 carriage', 30),
       (2, 'BUSINESS', '1 carriage', 15),
       (2, 'COMFORT', '2 carriage', 30),
       (2, 'ECONOMY', '3 carriage', 35),
       (2, 'COMFORT', '4 carriage', 30),
       (3, 'BUSINESS', '1 carriage', 15),
       (3, 'COMFORT', '2 carriage', 30),
       (3, 'ECONOMY', '3 carriage', 35),
       (3, 'COMFORT', '4 carriage', 30),
       (4, 'BUSINESS', '1 carriage', 15),
       (4, 'COMFORT', '2 carriage', 30),
       (4, 'ECONOMY', '3 carriage', 35),
       (4, 'COMFORT', '4 carriage', 30),
       (5, 'BUSINESS', '1 carriage', 15),
       (5, 'COMFORT', '2 carriage', 30),
       (5, 'ECONOMY', '3 carriage', 35),
       (6, 'COMFORT', '4 carriage', 30),
       (6, 'BUSINESS', '1 carriage', 15),
       (6, 'COMFORT', '2 carriage', 30),
       (7, 'ECONOMY', '3 carriage', 35),
       (7, 'COMFORT', '4 carriage', 30),
       (7, 'BUSINESS', '1 carriage', 15);



INSERT INTO js_user ("user_first_name", "user_last_name", "user_date_of_birth", "user_login",
                     "user_password", "user_email", "user_phone", "user_role", "user_enabled")
VALUES ('admin', 'admin', '1990-06-14', 'admin', '$2a$10$iAEq9SVkG8WnYtLz.fonU.wIXnfa.G1H411XFt/xycp/Bi7uBjWce',
        'krylov.dmitry90@gmail.com', '+79214357668', 'ROLE_ADMIN', 'true'),
       ('user', 'user', '1990-06-14', 'user', '$2a$10$iAEq9SVkG8WnYtLz.fonU.wIXnfa.G1H411XFt/xycp/Bi7uBjWce',
        'test@gmail.com', '+1234567890','ROLE_USER', 'true'),
       ('test5', 'test5', '1990-06-14', 'test5', '$2a$10$iAEq9SVkG8WnYtLz.fonU.wIXnfa.G1H411XFt/xycp/Bi7uBjWce',
        'tttttt@mail.ru', '+71111111111','ROLE_USER', 'true');
-- pass = 123

INSERT INTO js_station ("station_name")
VALUES ('SAINT-PETERSBURG'),
       ('TVER'),
       ('MOSCOW'),
       ('VVORONEZH'),
       ('TULA'),
       ('ROSTOV'),
       ('ANAPA');

INSERT INTO js_route ("train_id", "station_id", "arrival_time", "departure_time")
VALUES (1, 1, '2019-09-12 23:40', '2019-09-12 23:55'),
       (1, 3, '2019-09-13 03:45', '2019-09-13 04:00'),
       (1, 4, '2019-09-13 07:55', '2019-09-13 08:30'),
       (2, 2, '2019-09-13 04:00', '2019-09-13 04:30'),
       (2, 3, '2019-09-13 16:55', '2019-09-13 17:30'),
       (2, 4, '2019-09-13 21:45', '2019-09-13 22:00'),
       (2, 5, '2019-09-14 02:55', '2019-09-14 03:15'),
       (3, 3, '2019-09-13 04:00', '2019-09-14 04:30'),
       (3, 5, '2019-09-13 09:55', '2019-09-14 10:30'),
       (3, 6, '2019-09-13 17:45', '2019-09-14 18:00'),
       (3, 7, '2019-09-13 22:55', '2019-09-14 23:15'),
       (4, 1, '2019-09-13 18:00', '2019-09-14 18:20'),
       (4, 3, '2019-09-14 06:55', '2019-09-14 07:20'),
       (4, 6, '2019-09-14 19:45', '2019-09-14 20:00'),
       (4, 7, '2019-09-15 02:55', '2019-09-15 03:15'),
       (5, 6, '2019-09-12 21:30', '2019-09-12 22:00'),
       (5, 5, '2019-09-13 05:00', '2019-09-13 05:20'),
       (5, 4, '2019-09-13 14:55', '2019-09-13 15:20'),
       (5, 3, '2019-09-13 23:45', '2019-09-13 23:55'),
       (5, 1, '2019-09-14 03:55', '2019-09-14 04:15'),
       (6, 6, '2019-09-12 05:00', '2019-09-12 05:20'),
       (6, 5, '2019-09-12 14:55', '2019-09-12 15:20'),
       (6, 3, '2019-09-12 23:45', '2019-09-13 00:01'),
       (6, 1, '2019-09-13 03:55', '2019-09-13 04:15'),
       (7, 6, '2019-09-13 04:00', '2019-09-13 04:30'),
       (7, 5, '2019-09-13 16:55', '2019-09-13 17:30'),
       (7, 4, '2019-09-13 21:45', '2019-09-13 22:00'),
       (7, 2, '2019-09-14 02:55', '2019-09-14 03:15');


INSERT INTO js_ticket ("carriage_id", "place_number", "user_id", "start_station_id", "finish_station_id",
                       "ticket_price", "creation_time")
VALUES (2, 1, 1, 1, 3, 4000, '2021-07-21 03:15'),
       (1, 2, 2, 3, 4, 3000, '2021-07-21 03:15'),
       (2, 2, 3, 1, 3, 4000, '2021-07-21 03:15');

INSERT INTO js_price (carriage_type, train_id, start_station_id, finish_station_id, price)
values ('BUSINESS', 1, 1, 3, 3000),
       ('COMFORT', 1, 1, 3, 2000),
       ('ECONOMY', 1, 1, 3, 1000),
       ('BUSINESS', 1, 3, 4, 3000),
       ('COMFORT', 1, 3, 4, 2000),
       ('ECONOMY', 1, 3, 4, 1000),
       ('BUSINESS', 2, 2, 3, 3000),
       ('COMFORT', 2, 2, 3, 2000),
       ('ECONOMY', 2, 2, 3, 1000),
       ('BUSINESS', 2, 3, 4, 3000),
       ('COMFORT', 2, 3, 4, 2000),
       ('ECONOMY', 2, 3, 4, 1000),
       ('BUSINESS', 2, 4, 5, 3000),
       ('COMFORT', 2, 4, 5, 2000),
       ('ECONOMY', 2, 4, 5, 1000),
       ('BUSINESS', 3, 3, 5, 3000),
       ('COMFORT', 3, 3, 5, 2000),
       ('ECONOMY', 3, 3, 5, 1000),
       ('BUSINESS', 3, 5, 6, 3000),
       ('COMFORT', 3, 5, 6, 2000),
       ('ECONOMY', 3, 5, 6, 1000),
       ('BUSINESS', 3, 6, 7, 3000),
       ('COMFORT', 3, 6, 7, 2000),
       ('ECONOMY', 3, 6, 7, 1000),
       ('BUSINESS', 4, 1, 3, 3000),
       ('COMFORT', 4, 1, 3, 2000),
       ('ECONOMY', 4, 1, 3, 1000),
       ('BUSINESS', 4, 3, 6, 3000),
       ('COMFORT', 4, 3, 6, 2000),
       ('ECONOMY', 4, 3, 6, 1000),
       ('BUSINESS', 4, 6, 7, 3000),
       ('COMFORT', 4, 6, 7, 2000),
       ('ECONOMY', 4, 6, 7, 1000),
       ('BUSINESS', 5, 6, 5, 3000),
       ('COMFORT', 5, 6, 5, 2000),
       ('ECONOMY', 5, 6, 5, 1000),
       ('BUSINESS', 5, 5, 4, 3000),
       ('COMFORT', 5, 5, 4, 2000),
       ('ECONOMY', 5, 5, 4, 1000),
       ('BUSINESS', 5, 4, 3, 3000),
       ('COMFORT', 5, 4, 3, 2000),
       ('ECONOMY', 5, 4, 3, 1000),
       ('BUSINESS', 5, 3, 1, 3000),
       ('COMFORT', 5, 3, 1, 2000),
       ('ECONOMY', 5, 3, 1, 1000),
       ('BUSINESS', 6, 6, 5, 3000),
       ('COMFORT', 6, 6, 5, 2000),
       ('ECONOMY', 6, 6, 5, 1000),
       ('BUSINESS', 6, 5, 3, 3000),
       ('COMFORT', 6, 5, 3, 2000),
       ('ECONOMY', 6, 5, 3, 1000),
       ('BUSINESS', 6, 3, 1, 3000),
       ('COMFORT', 6, 3, 1, 2000),
       ('ECONOMY', 6, 3, 1, 1000),
       ('BUSINESS', 7, 6, 5, 3000),
       ('COMFORT', 7, 6, 5, 2000),
       ('ECONOMY', 7, 6, 5, 1000),
       ('BUSINESS', 7, 5, 4, 3000),
       ('COMFORT', 7, 5, 4, 2000),
       ('ECONOMY', 7, 5, 4, 1000),
       ('BUSINESS', 7, 4, 2, 3000),
       ('COMFORT', 7, 4, 2, 2000),
       ('ECONOMY', 7, 4, 2, 1000);




